import React from 'react'
import { StaticRouter } from 'react-router-dom'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'

import App from './src/App'


export default ({
    store,
    url,
    DL404,
}) => renderToString(
    <StaticRouter location={url}>
        <Provider store={store}>
            <App DL404={DL404}/>
        </Provider>
    </StaticRouter>
)