const path = require('path');
const miniCss = require('mini-css-extract-plugin');


module.exports = {
    mode: "development",
    entry: ["./index.client.js"],
    output: {
        filename: "client.js",
        path: path.resolve(__dirname, "dist"),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/i,
                exclude: /(node_modules)/,
                loader: "babel-loader"
            },
            {
                test: /\.(jpg|jpeg|png|webp|svg|ttf|woff2|woff|eot)$/i,
                loader: "file-loader",
                options: {
                    name: "[path][name].[ext]",
                },
            },
            {
                test: /\.s[ca]ss$/i,
                use: [
                    miniCss.loader,
                    "css-loader", 
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("node-sass"),
                        },
                    },
                ]
            }
        ]
    },
    plugins: [
        new miniCss({
            filename: "style.css"
        })
    ],
    resolve: {
        extensions: [".js", ".jsx", ".json", ".ts", ".tsx"],
    },
};