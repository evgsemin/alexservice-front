import express from 'express'
import device from 'express-device'
import cookieParser from 'cookie-parser'
import path from 'path'
import SSR from './ssr'

import { appendUniversalPortals } from 'react-portal-universal/lib/server'

import { cities } from './src/store/connect.server'
import { loadAll } from './src/sagas/sagas.server'
import { robotsTxt as getRobotsTxt } from './src/store/DetermineCurrentCity/select'
import { renderTemplate } from './utilities/renderTemplate'
import { getAppPages } from './src/store/App/utilities'
import { getSeoData, getRequestData } from './utilities/get'
import { isFoundedPage } from './utilities/is'
import { printSeo } from './src/utilities/printSeo'
import { createSitemap } from './utilities/createSitemap'


const PORT  = process.env.PORT || 4003
const APP   = express()
const CACHE = 15 // минут

//
// Маршруты
//

APP.use(device.capture())
APP.use(cookieParser())

APP.use("/dist", express.static(path.resolve(__dirname, "dist")))

APP.get("/update-cache", (req, res) => {
    updateStore()
    res.status(404).send("<h2>Not found.</h2>")
})

// закрытие для теста
APP.get("/PbVQBGVCKVAv7NOjAhTi", (req, res) => {
    res.cookie("iamDeveloper", "true", { maxAge: 999999999 })
    res.status(200).send(`
        Авторизация успешно пройдена.
        <script>
            setTimeout(function() {
                window.location.href = "/";
            }, 1300);
        </script>
    `)
})

APP.get("/sitemap.xml", (req, res) => {
    testClose(req, res)

    const state = getStore("moscow").getState()
    const pages = getAppPages(state)
    const host  = getRequestData(req).host

    const sitemap = createSitemap(host, pages)

    res.header("Content-Type", "text/xml")
    res.send(sitemap)
})

APP.get("/robots.txt", (req, res) => {
    testClose(req, res)

    const state = getStore("moscow").getState()
    const robotsTxt = getRobotsTxt(state, "moscow")

    res.header("Content-Type", "text/plain")
    res.send(robotsTxt)
})

APP.get("*", (req, res) => {
    testClose(req, res)

    const { status, html, error } = getAnswer({
        cityTicket: "moscow",
        req
    })

    if(error) {
        res.status(304).send("")
    } else {
        res.status(status).send(html)
    }
})

//
// funcs
//

function getAnswer({
    cityTicket,
    req,
}) {
    const { host, url, path } = getRequestData(req)

    process.fk = {
        deviceType: req.device.type.toUpperCase(),
        path,
        host,
    }

    const store     = getStore(cityTicket)
    const state     = store.getState()

    const pages     = getAppPages(state)
    const seoData   = getSeoData(pages, path)
    const isFounded = isFoundedPage(pages, path)
    const status    = isFounded ? 200 : 404
    const DL404     = isFounded ? undefined : path

    let html = renderTemplate({
        host,
        path,
        DL404,
        state,
        content: SSR({ store, url, DL404 }),
    })
    html = printSeo({ html, ...seoData })
    html = appendUniversalPortals(html)

    return {
        status,
        html,
        error: false,
    }
}

//
// Старт
//

updateStore()
setInterval(updateStore, CACHE*60000)

APP.listen(PORT, () => console.log(`Сервер работает на порту ${PORT}...`))

//
// funcs
//

async function updateStore() {
    for(let ticket in cities) {
        const city = cities[ticket]
        await city.store.runSagas(loadAll, { currentCityId: city.id }).toPromise()
    }
}

function getStore(ticket) {
    return cities[ticket].store
}

function testClose(req, res) {
    // закрытие для теста
    const isHidden = !(req.cookies?.iamDeveloper || getRequestData(req).host.indexOf("localhost") !== -1)
    if(isHidden) return res.status(200).send("<a href='/PbVQBGVCKVAv7NOjAhTi'><h2 style='font-family:sans-serif;'>Перейдите по этой ссылке, чтобы попасть на сайт.</h2></a>")
}