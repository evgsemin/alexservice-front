import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isArr, isServer } from '../../utilities/is'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_WORKS_HOURS, putWorksHours } from '../../store/WorkHours/actions'

import { worksHours as getWorksHours } from '../../store/WorkHours/select'


export default function* () {
    yield takeEvery(LOAD_WORKS_HOURS, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "WorkHours",
        status: true,
    }))

    try {
        const cachedWorksHours = yield select(state => getWorksHours(state))

        if(!length(cachedWorksHours) || isServer()) {
            const fetchedWorksHours = yield call(fetchWorksHours)
        
            // Успех
            if(!fetchedWorksHours.code) {
                const worksHours = {}

                fetchedWorksHours.map(workHours => {
                    if(isArr(workHours?.data?.works_tables)) {
                        const rows = workHours.data.works_tables.map(row => ({
                            title: row.time_for_work,
                            items: row.works?.map(work => work.work_title),
                        }))
    
                        worksHours[workHours.id] = {
                            id: workHours.id,
                            slug: workHours.slug,
                            title: workHours.title,
                            rows,
                        }
                    }
                })

                yield put(putWorksHours({
                    worksHours,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_WORKS_HOURS, fetchedWorksHours)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_WORKS_HOURS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "WorkHours",
            status: false,
        }))
    }
}

function fetchWorksHours() {
    return fetch( buildAPIUrl({
        type: "works_hours",
    }) )
        .then(resp => resp.json())
}