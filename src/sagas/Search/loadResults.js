import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'

import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { map } from '../../utilities/map'
import { search } from '../../utilities/search'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_RESULTS, putResults } from '../../store/Search/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { allServices as getAllServices, allArticles as getAllArticles } from '../../store/Posts/select'


const isMatch = search()

export default function* () {
    yield takeEvery(LOAD_RESULTS, worker)
}

function* worker({ payload }) {
    yield put(changeEventStatus({
        type: "loading",
        name: "Search",
        status: true,
    }))

    const requestToServer = false

    const currentCity = yield select(state => currentId(state))

    try {
        const phrase = payload.phrase

        // Данные с сервера
        if(requestToServer) {
            const fetchedSearch = yield call(fetchSearch, "services, articles", phrase, currentCity)
        
            // Успех
            if(!fetchedSearch.code) {
                const filtered = []

                fetchedSearch.map(result => {
                    let topic

                    if(result.type === "services") topic = "Услуги"
                    if(result.type === "articles") topic = "Статьи"

                    filtered.push({
                        topic,
                        title: result.title,
                        href: "/",
                    })
                })

                yield put(putResults({
                    results: filtered
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_RESULTS, fetchedSearch)
            }
        }

        // Берём сохранённые данные
        else {
            const services = yield select(state => getAllServices(state))
            const articles = yield select(state => getAllArticles(state))

            const founded = []

            map(services, (service) => {
                if(!service.isChild) {
                    if(isMatch(service.title, phrase) || isMatch(service.contentBeforeTables, phrase) || isMatch(service.contentAfterTables, phrase)) {
                        founded.push({
                            topic: "Услуги",
                            title: service.title,
                            href: service.href,
                        })
                    }
                }
            })

            map(articles, (article) => {
                if(isMatch(article.title, phrase) || isMatch(article.content, phrase)) {
                    founded.push({
                        topic: "Статьи",
                        title: article.title,
                        href: article.href,
                    })
                }
            })

            yield put(putResults({
                results: founded
            }))
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_RESULTS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "Search",
            status: false,
        }))
    }
}

function fetchSearch(type, phrase, currentCity) {
    return fetch( buildAPIUrl({
        type,
        search: phrase,
        city: currentCity,
    }) )
        .then(resp => resp.json())
}