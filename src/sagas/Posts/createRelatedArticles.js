import { fetch } from '../../utilities/uni'
import { put, takeEvery, select } from 'redux-saga/effects'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { appendArticlesRelatedLinks } from './utilities/appendArticlesRelatedLinks'

import { CREATE_RELATED_ARTICLES, putArticles } from '../../store/Posts/actions'

import { allArticles as getAllArticles } from '../../store/Posts/select'


export default function* () {
    yield takeEvery(CREATE_RELATED_ARTICLES, worker)
}

export function* worker() {
    try {
        const articles = yield select(state => getAllArticles(state))

        yield put(putArticles({
            articles: appendArticlesRelatedLinks(articles),
        }))
    }

    // Ошибка
    catch(e) {
        printError(2, CREATE_RELATED_ARTICLES, e)
    }
}