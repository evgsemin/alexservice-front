import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'
import { getACFImageData } from '../../utilities/getACFImageData'
import { createPostId, createPostLink } from '../../utilities/createPostIdAndLink'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_PAGES, putPages } from '../../store/Posts/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { allPages as getAllPages } from '../../store/Posts/select'


export default function* () {
    yield takeEvery(LOAD_PAGES, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "PostsPages",
        status: true,
    }))

    const currentCityId = yield select(state => currentId(state))

    try {
        const cachedPages = yield select(state => getAllPages(state))

        if(!length(cachedPages) || isServer()) {
            const fetchedPages = yield call(fetchPages, currentCityId)
        
            // Успех
            if(!fetchedPages.code) {
                const pages = {}

                fetchedPages.map(page => {
                    const postId = createPostId({ slug: page.slug, id: page.id })
                    const href = createPostLink({ postId })

                    pages[postId] = {
                        id: page.id,
                        parentId: page.parentId,
                        isChild: page.isChild,
                        title: page.title,
                        content: page.content,
                        contentBeforeMore: page.content?.includes("<!--more-->")
                                                ? page.content?.split("<!--more-->")[0]
                                                : undefined,
                        slug: page.slug,
                        postId,
                        href,
                        seo: {
                            title: page.data.seoTitle,
                            description: page.data.seoDescription,
                            keywords: page.data.seoKeywords,
                            cover: getACFImageData(page.data.seoCover?.sizes, "medium").medium,
                        },
                    }
                })

                yield put(putPages({
                    pages,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_PAGES, fetchedPages)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_PAGES, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "PostsPages",
            status: false,
        }))
    }
}

function fetchPages(currentCityId) {
    return fetch( buildAPIUrl({
        type: "page",
        city: currentCityId,
    }) )
        .then(resp => resp.json())
}