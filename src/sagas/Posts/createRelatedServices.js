import { fetch } from '../../utilities/uni'
import { put, takeEvery, select } from 'redux-saga/effects'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { map } from '../../utilities/map'

import { CREATE_RELATED_SERVICES, putArticles } from '../../store/Posts/actions'

import {
    allServices as getAllServices,
    allArticles as getAllArticles,
} from '../../store/Posts/select'
import { isObj } from '../../utilities/is'


export default function* () {
    yield takeEvery(CREATE_RELATED_SERVICES, worker)
}

export function* worker() {
    try {
        let services = yield select(state => getAllServices(state))
        let articles = yield select(state => getAllArticles(state))

        if(length(articles)) {
            services = map(services, service => [ service.id, service ])

            
            for(let link in articles) {
                const ArticleRelatedServices = []

                articles[link].relatedServices.map((service, index) => {
                    const serviceNumId = service.link[0]
                    if(services[serviceNumId]) {
                        ArticleRelatedServices[index] = {
                            ...service,
                            href: services[serviceNumId].href
                        }
                    }
                })

                articles[link].relatedServices = ArticleRelatedServices
            }

            articles = {...articles}
        
            yield put(putArticles({
                articles,
            }))
        }
    }

    // Ошибка
    catch(e) {
        printError(2, CREATE_RELATED_SERVICES, e)
    }
}

//
// funcs
//