import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { createPostId, createPostLink } from '../../utilities/createPostIdAndLink'
import { isArr, isServer } from '../../utilities/is'
import { getACFImageData } from '../../utilities/getACFImageData'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_SERVICES, putServices } from '../../store/Posts/actions'

import { currentId, currentCity as currentData } from '../../store/DetermineCurrentCity/select'
import { allServices as getAllServices } from '../../store/Posts/select'


export default function* () {
    yield takeEvery(LOAD_SERVICES, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "PostsServices",
        status: true,
    }))

    const currentCityId = yield select(state => currentId(state))
    const currentCity = yield select(state => currentData(state))

    try {
        const cachedServices = yield select(state => getAllServices(state))

        if(!length(cachedServices) || isServer()) {
            const fetchedServices = yield call(fetchServices, currentCityId)
        
            // Успех
            if(!fetchedServices.code) {
                const servicesTypes = {}
                const servicesByIds = {}

                fetchedServices.map(service => {
                    const postId = createPostId({ slug: service.slug, id: service.id })
                    const href = createPostLink({ postType: "services", postId })

                    servicesByIds[service.id] = {
                        ...service,
                        postId,
                        href,
                    }
                })
                
                fetchedServices.map(service => {
                    const serviceType = service.taxes["services_types"].terms[0].name

                    if(!servicesTypes[serviceType]) servicesTypes[serviceType] = {}

                    servicesTypes[serviceType][servicesByIds[service.id].postId] = {
                        id: service.id,
                        postId: servicesByIds[service.id].postId,
                        parentId: service.parentId,
                        parentPostId: servicesByIds[service.parentId]?.postId || "",
                        isChild: service.isChild,
                        assocBrand: Number((service.data.assocBrand || [])[0] || -1),
                        slug: service.slug,
                        title: service.title,
                        cityTitle: service.title + `${currentCity.nameWhere ? " в "+currentCity.nameWhere : ""}`,
                        cover: getACFImageData(service.data.serviceCover?.sizes, "medium").medium,
                        typeTitle: service.taxes["services_types"].terms[0].name,
                        typeSlug: service.taxes["services_types"].terms[0].slug,
                        contentBeforeTables: service.data.contentBeforeTables,
                        contentAfterTables: service.data.contentAfterTables,
                        tablePrices: service.data.tables?.tablePrices[0] || -1,
                        tableWorks: service.data.tables?.tableWorks[0] || -1,
                        faultsTitle: service.data.serviceFaultsTitle,
                        faults: isArr(service.data.serviceFaults) ? service.data.serviceFaults : [],
                        faqs: isArr(service.data.faqs) ? service.data.faqs : [],
                        href: servicesByIds[service.id].href,
                        sectionStepsVisible: service.data.serviceSectionStepsVisible,
                        sectionContactsVisible: service.data.serviceSectionContactsVisible,
                        seo: {
                            title: service.data.seoTitle,
                            description: service.data.seoDescription,
                            keywords: service.data.seoKeywords,
                            cover: getACFImageData(service.data.seoCover?.sizes, "medium").medium,
                        },
                    }
                })

                yield put(putServices({
                    services: servicesTypes,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_SERVICES, fetchedServices)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_SERVICES, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "PostsServices",
            status: false,
        }))
    }
}

function fetchServices(currentCityId) {
    return fetch( buildAPIUrl({
        type: "services",
        city: currentCityId,
        includeChilds: true,
        args: {
            order: "ASC"
        }
    }) )
        .then(resp => resp.json())
}