import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'
import { getACFImageData } from '../../utilities/getACFImageData'
import { createPostId, createPostLink } from '../../utilities/createPostIdAndLink'
import { appendArticlesRelatedLinks } from './utilities/appendArticlesRelatedLinks'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_ARTICLES, putArticles, createRelatedServices } from '../../store/Posts/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { allArticles as getAllArticles } from '../../store/Posts/select'


export default function* () {
    yield takeEvery(LOAD_ARTICLES, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "PostsArticles",
        status: true,
    }))

    const currentCityId = yield select(state => currentId(state))

    try {
        const cachedArticles = yield select(state => getAllArticles(state))

        if(!length(cachedArticles) || isServer()) {
            let fetchedArticles = yield call(fetchArticles, currentCityId)
            
            fetchedArticles[1] = {...fetchedArticles[0]}
            fetchedArticles[2] = {...fetchedArticles[0]}
            fetchedArticles[0].title = "Назад "+fetchedArticles[0].title
            fetchedArticles[0].id = 99
            fetchedArticles[2].title = "Вперёд "+fetchedArticles[2].title
            fetchedArticles[2].id = 233
        
            // Успех
            if(!fetchedArticles.code) {
                const articles = {}

                fetchedArticles.map(a => {
                    const articleId = createPostId({
                        slug: a.slug,
                        id: a.id,
                    })

                    articles[articleId] = {
                        className: a.data.className,
                        id: a.id,
                        title: a.title,
                        image: getACFImageData(a.data.article_cover.sizes, "medium_large").medium_large,
                        content: a.data.articleContent,
                        relatedServices: a.data.articleRelatedServices || [],
                        isReadAlso: a.data.articleIsReadAlso,
                        description: a.data.articleShort,
                        href: createPostLink({
                            postType: "articles",
                            postId: articleId
                        }),
                        seo: {
                            title: a.data.seoTitle,
                            description: a.data.seoDescription,
                            keywords: a.data.seoKeywords,
                            cover: getACFImageData(a.data.seoCover?.sizes, "medium").medium,
                        },
                    }
                })

                yield put(putArticles({
                    articles: appendArticlesRelatedLinks(articles),
                }))
                yield put(createRelatedServices())
            }
            
            // Ошибка
            else {
                printError(1, LOAD_ARTICLES, fetchedArticles)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_ARTICLES, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "PostsArticles",
            status: false,
        }))
    }
}

function fetchArticles(currentCityId) {
    return fetch( buildAPIUrl({
        type: "articles",
        city: currentCityId,
    }) )
        .then(resp => resp.json())
}