import { map } from '../../../utilities/map'


export function appendArticlesRelatedLinks(articles) {
    const articlesWithRelatedLinks = {}
    const articlesLinks = []

    map(articles, (article, link) => {
        articlesLinks.push(link)
    })

    // Собираем записи в новый массив, но уже с .relatedArticles (если они есть и включены для конкретной записи)
    articlesLinks.map((link, index) => {
        articlesWithRelatedLinks[link] = articles[link]

        if(articlesWithRelatedLinks[link]?.isReadAlso) {
            let prevLink = articlesLinks[index-1]
            let nextLink = articlesLinks[index+1]

            // Зацикливаем ссылки, если есть возможность
            if(articlesLinks.length > 2) {
                if(!prevLink) prevLink = articlesLinks[articlesLinks.length-1]
                if(!nextLink) nextLink = articlesLinks[0]
            }

            if(prevLink || nextLink) {
                articlesWithRelatedLinks[link].relatedArticles = []

                if(prevLink) {
                    articlesWithRelatedLinks[link].relatedArticles.push({
                        href: prevLink,
                        title: articles[prevLink].title
                    })
                }
                if(nextLink) {
                    articlesWithRelatedLinks[link].relatedArticles.push({
                        href: nextLink,
                        title: articles[nextLink].title
                    })
                }
            }
        }
    })

    return articlesWithRelatedLinks
}