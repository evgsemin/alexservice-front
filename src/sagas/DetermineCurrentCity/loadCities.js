import { fetch } from '../../utilities/uni'
import { call, put, takeEvery } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { getACFImageData } from '../../utilities/getACFImageData'

import { LOAD_CITIES, putCities } from '../../store/DetermineCurrentCity/actions'


export default function* () {
    yield takeEvery(LOAD_CITIES, worker)
}

export function* worker() {
    try {
        const fetchedCities = yield call(fetchCities)
        
        if(!fetchedCities.code) {
            const cities = {}

            fetchedCities.map(city => {
                const phones = city.data.phones.replace(", ", ",").split(",")
                city.data.scriptsHead = city.data.scriptsHead ? city.data.scriptsHead.replace(/script/g, "s-c-r-i-p-t") : ""
                city.data.scriptsHead = city.data.scriptsHead ? city.data.scriptsHead.replace(/style/g, "s-t-y-l-e") : ""
                city.data.scriptsBody = city.data.scriptsBody ? city.data.scriptsBody.replace(/script/g, "s-c-r-i-p-t") : ""
                city.data.scriptsBody = city.data.scriptsBody ? city.data.scriptsBody.replace(/style/g, "s-t-y-l-e") : ""

                const cityData = {
                    id: city.id,
                    title: city.data.name,
                    nameWhere: city.data.name_where,
                    mapHtml: city.data.map_html,
                    robotsTxt: city.data.cityRobotsTxt,
                    ...city.data,
                    mainPhone: phones[0],
                    phones,
                    seo: {
                        title: city.data.cityTitle,
                        description: city.data.cityDescription,
                        keywords: city.data.cityKeywords,
                        cover: getACFImageData(city.data.cityCover.sizes, "large").large,
                    },
                }

                delete cityData.name_where
                delete cityData.map_html

                cities[city.id] = cityData
            })

            yield put(putCities({
                cities
            }))
        }
        
        // Ошибка
        else {
            printError(1, LOAD_CITIES, fetchedCities)
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_CITIES, e)
    }
}

function fetchCities() {
    return fetch( buildAPIUrl({
        type: "cities"
    }) )
        .then(resp => resp.json())
}