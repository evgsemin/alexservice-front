import { fetch } from '../../../utilities/uni'
import { call, takeEvery } from 'redux-saga/effects'
import { buildAPIUrl } from '../../../utilities/buildAPIUrl'

import { LOAD_ARTICLES, putArticles } from '../../../store/routes/Articles/actions'

export default function* () {
    yield takeEvery(LOAD_ARTICLES, worker)
}

export function* worker() {
    const fetchedArticles = yield call(fetchArticles)
    yield call(fetchSearch, "фасадные")
    yield call(fetchAdd, "https://xn--80agcocee3amvt.xn--p1ai/wp-json/wp/v2/media/1471")
}

function fetchArticles() {
    return fetch( buildAPIUrl(
        "product/3709"
    ) )
        .then(resp => resp.json())
}

function fetchSearch(word) {
    return fetch( buildAPIUrl(
        "search",
        {
            search: word,
            per_page: 50,
        }
    ) )
        .then(resp => resp.json())
}

function fetchAdd(add) {
    return fetch(add).then(resp => resp.json())
}