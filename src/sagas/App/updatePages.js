import 'regenerator-runtime/runtime'
import { takeEvery, select, put } from 'redux-saga/effects'
import { printError } from '../../utilities/printError'
import { getAppPages } from '../../store/App/utilities'

import { UPDATE_PAGES, putPages } from '../../store/App/actions'
import { PUT_SERVICES, PUT_ARTICLES, PUT_PAGES } from '../../store/Posts/actions'
import { PUT_CITIES, CHANGE_CITY } from '../../store/DetermineCurrentCity/actions'


export default function* () {
    yield takeEvery(UPDATE_PAGES, worker)
    yield takeEvery(PUT_SERVICES, worker)
    yield takeEvery(PUT_ARTICLES, worker)
    yield takeEvery(PUT_PAGES, worker)
    yield takeEvery(PUT_CITIES, worker)
    yield takeEvery(CHANGE_CITY, worker)
}

export function* worker() {
    try {
        const state = yield select(state => state)
        const pages = getAppPages(state)

        yield put(putPages({ pages }))
    }

    // Ошибка
    catch(e) {
        printError(2, UPDATE_PAGES, e)
    }
}