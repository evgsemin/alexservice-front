import { all, call, put } from 'redux-saga/effects'
import { changeCity } from '../store/DetermineCurrentCity/actions'

import { worker as loadActions } from './Actions/loadActions'

import { worker as updatePages } from './App/updatePages'

import { worker as loadBrands } from './Brands/loadBrands'

import { worker as loadCities } from './DetermineCurrentCity/loadCities'

import { worker as loadFaqs } from './FAQ/loadFaqs'

import { worker as loadArticles } from './Posts/loadArticles'
import { worker as loadPages } from './Posts/loadPages'
import { worker as loadServices } from './Posts/loadServices'
import { worker as createRelatedArticles } from './Posts/createRelatedArticles'
import { worker as createRelatedServices } from './Posts/createRelatedServices'

import { worker as loadPricesTables } from './PricesTable/loadPricesTables'

import { worker as loadPromos } from './Promos/loadPromos'

import { worker as loadFaults } from './TopFaults/loadFaults'

import { worker as loadWorksHours } from './WorkHours/loadWorksHours'


export function* loadAll({ currentCityId }) {
    yield put(changeCity({ cityId: currentCityId }))
    yield call(loadActions)
    yield call(loadCities)
    yield call(loadBrands)
    yield call(loadFaqs)
    yield call(loadServices)
    yield call(loadArticles)
    yield call(loadPages)
    yield call(loadPricesTables)
    yield call(loadPromos)
    yield call(loadFaults)
    yield call(loadWorksHours)
    yield call(updatePages)
    yield call(createRelatedArticles)
    yield call(createRelatedServices)
}