import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { isValidObj } from '../../utilities/valid'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { SEND, clearState } from '../../store/OrderForm/actions'
import { validMaps } from '../../store/OrderForm/validMaps'

import { formFields as getFormFields } from '../../store/OrderForm/select'
import { currentCity as getCurrentCity } from '../../store/DetermineCurrentCity/select'


export default function* () {
    yield takeEvery(SEND, worker)
}

function* worker({ payload }) {
    const formId = payload.formId

    yield put(changeEventStatus({
        type: "sending",
        name: "OrderForm"+formId,
        status: true,
    }))

    const currentCity = yield select(state => getCurrentCity(state))

    try {
        const formFields = yield select(state => getFormFields(state, formId))
        
        const params = {
            ...formFields,
            typeTitle: payload.typeTitle,
            title: payload.title,
            cityId: currentCity.id,
            cityName: currentCity.title,
        }

        const isValid = isValidObj(validMaps[formId], params)

        if(isValid) {
            const sended = yield call(fetchSend, params)

            // Успех
            if(!sended.code) {
                yield put(clearState({ formId }))
                alert("Спасибо за Ваше обращение. Скоро мы Вам поможем.")
            }

            // Ошибка
            else {
                alert("ОШИБКА! К сожалению сообщение не было отправлено. Пожалуйста, свяжитесь с нами по телефону, указанному вверху сайта.")
                printError(1, SEND, sended)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, SEND, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "sending",
            name: "OrderForm"+formId,
            status: false,
        }))
    }
}

function fetchSend(params) {
    return fetch( buildAPIUrl({
        method: "send",
        params,
    }) )
        .then(resp => resp.json())
}