import 'regenerator-runtime/runtime'
import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { getACFImageData } from '../../utilities/getACFImageData'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_ACTIONS, putActions } from '../../store/Actions/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { actions as getActions } from '../../store/Actions/select'


export default function* () {
    yield takeEvery(LOAD_ACTIONS, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "Actions",
        status: true,
    }))

    const currentCityId = yield select(state => currentId(state))

    try {
        const cachedActions = yield select(state => getActions(state))

        if(!length(cachedActions) || isServer()) {
            const fetchedActions = yield call(fetchActions, currentCityId)
        
            // Успех
            if(!fetchedActions.code) {
                const actions = {}

                fetchedActions.map(action => {
                    actions[action.id] = {
                        id: action.id,
                        className: action.data.actionClassName,
                        title: action.data.actionTitle,
                        subtitle: action.data.actionSubtitle,
                        content: action.data.actionContent,
                        href: action.data.actionHref,
                        useButton: action.data.actionUseButton,
                        imageMedium: getACFImageData(action.data.actionImage.sizes, "medium").medium,
                        imageLarge: getACFImageData(action.data.actionImage.sizes, "large").large,
                    }
                })

                yield put(putActions({
                    actions,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_ACTIONS, fetchedActions)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_ACTIONS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "Actions",
            status: false,
        }))
    }
}

function fetchActions(currentCityId) {
    return fetch( buildAPIUrl({
        type: "actions",
        city: currentCityId,
    }) )
        .then(resp => resp.json())
}