import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_FAQS, putFaqs } from '../../store/FAQ/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { faqs as getFaqs } from '../../store/FAQ/select'


export default function* () {
    yield takeEvery(LOAD_FAQS, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "FAQ",
        status: true,
    }))

    const currentCityId = yield select(state => currentId(state))

    try {
        const cachedFaqs = yield select(state => getFaqs(state))

        if(!length(cachedFaqs) || isServer()) {
            const fetchedFaqs = yield call(fetchFaqs, currentCityId)
        
            // Успех
            if(!fetchedFaqs.code) {
                const faqs = {}

                fetchedFaqs.map(faq => {
                    faqs[faq.id] = {
                        id: faq.id,
                        slug: faq.slug,
                        title: faq.title,
                        content: faq.data.answer,
                    }
                })

                yield put(putFaqs({
                    faqs,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_FAQS, fetchedFaqs)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_FAQS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "FAQ",
            status: false,
        }))
    }
}

function fetchFaqs(currentCityId) {
    return fetch( buildAPIUrl({
        type: "faqs",
        city: currentCityId,
    }) )
        .then(resp => resp.json())
}