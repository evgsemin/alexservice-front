import 'regenerator-runtime/runtime'
import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { getACFImageData } from '../../utilities/getACFImageData'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_BRANDS, putBrands } from '../../store/Brands/actions'

import { brands as getBrands } from '../../store/Brands/select'


export default function* () {
    yield takeEvery(LOAD_BRANDS, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "Brands",
        status: true,
    }))

    try {
        const cachedBrands = yield select(state => getBrands(state))

        if(!length(cachedBrands) || isServer()) {
            const fetchedBrands = yield call(fetchBrands)
        
            // Успех
            if(!fetchedBrands.code) {
                const brands = {}

                fetchedBrands.map(brand => {
                    brands[brand.id] = {
                        id: brand.id,
                        slug: brand.slug,
                        title: brand.title,
                        titleEng: brand.data.name_eng,
                        titleRu: brand.data.name_ru,
                        ticket: brand.data.ticket,
                        logo: {
                            thumbnail: getACFImageData(brand.data.logo.sizes, "thumbnail").thumbnail,
                            medium: getACFImageData(brand.data.logo.sizes, "medium").medium,
                            mediumLarge: getACFImageData(brand.data.logo.sizes, "medium_large").medium_large,
                            large: getACFImageData(brand.data.logo.sizes, "large").large,
                        },
                    }
                })

                yield put(putBrands({
                    brands,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_BRANDS, fetchedBrands)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_BRANDS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "Brands",
            status: false,
        }))
    }
}

function fetchBrands() {
    return fetch( buildAPIUrl({
        type: "brands",
    }) )
        .then(resp => resp.json())
}