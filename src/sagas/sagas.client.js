import { all } from 'redux-saga/effects'

import ActionsLoadActions from './Actions/loadActions'

import AppUpdatePages from './App/updatePages'

import BrandsLoadBrands from './Brands/loadBrands'

import DetermineCurrentCityLoadCities from './DetermineCurrentCity/loadCities'

import FAQLoadFaqs from './FAQ/loadFaqs'

import OrderFormSend from './OrderForm/send'

import PostsCreateRelatedArticles from './Posts/createRelatedArticles'
import PostsCreateRelatedServices from './Posts/createRelatedServices'
import PostsLoadServices from './Posts/loadServices'
import PostsLoadPages from './Posts/loadPages'
import PostsLoadArticles from './Posts/loadArticles'

import PricesTableLoadPricesTables from './PricesTable/loadPricesTables'

import PromosLoadPromos from './Promos/loadPromos'
import SearchLoadResults from './Search/loadResults'
import TopFaultsLoadFaults from './TopFaults/loadFaults'
import WorkHoursLoadWorksHours from './WorkHours/loadWorksHours'

import RouteArticlesLoadArticles from './routes/Articles/loadArticles'


export function* rootSaga() {
    yield all([
        ActionsLoadActions(),
        
        AppUpdatePages(),

        BrandsLoadBrands(),

        DetermineCurrentCityLoadCities(),

        FAQLoadFaqs(),
        
        OrderFormSend(),

        PostsCreateRelatedArticles(),
        PostsCreateRelatedServices(),
        PostsLoadServices(),
        PostsLoadPages(),
        PostsLoadArticles(),

        PricesTableLoadPricesTables(),

        PromosLoadPromos(),
        SearchLoadResults(),
        TopFaultsLoadFaults(),
        WorkHoursLoadWorksHours(),
        
        RouteArticlesLoadArticles(),
    ])
}