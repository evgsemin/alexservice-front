import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isArr, isServer } from '../../utilities/is'
import { getACFImageData } from '../../utilities/getACFImageData'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_PROMOS, putPromos } from '../../store/Promos/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { promos as getPromos } from '../../store/Promos/select'


export default function* () {
    yield takeEvery(LOAD_PROMOS, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "Promos",
        status: true,
    }))

    const currentCity = yield select(state => currentId(state))

    try {
        const cachedPromos = yield select(state => getPromos(state))

        if(!length(cachedPromos) || isServer()) {
            const fetchedPromos = yield call(fetchPromo, currentCity)
        
            if(!fetchedPromos.code) {
                let promos = {}

                fetchedPromos.map(slide => {
                    const slideButtons = []
        
                    if(isArr(slide.data.buttons)) {
                        slide.data.buttons.map(button => {
                            slideButtons.push({
                                title: button.title,
                                size: button.size,
                                iconBefore: button.icon_before,
                                iconAfter: button.icon_after,
                                modalType: button.modal_type,
                                variant: button.variant,
                                href: button.href,
                                attrs: {
                                    attrs: {
                                        alt: button.alt,
                                        target: button.is_target_blank ? "_blank" : ""
                                    }
                                }
                            })
                        })
                    }
        
                    promos[slide.id] = {
                        id: slide.id,
                        type: slide.data.slide_type,
                        variant: slide.data.type_text_variant,
                        title: slide.data.promo,
                        slug: slide.slug,
                        subtitle: slide.data.banner_text,
                        image: getACFImageData(slide.data.image.sizes, "large").large,
                        buttons: slideButtons,
                    }
                })

                yield put(putPromos({
                    promos
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_PROMOS, fetchedPromos)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_PROMOS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "Promos",
            status: false,
        }))
    }
}

function fetchPromo(currentCity) {
    return fetch( buildAPIUrl({
        type: "promos",
        city: currentCity,
        args: {
            order: "DESC"
        }
    }) )
        .then(resp => resp.json())
}