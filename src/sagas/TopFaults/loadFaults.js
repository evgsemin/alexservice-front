import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'
import { map } from '../../utilities/map'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_FAULTS, putFaults, putFaultsCats } from '../../store/TopFaults/actions'

import { currentId } from '../../store/DetermineCurrentCity/select'
import { allFaults as getFaults } from '../../store/TopFaults/select'


export default function* () {
    yield takeEvery(LOAD_FAULTS, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "TopFaults",
        status: true,
    }))

    const currentCity = yield select(state => currentId(state))

    try {
        const cachedFaults = yield select(state => getFaults(state))

        if(!length(cachedFaults) || isServer()) {
            const fetchedFaults = yield call(fetchFaults, currentCity)
        
            if(!fetchedFaults.code) {
                const faults = {}
                const cats = {}

                fetchedFaults.map(fault => {
                    // Сохраняем поломку
                    const faultCat = fault.cats[0]?.slug || "other"

                    if(!faults[faultCat]) faults[faultCat] = {}
                    
                    const tasks = []
                    map(fault.data.works, work => tasks.push(work.work))

                    faults[faultCat][fault.id] = {
                        id: fault.id,
                        cat: faultCat,
                        title: fault.title,
                        what: fault.data.what?.toLocaleLowerCase(),
                        tasks: tasks,
                        price: fault.data.price,
                        hideCTAButton: fault.data.hideCTAButton || false,
                    }

                    // Сохраняем категории
                    map(fault.cats, cat => {
                        cats[cat.slug] = cat.name
                    })
                })

                yield put(putFaults({
                    faults
                }))

                yield put(putFaultsCats({
                    cats
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_FAULTS, fetchedFaults)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_FAULTS, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "TopFaults",
            status: false,
        }))
    }
}

function fetchFaults(currentCity) {
    return fetch( buildAPIUrl({
        type: "faults",
        city: currentCity
    }) )
        .then(resp => resp.json())
}