import { fetch } from '../../utilities/uni'
import { call, put, takeEvery, select } from 'redux-saga/effects'
import { buildAPIUrl } from '../../utilities/buildAPIUrl'
import { printError } from '../../utilities/printError'
import { length } from '../../utilities/length'
import { isServer } from '../../utilities/is'

import { changeEventStatus } from '../../store/useEventStatus/actions'
import { LOAD_PRICES_TABLES, putPricesTables } from '../../store/PricesTable/actions'

import { pricesTables as getPricesTables } from '../../store/PricesTable/select'


export default function* () {
    yield takeEvery(LOAD_PRICES_TABLES, worker)
}

export function* worker() {
    yield put(changeEventStatus({
        type: "loading",
        name: "PricesTable",
        status: true,
    }))

    try {
        const cachedPricesTables = yield select(state => getPricesTables(state))

        if(!length(cachedPricesTables) || isServer()) {
            const fetchedPricesTables = yield call(fetchPricesTables)
        
            // Успех
            if(!fetchedPricesTables.code) {
                const pricesTables = {}

                fetchedPricesTables.map(pricesTable => {
                    const rows = pricesTable.data.prices_tables.map(row => ({
                        name: row.title,
                        priceNumber: row.price_number,
                        priceText: row.price_text,
                    }))

                    pricesTables[pricesTable.id] = {
                        id: pricesTable.id,
                        slug: pricesTable.slug,
                        title: pricesTable.title,
                        rows,
                    }
                })

                yield put(putPricesTables({
                    pricesTables,
                }))
            }
            
            // Ошибка
            else {
                printError(1, LOAD_PRICES_TABLES, fetchedPricesTables)
            }
        }
    }

    // Ошибка
    catch(e) {
        printError(2, LOAD_PRICES_TABLES, e)
    }

    finally {
        yield put(changeEventStatus({
            type: "loading",
            name: "PricesTable",
            status: false,
        }))
    }
}

function fetchPricesTables() {
    return fetch( buildAPIUrl({
        type: "prices_tables",
    }) )
        .then(resp => resp.json())
}