import React, { useEffect, useMemo } from 'react'
import { useIsCitySelected } from '../../components/DetermineCurrentCity/useIsCitySelected'

import { cl } from '../../utilities/cl'
import { convertObjToArr } from '../../utilities/convert'

import Container from '../../components/FK/Container'
import Slider from '../../components/Slider'


const MainSlider:React.FC<{
    className?:string
    slides:any
    loadPromos:any
}> = ({
    className,
    slides = {},
    loadPromos,
}) => {

    const isCitySelected = useIsCitySelected()
    const slidesArr = useMemo(() => convertObjToArr(slides), [slides])

    useEffect(() => {
        if(isCitySelected) {
            loadPromos()
        }
    }, [isCitySelected])

    className = cl(
        "SectionMainSlider",
        className,
    )

    return (
        <section className={className}>
            <Container className="fk-pt-sm" size="xl">
                <Slider
                    className="RouteMain__MainSlider"
                    interval={8000}
                    slides={slidesArr}
                />
            </Container>
        </section>
    )
}

export default MainSlider