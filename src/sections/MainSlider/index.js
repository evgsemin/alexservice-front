import React from 'react'
import { connect } from 'react-redux'

import { loadPromos } from '../../store/Promos/actions'
import { promos } from '../../store/Promos/select'

import MainSlider from './MainSlider'


const MainSliderContainer = props => (
    <MainSlider
        className={props.className}
        slides={props.promos}
        loadPromos={props.loadPromos}
    />
)

const stateToProps = state => ({
    promos: promos(state)
})

const actionsToProps = {
    loadPromos
}

export default connect(stateToProps, actionsToProps)(MainSliderContainer)