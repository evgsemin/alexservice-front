import React from 'react'

import { cl } from '../../utilities/cl'

import Section from '../../components/FK/Section'
import Div from '../../components/Div'


const About:React.FC<{
    className?:string
    href:string
    content?:string
    previewContent?:string
}> = ({
    className,
    href,
    content,
    previewContent,
}) => {

    className = cl(
        "SectionAbout",
        className,
    )

    if(!content && !previewContent) return null

    return (
        <Section
            header={{
                title: "Сервисный центр ALEX SERVICE",
                titleTag: "h3",
            }}
            className={className}
            containerSize="md"
        >
            {previewContent && (
                <>
                    <Div>
                        {previewContent}
                    </Div>
                    <p>
                        <a href={href}>Читать подробнее &#8594;</a>
                    </p>
                </>
            )}
            {!previewContent && (
                <Div>{content}</Div>
            )}
        </Section>
    )
}

export default About