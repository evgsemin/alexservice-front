import React from 'react'
import { connect } from 'react-redux'

import { page } from '../../store/Posts/select'

import About from './About'


const aboutPostId = "o-kompanii"

const AboutContainer = props => (
    <About
        className={props.className}
        href={props.href}
        content={props.content}
        previewContent={props.previewContent}
    />
)

const stateToProps = state => ({
    href: page(state, aboutPostId).href,
    content: page(state, aboutPostId).content,
    previewContent: page(state, aboutPostId).contentBeforeMore
})

export default connect(stateToProps, null)(AboutContainer)