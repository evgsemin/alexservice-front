import React, { useMemo } from 'react'

import { cl } from '../../utilities/cl'

const svgOrder = '/dist/color-icons/shopping-cart.svg'
const svgClock = '/dist/color-icons/24-hours.svg'
const svgPay   = '/dist/color-icons/wallet.svg'

import Section from '../../components/FK/Section'
import Grid from '../../components/FK/Grid'
import LazyImage from '../../components/FK/LazyImage'


const Steps:React.FC<{
    className?:string
    titleTag?:string
}> = ({
    className,
    titleTag,
}) => {

    const items = useMemo(() => ([
        {
            title: "Закажите ремонт",
            icon: svgOrder,
            subtitle: "Оставьте заявку или свяжитесь с нами по телефону",
        },
        {
            title: "Выберите время",
            icon: svgClock,
            subtitle: "Договоритесь о времени визита мастера. Мастер может выехать в течении 15 минут с момента заказа",
        },
        {
            title: "Оплатите ремонт",
            icon: svgPay,
            subtitle: "Оплатите по факту ремонта и получите гарантийный талон. Ваша техника готова к работе!",
        }
    ]), [])

    className = cl(
        "SectionSteps",
        className,
    )

    return (
        <Section
            className={className}
            header={{
                title: "Как починить свою технику?",
                titleTag,
            }}
            containerSize="lg"
        >
            <div className="SectionSteps__wrap">
                <Grid
                    size="lg"
                    cols={{
                        desktopAndUp: 3,
                        tabletAndDown: 1,
                    }}
                >
                    {items.map((item, index) => (
                        <div key={index} className="SectionSteps__item">
                            <LazyImage
                                className="icon"
                                type="img"
                                image={item.icon}
                                alt={item.title}
                            />
                            <div className="number">{index+1}</div>
                            <h4 className="title">{item.title}</h4>
                            <div className="subtitle">{item.subtitle}</div>
                        </div>
                    ))}
                </Grid>
            </div>
        </Section>
    )
}

export default Steps