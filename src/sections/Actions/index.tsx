import React, { useState } from 'react'

import { cl } from '../../utilities/cl'
import { renderIf } from '../../utilities/renderIf'

import ActionsComponent from '../../components/Actions'

import Section from '../../components/FK/Section'


const Actions:React.FC<{
    className?:string
}> = ({
    className,
}) => {

    const [rendered, setRendered] = useState(false)

    className = cl(
        "SectionActions",
        className,
    )

    return (
        renderIf({
            isHide: true,
            condition: rendered,
            children: (
                <Section
                    className={className}
                    containerSize="lg"
                >
                    <ActionsComponent onRender={setRendered}/>
                </Section>
            )
        })
    )
}

export default Actions