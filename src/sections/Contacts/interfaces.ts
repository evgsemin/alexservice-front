export interface ICityInfoCard {
    className?:string
    title:string
    content:any
}

export interface ICityData {
    [key:string]:any
}

export interface ICityContact {
    className?:string
    cityName:string
    map:string
    infoCards:ICityInfoCard[]
}