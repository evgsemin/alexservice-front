import React, { useState, useMemo } from 'react'

import { cl } from '../../utilities/cl'
import { ICityData, ICityContact } from './interfaces'

import Container from '../../components/FK/Container'
import LazyMap from '../../components/FK/LazyMap'
import Grid from '../../components/FK/Grid'
import Loading from '../../components/FK/Loading'
import SVG from '../../components/FK/SVG'

import CardContent from '../../components/Card/Content'


const Contacts:React.FC<{
    className?:string
    currentId:number
    city:ICityData
    scrollOn?:boolean
    scrollFade?:boolean
}> = ({
    className,
    currentId,
    city = {},
    scrollOn = true,
    scrollFade = true,
}) => {

    const [scrollFaded, setScrollFaded] = useState(true)

    const cityContactObj:ICityContact = useMemo(() => buildCityContactObj(city), [city])

    className = cl(
        "SectionContacts",
        {"SectionContacts_map-scroll-on": scrollOn},
        className,
    )

    return (
        <section className={className}>
            <LazyMap
                className="SectionContacts__map"
                map={cityContactObj.map}
            />

            {(scrollFade && scrollFaded) && (
                <div className="scroll-fade" onClick={() => setScrollFaded(false)}>
                    <div>Нажмите, чтобы включить <br/>прокрутку карты</div>
                </div>
            )}

            <Container className="SectionContacts__content">
                <CardContent
                    className="SectionContacts__card"
                    variant="light"
                    size="sm-1"
                >
                    {currentId === -1 && (
                        <Loading/>
                    )}
                    {currentId !== -1 && (
                        <>
                            <h2 className="SectionContacts__title">Контакты в г. {cityContactObj.cityName}</h2>

                            <Grid
                                size="md"
                                cols={2}
                            >
                                {cityContactObj.infoCards.map((infoCard, index) => (
                                    <div
                                        key={index}
                                        className={cl(
                                            "SectionContacts__infoCard",
                                            infoCard.className,
                                        )}
                                    >
                                        {infoCard.title && <h4 className="title">{infoCard.title}</h4>}
                                        {infoCard.content}
                                    </div>
                                ))}
                            </Grid>
                        </>
                    )}
                </CardContent>
            </Container>
        </section>
    )
}

export default Contacts

//
// func
//

function buildCityContactObj(city:ICityData) {
    const answer = {
        cityName: city.title,
        map: city.mapHtml,
        infoCards: []
    }

    // Адрес
    answer.infoCards.push({
        title: <><SVG className="fk-mr-mr-1" icon="Location" size="auto"/> Адрес</>,
        content: 
            <p>
                <a
                    className="fk-text-skip"
                    href={city.linkToMap}
                    title={`AlexService ${city.title}`}
                    target="_blank"
                >
                    {city.address} <SVG className="fk-ml-mr-1" icon="NewTab" variant="dark-md" size="auto"/>
                </a>
            </p>
    })

    // Телефоны
    answer.infoCards.push({
        title: <><SVG className="fk-mr-mr-1" icon="Phone" size="auto"/> Телефоны</>,
        content:
            <p>
                {(city.phones || []).map((phone, index) => (
                    <a
                        key={index}
                        className="fk-text-skip fk-d-block"
                        href={`tel:${phone}`}
                    >
                        {phone}
                    </a>
                ))}
            </p>
    })

    // Часы работы
    answer.infoCards.push({
        title: <><SVG className="fk-mr-mr-1" icon="Clock" size="auto"/> Часы работы</>,
        content:
            <p>
                ПН-ПТ: {city.openingHours?.from} — {city.openingHours?.to} <br/>
                СБ-ВС: {city.openingHours?.from} — {city.openingHours?.to} <br/>
            </p>
    })

    return answer
}