import React from 'react'
import { connect } from 'react-redux'

import { currentId, currentCity } from '../../store/DetermineCurrentCity/select'

import Contacts from './Contacts'


const ContactsContainer = props => (
    <Contacts
        className={props.className}
        scrollOn={props.scrollOn}
        currentId={props.currentId}
        city={props.cityData}
    />
)

const stateToProps = state => ({
    currentId: currentId(state),
    cityData: currentCity(state),
})

export default connect(stateToProps, null)(ContactsContainer)