import React from 'react'

import { cl } from '../../utilities/cl'
import { isLength } from '../../utilities/is'
import { ICardImageLink } from '../../components/Card/ImageLink/interfaces'

import CardImageLink from '../../components/Card/ImageLink'

import Section from '../../components/FK/Section'
import Grid from '../../components/FK/Grid'


const ImageMenu:React.FC<{
    className?:string
    type: "appliances" | "conditioners"
    title?:string
    links:ICardImageLink[]
}> = ({
    className,
    title,
    links = [],
}) => {

    className = cl(
        "SectionImageMenu",
        className,
    )

    if(!isLength(links)) return null

    return (
        <Section
            className={className}
            header={{
                title,
            }}
            containerSize="lg"
        >
            <Grid cols={{
                desktopAndUp: 4,
                tablet: 3,
                mobileAndDown: 2,
            }}>
                {links.map((link, index) => (
                    <CardImageLink
                        key={index}
                        className={link.className}
                        title={link.title}
                        alt={link.alt || link.title}
                        href={link.href}
                        image={link.image}
                        onClick={link.onClick}
                    />
                ))}
            </Grid>
        </Section>
    )
}

export default ImageMenu