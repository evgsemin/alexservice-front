import React  from 'react'
import { connect }  from 'react-redux'

import { serviceMenuByType } from '../../store/Posts/select'

import ImageMenu from './ImageMenu'


const ImageMenuContainer = props => (
    <ImageMenu
        className={props.className}    
        type={props.type}
        title={props.servicesMenu.title}
        links={props.servicesMenu.links}
    />
)

const stateToProps = (state, props) => ({
    servicesMenu: serviceMenuByType(state, props.type)
})

export default connect(stateToProps, null)(ImageMenuContainer)