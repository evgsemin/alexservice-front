import React from 'react'

import { cl } from '../../utilities/cl'

import Section from '../../components/FK/Section'
import Button from '../../components/FK/Button'


const Corps:React.FC<{
    className?:string
}> = ({
    className,
}) => {

    className = cl(
        "SectionCorps",
        "fk-bg-",
        className,
    )

    return (
        <Section
            className={className}
            header={{
                title: "Ремонт промышленной и коммерческой техники",
                subtitle: "Компания АлексСервис осуществляет профессиональный ремонт и обслуживание промышленной техники во всех представленных городах. Мы гарантируем качество и своевременность оказываемых услуг."
            }}
            containerSize="lg"
            containerProps={{
                by: "margin"
            }}
        >
            <div className="fk-align-center">
                <Button
                    title="Узнать подробнее"
                    size="xl"
                    variant="prim-linear"
                    iconAfter="ArrowRight"
                    modalType="OrderCorp"
                />
            </div>
        </Section>
    )
}

export default Corps