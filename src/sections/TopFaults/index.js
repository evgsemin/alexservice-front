import React from 'react'
import { connect } from 'react-redux'

import { faults, cats } from '../../store/TopFaults/select'

import TopFaults from './TopFaults'


const TopFaultsContainer = props => (
    <TopFaults
        className={props.className}
        faults={props.faults}
        cats={props.cats}
        showMore={props.showMore}
    />
)

const stateToProps = state => ({
    faults: faults(state),
    cats: cats(state),
})

export default connect(stateToProps, null)(TopFaultsContainer)