import React, { useState, useMemo } from 'react'

import { cl } from '../../utilities/cl'
import { isLength } from '../../utilities/is'
import { map } from '../../utilities/map'
import { renderIf } from '../../utilities/renderIf'
import { useIsBrowser } from '../../utilities/useIsBrowser'

import Section from '../../components/FK/Section'
import Group from '../../components/FK/Group'
import Button from '../../components/FK/Button'

import TopFaults from '../../components/TopFaults'


const SectionTopFaults:React.FC<{
    className?:string
    faults:any
    cats:any
    showMore?:number
}> = ({
    className,
    faults,
    cats,
    showMore = 10,
}) => {

    const isBrowser = useIsBrowser()

    const [ iFaults, firstSlug ] = useMemo(() => {
        const cats = {}
        let first = ""
        map(faults, (cat, catSlug) => {
            if(catSlug !== "other") {
                if(!first) first = catSlug
                cats[catSlug] = cat
            }
        })
        return [ cats, first ]
    }, [faults])

    const [active, setActive] = useState(firstSlug)

    const header = useMemo(() => ({
        className: "SectionTopFaults__header",
        title: `Топ-${showMore} поломок`,
        contentAfter: getButtons(),
        titleTag: "h3",
    }), [active])

    className = cl(
        "SectionTopFaults",
        "fk-bg-light-xs",
        className,
    )

    return (
        isBrowser && renderIf({
            isHide: true,
            condition: isLength(iFaults),
            children: (
                <Section 
                    header={header}
                    className={className}
                >
                    {Object.keys(iFaults).map((catSlug, index) => (
                        <TopFaults
                            key={index}
                            cardsVariant="light"
                            className={cl(
                                {"active": catSlug === active}
                            )}
                            faultsCat={catSlug}
                            showMore={showMore}
                        />
                    ))}
                </Section>
            )
        })
    )

    function getButtons() {
        return (
            <div className="SectionTopFaults__buttons">
                <Group
                    alignX="center"
                >
                    {Object.keys(iFaults).map((catSlug, index) => (
                        <Button
                            key={index}
                            title={cats[catSlug]}
                            variant={cl(
                                {"prim-linear": catSlug === active},
                                {"link-dark": catSlug !== active},
                            )}
                            onClick={() => setActive(catSlug)}
                        />
                    ))}
                </Group>
            </div>
        )
    }
}

export default SectionTopFaults