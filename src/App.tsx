import React from 'react'
import { Switch, Route } from 'react-router-dom'

import ComponentApp from './components/App'
import DetermineCurrentCity from './components/DetermineCurrentCity'
import RouteMain from './routes/Main'
import RouteActions from './routes/Actions'
import RouteService from './routes/Service'
import RouteProducts from './routes/Products'
import RouteProduct from './routes/Product'
import RouteArticles from './routes/Articles'
import RouteArticle from './routes/Article'
import RoutePage from './routes/Page'
import RouteContacts from './routes/Contacts'
import Route404 from './routes/404/index'

import { MAIN, ACTIONS, SERVICE, PRODUCTS, PRODUCT, ARTICLES, ARTICLE, PAGE, CONTACTS } from './routes/routes'


const App:React.FC<any> = ({ DL404 }) => (
    <ComponentApp>
        <DetermineCurrentCity/>
        <Switch>
            {DL404 && (
                <Route exact path={DL404} component={Route404}/>
            )}
            <Route exact path={MAIN} component={RouteMain}/>
            <Route exact path={ACTIONS} component={RouteActions}/>
            <Route path={`${SERVICE}/:postId`} component={RouteService}/>
            <Route exact path={PRODUCTS} component={RouteProducts}/>
            <Route exact path={PRODUCT} component={RouteProduct}/>
            <Route exact path={ARTICLES} component={RouteArticles}/>
            <Route path={`${ARTICLE}/:postId`} component={RouteArticle}/>
            <Route exact path={CONTACTS} component={RouteContacts}/>
            <Route path={`${PAGE}:postId`} component={RoutePage}/>
        </Switch>
    </ComponentApp>
)

export default App