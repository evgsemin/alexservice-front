export const LOAD_CITIES = "DetermineCurrentCity/LOAD_CITIES"
export const PUT_CITIES  = "DetermineCurrentCity/PUT_CITIES"
export const CHANGE_CITY = "DetermineCurrentCity/CHANGE_CITY"

export const loadCities = () => ({
    type: LOAD_CITIES
})

export const putCities = ({ cities }) => ({
    type: PUT_CITIES,
    payload: {
        cities
    }
})

export const changeCity = ({ cityId }) => ({
    type: CHANGE_CITY,
    payload: {
        cityId,
    }
})