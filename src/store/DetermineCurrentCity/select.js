import { createSelector } from 'reselect'
import { convertObjToArr } from '../../utilities/convert'


export const getCities    = (state) => state.Cities.cities || {}
export const getCity      = (state, cityId) => getCities(state)[cityId] || {}
export const getCurrentId = (state) => state.Cities.currentId || -1

export const cities = createSelector(
    [getCities],
    (cities) => cities
)

export const city = createSelector(
    [getCity],
    (city) => city
)

export const currentId = createSelector(
    [getCurrentId],
    (id) => id
)

export const currentCity = createSelector(
    [getCities, getCurrentId],
    (cities, currentId) => cities[currentId] || {}
)

export const idByTicket = createSelector(
    [
        cities,
        (state, ticket) => ticket
    ],
    (cities, ticket) => convertObjToArr(cities).filter(city => city.ticket === ticket.toLocaleLowerCase())[0]?.id || -1
)

export const robotsTxt = createSelector(
    [
        state => state,
        idByTicket
    ],
    (state, cityId) => getCity(state, cityId).robotsTxt || ""
)