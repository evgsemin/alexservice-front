import { PUT_CITIES, CHANGE_CITY } from './actions'

const initialState = {
    cities: {},
    currentId: -1,
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_CITIES:
            return {
                ...state,
                cities: payload.cities
            }

        case CHANGE_CITY:
            return {
                ...state,
                currentId: payload.cityId
            }
    }
    return state
}

export default reducer