import { PUT_PROMOS } from './actions'

const initialState = {
    promos: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_PROMOS:
            return {
                ...state,
                promos: payload.promos
            }
    }
    return state
}

export default reducer