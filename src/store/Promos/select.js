import { createSelector } from 'reselect'


export const getPromos = (state) => state.Promos.promos || {}

export const promos = createSelector(
    [getPromos],
    (promos) => promos
)