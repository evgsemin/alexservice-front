export const LOAD_PROMOS = "Promos/LOAD_PROMOS"
export const PUT_PROMOS  = "Promos/PUT_PROMOS"

export const loadPromos = () => ({
    type: LOAD_PROMOS
})

export const putPromos = ({ promos }) => ({
    type: PUT_PROMOS,
    payload: {
        promos,
    }
})