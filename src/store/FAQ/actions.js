export const LOAD_FAQS = "FAQ/LOAD_FAQS"
export const PUT_FAQS  = "FAQ/PUT_FAQS"

export const loadFaqs = () => ({
    type: LOAD_FAQS,
})

export const putFaqs = ({ faqs }) => ({
    type: PUT_FAQS,
    payload: {
        faqs,
    }
})