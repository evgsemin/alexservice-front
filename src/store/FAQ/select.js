import { createSelector } from 'reselect'
import { isArr } from '../../utilities/is'


export const getFaqs = (state) => state.FAQ.faqs || {}

export const faqs = createSelector(
    [getFaqs],
    (faqs) => faqs
)

export const selectFaqs = createSelector(
    [
        getFaqs,
        (state, faqsIds) => faqsIds
    ],
    (faqs, faqsIds = []) => {
        const selected = []
        if(isArr(faqsIds)) {
            faqsIds.map(id => {
                if(faqs[id]) {
                    selected.push(faqs[id])
                }
            })
        }
        return selected
    }
)