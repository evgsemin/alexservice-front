import { PUT_FAQS } from './actions'

const initialState = {
    faqs: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_FAQS:
            return {
                ...state,
                faqs: payload.faqs
            }
    }
    return state
}

export default reducer