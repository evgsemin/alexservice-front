import { createSelector } from 'reselect'


export const getPricesTables = (state) => state.PricesTable.pricesTables || {}

export const pricesTables = createSelector(
    [getPricesTables],
    (pricesTables) => pricesTables
)

export const pricesTable = createSelector(
    [
        getPricesTables,
        (state, pricesTableId) => pricesTableId
    ],
    (pricesTables, pricesTableId) => pricesTables[pricesTableId] || {}
)