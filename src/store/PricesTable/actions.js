export const LOAD_PRICES_TABLES = "PricesTable/LOAD_PRICES_TABLES"
export const PUT_PRICES_TABLES  = "PricesTable/PUT_PRICES_TABLES"

export const loadPricesTables = () => ({
    type: LOAD_PRICES_TABLES,
})

export const putPricesTables = ({ pricesTables }) => ({
    type: PUT_PRICES_TABLES,
    payload: {
        pricesTables,
    }
})