import { PUT_PRICES_TABLES } from './actions'

const initialState = {
    pricesTables: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_PRICES_TABLES:
            return {
                ...state,
                pricesTables: payload.pricesTables
            }
    }
    return state
}

export default reducer