import { PUT_BRANDS } from './actions'

const initialState = {
    brands: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_BRANDS:
            return {
                ...state,
                brands: payload.brands
            }
    }
    return state
}

export default reducer