export const LOAD_BRANDS = "Brands/LOAD_BRANDS"
export const PUT_BRANDS  = "Brands/PUT_BRANDS"

export const loadBrands = () => ({
    type: LOAD_BRANDS,
})

export const putBrands = ({ brands }) => ({
    type: PUT_BRANDS,
    payload: {
        brands,
    }
})