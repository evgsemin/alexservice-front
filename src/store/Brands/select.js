import { createSelector } from 'reselect'
import { convertObjToArr } from '../../utilities/convert'

export const getBrands = (state) => state.Brands.brands || {}

export const brand = createSelector(
    [
        getBrands,
        (state, brandId) => brandId
    ],
    (brands, brandId) => brands[brandId] || {}
)

export const brandLogoBySize = createSelector(
    [
        getBrands,
        (state, brandId, logoSize) => ({ brandId, logoSize })
    ],
    (brands, { brandId, logoSize }) => ((brands[brandId] || {}).logo || {})[logoSize] || ""
)

export const brands = createSelector(
    [getBrands],
    (brands) => brands
)

export const brandsArr = createSelector(
    [getBrands],
    (brands) => convertObjToArr(brands)
)