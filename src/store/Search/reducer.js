import { PUT_RESULTS } from './actions'

const initialState = {
    results: []
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_RESULTS:
            return {
                ...state,
                results: payload.results
            }
    }
    return state
}

export default reducer