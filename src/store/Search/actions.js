export const LOAD_RESULTS = "Search/LOAD_RESULTS"
export const PUT_RESULTS  = "Search/PUT_RESULTS"

export const loadResults = ({ phrase }) => ({
    type: LOAD_RESULTS,
    payload: {
        phrase
    }
})

export const putResults = ({ results }) => ({
    type: PUT_RESULTS,
    payload: {
        results
    }
})