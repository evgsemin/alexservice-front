import { createSelector } from 'reselect'


export const getResults = (state) => state.Search.results || []

export const results = createSelector(
    [getResults],
    (results) => results
)