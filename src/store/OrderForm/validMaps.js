import { isStr } from '../../utilities/is'


export const validMaps = {
    Service: {
        typeTitle: {
            required: true,
            type: "string",
            min: 2,
        },
        title: {
            required: true,
            type: "string",
            min: 2,
        },
        phone: {
            required: true,
            type: "string",
            func: (val) => {
                if(!isStr(val)) return false
                return val.indexOf("+7") !== -1 && val.indexOf("_") === -1
            }
        }
    },
    Corp: {
        typeTitle: {
            required: true,
            type: "string",
            min: 2,
        },
        title: {
            required: true,
            type: "string",
            min: 2,
        },
        phone: {
            required: true,
            type: "string",
            func: (val) => {
                if(!isStr(val)) return false
                return val.indexOf("+7") !== -1 && val.indexOf("_") === -1
            }
        }
    }
}