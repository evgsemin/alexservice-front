import { createSelector } from 'reselect'


export const getFormFields = (state, formId) => state.OrderForm.formsFields[formId] || {}

export const formFields = createSelector(
    [getFormFields],
    (fields) => fields
)