import { UPDATE_STATE, CLEAR_STATE } from './actions'
import { map } from '../../utilities/map'


const initialState = {
    formsFields: {},
}

const reducer = (state = initialState, { type, payload }) => {
    const nextState = { ...state }

    switch(type) {

        case UPDATE_STATE:
            nextState.formsFields[payload.formId] = {
                ...(nextState.formsFields[payload.formId] || {}),
                ...payload.fields,
            }
            return nextState

        case CLEAR_STATE:
            nextState.formsFields[payload.formId] = map(
                nextState.formsFields[payload.formId],
                (val, key) => [key, ""],
            )

            return nextState
    }
    return state
}

export default reducer