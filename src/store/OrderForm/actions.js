export const SEND         = "OrderForm/SEND"
export const UPDATE_STATE = "OrderForm/UPDATE_STATE"
export const CLEAR_STATE  = "OrderForm/CLEAR_STATE"

export const send = ({ formId, typeTitle, title }) => ({
    type: SEND,
    payload: {
        formId,
        typeTitle,
        title: title || typeTitle,
    }
})

export const updateState = ({ formId, fields }) => ({
    type: UPDATE_STATE,
    payload: {
        formId,
        fields,
    }
})

export const clearState = ({ formId }) => ({
    type: CLEAR_STATE,
    payload: {
        formId,
    }
})