import { CHANGE_EVENT_STATUS } from './actions'

const initialState = {}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case CHANGE_EVENT_STATUS:
            let next = [...(state[payload.type] || [])]

            // Включаем событие
            if(payload.status === true) {
                next.push(payload.name)
            }

            // Отключаем событие
            else {
                const namePosition = next.indexOf(payload.name)
                if(namePosition !== -1) {
                    next.splice(namePosition, 1)
                }
            }

            next = [...new Set(next)]

            return {
                ...state,
                [payload.type]: next,
            }
    }
    return state
}

export default reducer