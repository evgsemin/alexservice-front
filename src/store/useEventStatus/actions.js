export const CHANGE_EVENT_STATUS = "useEventStatus/CHANGE_EVENT_STATUS"

export const changeEventStatus = ({ type, name, status }) => ({
    type: CHANGE_EVENT_STATUS,
    payload: {
        type,
        name,
        status,
    }
})