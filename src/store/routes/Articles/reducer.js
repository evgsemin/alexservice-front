import { PUT_ARTICLES } from './actions'

const initialState = {
    articles: []
}

const reducer = (state = initialState, { type, payload }) => {

    switch(type.action) {
        
        case PUT_ARTICLES:
            return {
                ...state,
                articles: payload.articles
            }
    }

    return state
}

export default reducer