export const LOAD_ARTICLES = "RouteArticles/LOAD_ARTICLES"
export const PUT_ARTICLES = "RouteArticles/PUT_ARTICLES"

export const loadArticles = () => ({
    type: LOAD_ARTICLES
})

export const putArticles = ({ articles }) => ({
    type: PUT_ARTICLES,
    payload: {
        articles,
    }
})