import { createSelector } from 'reselect'
import { convertObjToArr } from '../../utilities/convert'

export const getActions = (state) => state.Actions.actions || {}

export const actions = createSelector(
    [getActions],
    (actions) => actions
)

export const actionsArr = createSelector(
    [getActions],
    (actions) => convertObjToArr(actions)
)