import { PUT_ACTIONS } from './actions'

const initialState = {
    actions: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_ACTIONS:
            return {
                ...state,
                actions: payload.actions
            }
    }
    return state
}

export default reducer