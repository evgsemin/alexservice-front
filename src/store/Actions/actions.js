export const LOAD_ACTIONS = "Actions/LOAD_ACTIONS"
export const PUT_ACTIONS  = "Actions/PUT_ACTIONS"

export const loadActions = () => ({
    type: LOAD_ACTIONS,
})

export const putActions = ({ actions }) => ({
    type: PUT_ACTIONS,
    payload: {
        actions,
    }
})