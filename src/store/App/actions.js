export const DID_MOUNT    = "App/DID_MOUNT"
export const UPDATE_PAGES = "App/UPDATE_PAGES"
export const PUT_PAGES    = "App/PUT_PAGES"

export const didMount = () => ({
    type: DID_MOUNT
})

export const updatePages = () => ({
    type: UPDATE_PAGES,
})

export const putPages = ({ pages }) => ({
    type: PUT_PAGES,
    payload: {
        pages
    }
})