import { createSelector } from 'reselect'
import { map } from '../../utilities/map'

export const allPages = (state) => state.App.pages || {}

export const pageBySlug = createSelector(
    [
        allPages,
        (state, slug) => slug
    ],
    (pages, slug) => {
        let page = pages[slug] || pages[`/${slug}`]

        if(!page) {
            map(pages, (appPage, path) => {
                if(path.slice(0-slug.length) === slug) {
                    page = appPage
                }
            })
        }

        return page || {}
    }
)