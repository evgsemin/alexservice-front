import { map } from '../../utilities/map'

import { allServices as getAllServices, allArticles as getAllArticles, allPages as getAllPages } from '../Posts/select'
import { currentCity } from '../DetermineCurrentCity/select'
import { MAIN, ACTIONS, CONTACTS, ARTICLES, SERVICE, ARTICLE, PAGE } from '../../routes/routes'


export function getAppPages(state) {
    const city = currentCity(state)

    const appPages = {
        [MAIN]: {
            cityData: city.seo,
            href: MAIN,
        },
        [CONTACTS]: {
            title: "Контакты",
            postTitle: "Контакты",
            cityData: city.seo,
            href: CONTACTS,
        },
        [ARTICLES]: {
            title: "Блог",
            postTitle: "Блог",
            cityData: city.seo,
            href: ARTICLES,
        },
        [ACTIONS]: {
            title: "Акции",
            postTitle: "Акции",
            cityData: city.seo,
            href: ACTIONS,
        },
        "404": {
            title: "Страница не найдена",
            postTitle: "Страница не найдена",
            cityData: city.seo,
            href: "",
        }
    }

    map(getAllServices(state), (post, postSlug) => {
        const pagePath = `${SERVICE}/${postSlug}`
        appPages[pagePath] = {
            title: post.title,
            postTitle: post.title,
            seoData: post.seo,
            cityData: city.seo,
            href: pagePath,
        }
    })
    map(getAllArticles(state), (post, postSlug) => {
        const pagePath = `${ARTICLE}/${postSlug}`
        appPages[pagePath] = {
            title: post.title,
            postTitle: post.title,
            seoData: post.seo,
            cityData: city.seo,
            href: pagePath,
        }
    })
    map(getAllPages(state), (post, postSlug) => {
        const pagePath = `${PAGE}${postSlug}`
        appPages[pagePath] = {
            title: post.title,
            postTitle: post.title,
            seoData: post.seo,
            cityData: city.seo,
            href: pagePath,
        }
    })

    return appPages
}