import { MAIN, CONTACTS, ARTICLES } from '../../routes/routes'
import { PUT_PAGES } from './actions'

const initialState = {
    pages: {}
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_PAGES:
            return {
                ...state,
                pages: payload.pages
            }
    }
    return state
}

export default reducer