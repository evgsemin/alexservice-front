import { createSelector } from 'reselect'
import { pageBySlug } from '../App/select'
import { isBrowser } from '../../utilities/is'
import { length } from '../../utilities/length'


export const breadcrumbs = (state) => {
    let path
    let domain

    try {
        if(isBrowser()) {
            path = window.location.pathname
            domain = window.location.origin
        } else {
            path = process.fk.path
            domain = process.fk.host
        }
    } catch(e) {
        path = process.fk.path
        domain = process.fk.host
    }

    const breadcrumbs = [
        {title: "Главная", href: domain},
    ]

    const [empty, pageName, postId] = path.split("/")

    // Внутренняя страница
    if(pageName) {
        const pageData = pageBySlug(state, pageName)
        if(length(pageData)) breadcrumbs.push({
            title: pageData.title,
            href: `${domain}${pageData.href}`
        })

        if(length(postId)) {
            const postData = pageBySlug(state, postId)
            if(length(postData)) breadcrumbs.push({
                title: postData.title,
                href: `${domain}${postData.href}`
            })
        }
    }
    
    return breadcrumbs
}