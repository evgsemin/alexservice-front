import { combineReducers } from 'redux'

import ActionsReducer from './Actions/reducer'
import AppReducer from './App/reducer'
import BrandsReducer from './Brands/reducer'
import DetermineCurrentCityReducer from './DetermineCurrentCity/reducer'
import FAQReducer from './FAQ/reducer'
import OrderFormReducer from './OrderForm/reducer'
import PostsReducer from './Posts/reducer'
import PricesTableReducer from './PricesTable/reducer'
import PromosReducer from './Promos/reducer'
import SearchReducer from './Search/reducer'
import TopFaultsReducer from './TopFaults/reducer'
import EventStatus from './useEventStatus/reducer'
import WorkHoursReducer from './WorkHours/reducer'

import RouteArticlesReducer from './routes/Articles/reducer'


export const rootReducer = combineReducers({
    Actions: ActionsReducer,
    App: AppReducer,
    Brands: BrandsReducer,
    Cities: DetermineCurrentCityReducer,
    FAQ: FAQReducer,
    OrderForm: OrderFormReducer,
    Posts: PostsReducer,
    PricesTable: PricesTableReducer,
    Promos: PromosReducer,
    Search: SearchReducer,
    TopFaults: TopFaultsReducer,
    EventStatus: EventStatus,
    WorkHours: WorkHoursReducer,

    RouteArticles: RouteArticlesReducer,
})