export const LOAD_WORKS_HOURS = "WorkHours/LOAD_WORKS_HOURS"
export const PUT_WORKS_HOURS  = "WorkHours/PUT_WORKS_HOURS"

export const loadWorksHours = () => ({
    type: LOAD_WORKS_HOURS,
})

export const putWorksHours = ({ worksHours }) => ({
    type: PUT_WORKS_HOURS,
    payload: {
        worksHours,
    }
})