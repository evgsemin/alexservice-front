import { PUT_WORKS_HOURS } from './actions'

const initialState = {
    worksHours: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_WORKS_HOURS:
            return {
                ...state,
                worksHours: payload.worksHours
            }
    }
    return state
}

export default reducer