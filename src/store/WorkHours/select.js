import { createSelector } from 'reselect'


export const getWorksHours = (state) => state.WorkHours.worksHours || {}

export const worksHours = createSelector(
    [getWorksHours],
    (worksHours) => worksHours
)

export const workHours = createSelector(
    [
        getWorksHours,
        (state, workHoursId) => workHoursId
    ],
    (worksHours, workHoursId) => worksHours[workHoursId] || {}
)