import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { rootReducer } from './reducer'

export const cities = {
    "moscow": {
        id: 14,
        store: getStore()
    },
    "krasnodar": {
        id: 110,
        store: getStore()
    }
}

function getStore() {
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore(
        rootReducer,
        applyMiddleware(sagaMiddleware),
    )
    store.runSagas = sagaMiddleware.run
    return store
}