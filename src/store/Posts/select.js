import { createSelector } from 'reselect'
import { convertObjToArr } from '../../utilities/convert'
import { map } from '../../utilities/map'


export const getAllServices = (state) => state.Posts.services || {}
export const getAllArticles = (state) => state.Posts.articles || {}
export const getAllPages    = (state) => state.Posts.pages || {}
export const getArticle     = (state, postId) => getAllArticles(state)[postId] || {}
export const getPage        = (state, postId) => getAllPages(state)[postId] || {}

// services

export const allServices = createSelector(
    [getAllServices],
    (services) => {
        const converted = convertObjToArr(services)
        return converted.length ? Object.assign(...converted) : {}
    }
)

export const allServicesArr = createSelector(
    [allServices],
    (servicesObj) => convertObjToArr(servicesObj)
)

export const servicesParentsWithCats = createSelector(
    [getAllServices],
    (servicesCats) => {
        const answer = {}
        map(servicesCats, (cat, catName) => {
            answer[catName] = {}
            map(cat, (service, serviceKey) => {
                if(!service.isChild) {
                    answer[catName][serviceKey] = service
                }
            })
        })
        return answer
    }
)

export const service = createSelector(
    [
        allServices,
        (state, postId) => postId
    ],
    (services, postId) => services[postId] || {}
)

export const serviceChilds = createSelector(
    [
        service,
        allServicesArr,
        (state, postId) => postId
    ],
    (parent, servicesArr) => {
        if(!parent.id) return []
        
        const parentId = parent.id

        const filtered = servicesArr.filter(service => (service.isChild && service.parentId === parentId))
        return filtered
    }
)

export const serviceBrands = createSelector(
    [
        service,
        (state, postId) => state
    ],
    (service, state) => {
        const childs = serviceChilds(state, service.parentPostId || service.postId)
        const brands = []

        childs.map(child => brands.push({
            id: child.assocBrand,
            title: child.title,
            href: child.href,
        }))

        return brands
    }
)

export const servicesMenu = createSelector(
    [servicesParentsWithCats],
    (servicesCats) => {
        const menu = []

        map(servicesCats, (cat, catName) => {
            const links = [
                ...convertObjToArr(cat).map(service => ({
                    title: service.title,
                    image: service.cover,
                    href: service.href,
                    type: service.typeSlug,
                }))
            ]

            menu.push({
                title: catName,
                type: links[0]?.type,
                links,
            })
        })

        return menu
    }
)

export const serviceMenuByType = createSelector(
    [
        servicesMenu,
        (state, type) => type,
    ],
    (serviceMenu, type) => {
        let answer = {}

        serviceMenu.map(menu => {
            if(menu.type === type) {
                answer = menu
            }
        })

        return answer
    }
)

// articles

export const allArticles = createSelector(
    [getAllArticles],
    (articles) => articles
)

export const allArticlesArr = createSelector(
    [allArticles],
    (articles) => convertObjToArr(articles)
)

export const article = createSelector(
    [getArticle],
    (article) => article
)

// pages

export const allPages = createSelector(
    [getAllPages],
    (pages) => pages
)

export const allPagesArr = createSelector(
    [allPages],
    (pages) => convertObjToArr(pages)
)

export const page = createSelector(
    [getPage],
    (page) => page
)

// posts

export const postByPostId = createSelector(
    [allServices, allArticles, allPages, (state, postId) => postId],
    (services, articles, pages, postId) => {
        return services[postId] || articles[postId] || pages[postId] || {}
    }
)