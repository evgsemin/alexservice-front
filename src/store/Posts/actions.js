export const LOAD_SERVICES           = "Posts/LOAD_SERVICES"
export const PUT_SERVICES            = "Posts/PUT_SERVICES"
export const LOAD_ARTICLES           = "Posts/LOAD_ARTICLES"
export const PUT_ARTICLES            = "Posts/PUT_ARTICLES"
export const CREATE_RELATED_SERVICES = "Posts/CREATE_RELATED_SERVICES"
export const CREATE_RELATED_ARTICLES = "Posts/CREATE_RELATED_ARTICLES"
export const LOAD_PAGES              = "Posts/LOAD_PAGES"
export const PUT_PAGES               = "Posts/PUT_PAGES"

// services

export const loadServices = () => ({
    type: LOAD_SERVICES,
})

export const putServices = ({ services, }) => ({
    type: PUT_SERVICES,
    payload: {
        services,
    }
})

// articles

export const loadArticles = () => ({
    type: LOAD_ARTICLES,
})

export const putArticles = ({ articles, }) => ({
    type: PUT_ARTICLES,
    payload: {
        articles,
    }
})

export const createRelatedServices = () => ({
    type: CREATE_RELATED_SERVICES,
})

export const createRelatedArticles = () => ({
    type: CREATE_RELATED_ARTICLES,
})

// pages

export const loadPages = () => ({
    type: LOAD_PAGES,
})

export const putPages = ({ pages, }) => ({
    type: PUT_PAGES,
    payload: {
        pages,
    }
})