import { PUT_SERVICES, PUT_ARTICLES, PUT_PAGES } from './actions'

const initialState = {
    services: {},
    articles: {},
    pages: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_SERVICES:
            return {
                ...state,
                services: payload.services
            }

        case PUT_ARTICLES:
            return {
                ...state,
                articles: payload.articles
            }

        case PUT_PAGES:
            return {
                ...state,
                pages: payload.pages
            }
    }
    return state
}

export default reducer