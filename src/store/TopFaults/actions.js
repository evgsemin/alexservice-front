export const LOAD_FAULTS     = "TopFaults/LOAD_FAULTS"
export const PUT_FAULTS      = "TopFaults/PUT_FAULTS"
export const PUT_FAULTS_CATS = "TopFaults/PUT_FAULTS_CATS"

export const loadFaults = () => ({
    type: LOAD_FAULTS
})

export const putFaults = ({ faults }) => ({
    type: PUT_FAULTS,
    payload: {
        faults
    }
})

export const putFaultsCats = ({ cats }) => ({
    type: PUT_FAULTS_CATS,
    payload: {
        cats
    }
})