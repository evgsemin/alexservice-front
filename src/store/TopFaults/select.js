import { createSelector } from 'reselect'

import { isArr } from '../../utilities/is'
import { convertObjToArr } from '../../utilities/convert'
import { map } from '../../utilities/map'
import { length } from '../../utilities/length'


export const getFaults = (state) => state.TopFaults.faults || {}
export const getCats   = (state) => state.TopFaults.cats || {}

export const faults = createSelector(
    [getFaults],
    (faults) => faults
)

export const allFaults = createSelector(
    [getFaults],
    (faults) => {
        const converted = convertObjToArr(faults)
        return converted.length ? Object.assign({}, ...converted) : {}
    }
)

export const faultsByCat = createSelector(
    [
        getFaults,
        (state, catSlug) => catSlug
    ],
    (faults, catSlug) => {
        if(!catSlug) return []
        return convertObjToArr(faults[catSlug])
    }
)

export const faultsByIds = createSelector(
    [
        allFaults,
        (state, faultsIds) => faultsIds
    ],
    (faults, faultsIds) => {
        if(!isArr(faultsIds)) return []

        const filtered = []

        faultsIds.map(id => {
            if(faults[id]) {
                filtered.push(faults[id])
            }
        })

        return filtered
    }
)

export const cats = createSelector(
    [getCats],
    (cats) => cats
)

export const catNameBySlug = createSelector(
    [
        getCats,
        (state, slug) => slug
    ],
    (cats, slug) => cats[slug] || ""
)