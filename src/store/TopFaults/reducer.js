import { PUT_FAULTS, PUT_FAULTS_CATS } from './actions'

const initialState = {
    faults: {},
    cats: {},
}

const reducer = (state = initialState, { type, payload }) => {
    switch(type) {

        case PUT_FAULTS:
            return {
                ...state,
                faults: { ...payload.faults }
            }

        case PUT_FAULTS_CATS:
            return {
                ...state,
                cats: { ...payload.cats }
            }
    }
    return state
}

export default reducer