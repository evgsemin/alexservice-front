declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__: any
    }
}

export interface INavLink {
    title:string
    seoTitle?:string
    href:string
    target?:string
    links:INavLink[]
}

export interface IImageProductGallery {
    className?:string
    image:string
    thumb?:string
    alt?:string
}

export interface IProductShort {
    id:number
    image:string
    images?:IImageProductGallery[]
    recommend?:boolean
    title:string
    short?:string
    price:string
    href:string
    [key:string]:any
}

export interface IProduct extends IProductShort {
    description?:any
}