import React, { useEffect, useRef } from 'react'

import { safeCall } from '../../utilities/safeCall'
import { clearHTMLTags } from '../../utilities/clearHTMLTags'


// setRef = func(ref)
// children = string

const Div:React.FC<any> = (props) => {
    const refDiv = useRef<any>()

    useEffect(() => {
        safeCall(props.setRef, refDiv)
    }, [props.setRef])

    useEffect(() => {
        if(refDiv.current) {
            if(typeof props.children === "string") {
                refDiv.current.innerHTML = ""
                refDiv.current.insertAdjacentHTML("afterbegin", props.children)
            }
        }
    }, [props.children])

    const divAttrs = {...props}
    delete divAttrs.setRef
    delete divAttrs.children
    
    return (
        props.setRef
            ? <div {...divAttrs} ref={props.setRef}>
                <div ref={refDiv}>{clearHTMLTags(props.children)}</div>
            </div>
            : <div {...divAttrs} ref={refDiv}>{clearHTMLTags(props.children)}</div>
    )
}

export default Div