import React from 'react'
import { outside } from '../../utilities/outside'


const AppOutside: React.FC = () => (
    <div className="AppOutside">
        {outside(
            <>
            </>
        )}   
    </div>
)

export default AppOutside