import React from 'react'

import { cl } from '../../utilities/cl'
import { isFunc } from '../../utilities/is'
import { safeCall } from '../../utilities/safeCall'

import SVG from '../FK/SVG'


const IconMarker:React.FC<{
    className?:string
    marker: "recommend"
    size?:string
    type?: "text" | "icon"
    title?:string
    onClick?:any
}> = ({
    className,
    marker,
    size = "md",
    type = "icon",
    title,
    onClick,
}) => {

    const hClick = (e:any) => {
        safeCall(onClick, e)
    }

    className = cl(
        "IconMarker",
        `IconMarker_marker-${marker}`,
        `IconMarker_size-${size}`,
        `IconMarker_type-${type}`,
        {"IconMarker_cursor-pointer": isFunc(onClick)},
        className,
    )

    const markerTitle = cl(
        {[`${title}`]: typeof title === "string"},
        {"Рекомендовано": (marker === "recommend" && typeof title !== "string")},
    )

    return (
        <div className={className} title={markerTitle} onClick={hClick}>
            {type === "icon" && getMarkerIcon()}
            {type === "text" && getMarkerText()}
        </div>
    )

    function getMarkerIcon() {
        if(marker === "recommend")
            return <SVG icon="Approved" variant="light"/>
        return null
    }

    function getMarkerText() {
        if(marker === "recommend")
            return (
                <>
                    <SVG icon="Approved" variant="light" size="auto"/>
                    {markerTitle}
                </>
            )
        return null
    }
}

export default IconMarker