import React from 'react'

import { cl } from '../../utilities/cl'

import CTADouble from './types/Double'


const CTA:React.FC<{
    className?:string
    type:string
    title?:string
    subtitle?:string
}> = ({
    className,
    type,
    title = "Закажите ремонт своей техники сейчас",
    subtitle = "И получите диагностику другой 1-ой единицы техники в подарок!",
}) => {

    className = cl(
        "CTA",
        className,
    )

    return (
        <section className={className}>
            {type === "double" && (
                <CTADouble
                    title={title}
                />
            )}
        </section>
    )
}

export default CTA