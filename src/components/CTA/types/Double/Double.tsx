import React from 'react'

import Button from '../../../FK/Button'

import OrderFormService from '../../../OrderForm/Service'


const Double:React.FC<{
    title:string
    mainPhone:string
    openingHours:any
}> = ({
    title,
    mainPhone,
    openingHours,
}) => {

    return (
        <section className="CTADouble">
            <div className="CTADouble__left">
                <div className="wrap">
                    {title && (
                        <h2 className="title fk-text-inverse">
                            {title}
                        </h2>
                    )}
                    <Button
                        className="phone"
                        title={mainPhone}
                        href={`tel:${mainPhone}`}
                        variant="prim"
                        size="lg"
                        iconBefore="Phone"
                    />
                    <div className="or">
                        или
                    </div>
                    <OrderFormService
                        fieldNameOn={false}
                        buttonProps={{
                            title: "Заказать звонок"
                        }}
                    />
                </div>
            </div>
            <div className="CTADouble__right">
                <p>Наша компания выполняет ремонт любой техники в представленных регионах.</p>
                <ul>
                    <li>Работаем ежедневно с {openingHours.from} до {openingHours.to}. В нерабочие часы вы можете оставить заявку. В этом случае мы обязательно перезвоним вам утром.</li>
                    <li>По окончанию работы специалист выдаст гарантийный талон на срок от 6 до 24 месяцев.</li>
                    <li>Возможен выезд в течении 15 минут.</li>
                </ul>
            </div>
        </section>
    )
}

export default Double