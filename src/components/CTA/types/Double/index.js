import React from 'react'
import { connect } from 'react-redux'

import { currentCity } from '../../../../store/DetermineCurrentCity/select'

import Double from './Double'


const DoubleContainer = props => (
    <Double
        title={props.title}
        mainPhone={props.mainPhone}
        openingHours={props.openingHours}
    />
)

const stateToProps = state => ({
    mainPhone: currentCity(state).mainPhone,
    openingHours: currentCity(state).openingHours,
})

export default connect(stateToProps, null)(DoubleContainer)