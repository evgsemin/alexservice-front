import React from 'react'

import { cl } from '../../utilities/cl'
import { isNumb, isLength } from '../../utilities/is'
import { safeCall } from '../../utilities/safeCall'
import { IBrandItem } from '../BrandToggler/interfaces'

import BrandToggler from '../BrandToggler'
import PricesTable from '../PricesTable'
import WorkHours from '../WorkHours'


const rendered = {}

const PricesByBrand:React.FC<{
    className?:string
    pricesTableId:number
    workHoursId?:number
    active:number
    brands:IBrandItem[]
    onRender?:any
}> = ({
    className,
    pricesTableId,
    workHoursId,
    active,
    brands,
    onRender,
}) => {

    className = cl(
        "PricesByBrand",
        className
    )

    const hRender = (name, mode) => {
        rendered[name] = mode
        changeOnRender()
    }

    return (
        <div className={className}>
            <BrandToggler {...{
                active,
                brands,
                onRender: (mode) => hRender("BrandToggler", mode),
            }}/>

            {isNumb(pricesTableId) && (
                <PricesTable
                    pricesTableId={pricesTableId}
                    onRender={(mode) => hRender("PricesTable", mode)}
                />
            )}
            {isNumb(workHoursId) && (
                <WorkHours
                    className="fk-text-offsets-p"
                    workHoursId={workHoursId}
                    onRender={(mode) => hRender("WorkHours", mode)}
                />
            )}
        </div>
    )

    function changeOnRender() {
        if(!isLength(rendered)) return

        for(let name in rendered) {
            if(rendered[name] === true) {
                safeCall(onRender, true)
                return
            }
        }
        safeCall(onRender, false)
    }
}

export default PricesByBrand