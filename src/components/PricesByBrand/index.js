import React from 'react'

import PricesByBrand from './PricesByBrand'


const PricesByBrandContainer = props => (
    <PricesByBrand
        className={props.className}
        pricesTableId={props.pricesTableId}
        workHoursId={props.workHoursId}
        active={props.active}
        brands={props.brands}
        onRender={props.onRender}
    />
)

export default PricesByBrandContainer