import React from 'react'
import { connect } from 'react-redux'

import { loadPricesTables } from '../../store/PricesTable/actions'
import { pricesTable } from '../../store/PricesTable/select'

import PricesTable from './PricesTable'


const PricesTableContainer = props => (
    <PricesTable
        className={props.className}
        pricesTable={props.pricesTable}
        prices={props.pricesTable.rows}
        loadPricesTables={props.loadPricesTables}
        onRender={props.onRender}
    />
)

const stateToProps = (state, props) => ({
    pricesTable: pricesTable(state, props.pricesTableId)
})

const actionsToProps = {
    loadPricesTables
}

export default connect(stateToProps, actionsToProps)(PricesTableContainer)