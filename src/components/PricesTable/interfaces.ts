export interface IPricesTable {
    name:string
    priceNumber?:string | number
    priceText?:string
}