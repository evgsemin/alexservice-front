import React, { useEffect } from 'react'

import { cl } from '../../utilities/cl'
import { isLength } from '../../utilities/is'
import { IPricesTable } from './interfaces'

import Table from '../FK/Table'
import { safeCall } from '../../utilities/safeCall'


const PricesTable:React.FC<{
    className?:string
    pricesTableId:number // используется в контейнере для вывода нужной таблицы
    prices:IPricesTable[]
    loadPricesTables:any
    onRender?:any
}> = ({
    className,
    prices = [],
    loadPricesTables,
    onRender,
}) => {

    useEffect(() => {
        loadPricesTables()
    }, [])

    useEffect(() => safeCall(onRender, isLength(prices)), [prices])

    className = cl(
        "PricesTable",
        className,
    )

    if(!isLength(prices)) return null

    return (
        <section className={className}>
            <Table
                size="sm-1"
                notBorder={true}
                table={{
                    thead: {
                        tr: [
                            {
                                items: [
                                    {title: "Вид работы"},
                                    {title: "Цена"},
                                ]
                            }
                        ]
                    },
                    tbody: {
                        tr: prices.map(tr => ({
                            items: [
                                {title: tr.name, type: "th"},
                                {title:
                                    (tr.priceNumber || tr.priceNumber === 0)
                                        ? `от ${tr.priceNumber} руб.`
                                        : tr.priceText
                                }
                            ]
                        }))
                    }
                }}
            />
        </section>
    )
}

export default PricesTable