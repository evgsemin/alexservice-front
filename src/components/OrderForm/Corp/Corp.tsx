import React, { useEffect, useMemo } from 'react'

import { cl } from '../../../utilities/cl'
import { isValidObj } from '../../../utilities/valid'
import { savedToStore, saveToStorage } from '../../../utilities/autofillFields'
import { validMaps } from '../../../store/OrderForm/validMaps'

import Group from '../../FK/Group'
import Input from '../../FK/Input'
import Button from '../../FK/Button'
import { useEventStatus } from '../../useEventStatus'


const Corp:React.FC<{
    className?:string
    formId:string
    size?:string
    buttonProps?:any
    groupProps?:any
    elementAfter?:any
    elementBefore?:any
    fields:any,
    updateState:any,
    send:any
}> = ({
    className,
    formId,
    size = "lg",
    buttonProps = {},
    groupProps = {},
    elementAfter,
    elementBefore,
    fields,
    updateState,
    send,
}) => {

    const sending = useEventStatus({
        type: "sending",
        name: "OrderForm"+formId,
    })

    const disabled = useMemo(() => {
        const validMap = (validMaps as any)[formId]
        delete validMap.typeTitle
        delete validMap.title
        return !isValidObj(validMap, fields)
    }, [fields])

    useEffect(() => {
        savedToStore(updateState)
    }, [])

    const hChange = ({value,name}:{ value:number, name:string }) => {
        const toStateObj = {
            formId,
            fields: {
                [name]: value
            }
        }

        updateState(toStateObj)
        saveToStorage(toStateObj)
    }

    const hSubmit = (e:any) => {
        e.preventDefault()
        send({
            formId,
            typeTitle: "Запрос о сотрудничестве",
        })
    }

    className = cl(
        "OrderFormCorp",
        className
    )

    return (
        <form onSubmit={hSubmit}>
            <Group
                direction="column"
                size="mr-1"
                stretch={true}
                {...groupProps}
            >
                {elementBefore && elementBefore}
                <Input
                    type="text"
                    name="name"
                    value={fields["name"]}
                    placeholder="Имя"
                    size={size}
                    onChange={hChange}
                />
                <Input
                    type="phone"
                    name="phone"
                    value={fields["phone"]}
                    placeholder="+7 (___) ___-__-__"
                    size={size}
                    onChange={hChange}
                />
                <Input
                    type="textarea"
                    name="message"
                    value={fields["message"]}
                    placeholder="Комментарий"
                    size={size}
                    onChange={hChange}
                />
                <Button
                    title="Отправить"
                    iconAfter="ArrowRight"
                    {...buttonProps}
                    size={size}
                    loading={sending}
                    disabled={disabled}
                />
                {elementAfter && elementAfter}
            </Group>
        </form>
    )
}

export default Corp