import React from 'react'
import { connect } from 'react-redux'

import { send, updateState } from '../../../store/OrderForm/actions'
import { formFields } from '../../../store/OrderForm/select'

import Corp from './Corp'


const formId = "Corp"

const CorpContainer = props => (
    <Corp
        className={props.className}
        formId={formId}
        size={props.size}
        buttonProps={props.buttonProps}
        groupProps={props.groupProps}
        elementAfter={props.elementAfter}
        elementBefore={props.elementBefore}
        fieldNameOn={props.fieldNameOn}

        fields={props.fields}

        updateState={props.updateState}
        send={props.send}
    />
)

const stateToProps = state => ({
    fields: formFields(state, formId)
})

const actionsToProps = {
    updateState,
    send,
}

export default connect(stateToProps, actionsToProps)(CorpContainer)