import React from 'react'

import { cl } from '../../utilities/cl'

import CardContent from '../Card/Content'

import OrderFormService from './Service'


const OrderFormAside:React.FC<{
    className?:string
}> = ({
    className
}) => {

    className = cl(
        "OrderFormAside",
        className
    )

    return (
        <CardContent
            className={className}
            variant="light"
            shadow="md"
        >
            <h5 className="title">Закажите ремонт своей техники сейчас</h5>
            <div className="subtitle">
                И получите диагностику другой 1-ой единицы техники в подарок!
            </div>
            <OrderFormService/>
        </CardContent>
    )
}

export default OrderFormAside