import React from 'react'

import { cl } from '../../utilities/cl'

import Section from '../FK/Section'

import OrderFormService from './Service'


const OrderFormSection:React.FC<{
    className?:string
}> = ({
    className,
}) => {

    className = cl(
        "OrderFormSection",
        className,
    )

    return (
        <Section
            className="fk-bg-prim"
            textVariant="inverse"
            containerSize="md"
            header={{
                title: <>Мы ремонтируем технику <b>ежедневно</b> с 2017 года</>,
                subtitle: <>
                    Нужна консультация? Звоните по номеру <a href="tel:+7 (905) 679-41-22">+7 (905) 679-41-22</a> <br/>
                    или оставьте заявку на обратный звонок
                </>,
            }}
        >
            <OrderFormService
                size="xl"
                groupProps={{
                    direction: "row",
                    alignX: "center",
                    alignY: "center",
                }}
                buttonProps={{
                    variant: "light",
                    title: "Заказать звонок",
                }}
            />
        </Section>
    )
}

export default OrderFormSection