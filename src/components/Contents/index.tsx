import React, { useEffect, useState } from 'react'

import { cl } from '../../utilities/cl'

import SVG from '../FK/SVG'


const Contents:React.FC<{
    className?:string
    offset?:number
    offsetElementSelector?:string
    refContent:any
    dependence:any
    titlesLevels?:any
}> = ({
    className,
    offset = 0,
    offsetElementSelector,
    refContent,
    dependence,
    titlesLevels = ["h2", "h3", "h4"],
}) => {

    const [opened, setOpened] = useState(true)
    const [links, setLinks] = useState<any>([])

    useEffect(updateLinks, [])
    useEffect(updateLinks, [dependence])

    const scrollToTitle = (e:any) => {
        try {
            if(e.target) {
                const linkIndex = Number(e.target.dataset.linkIndex)
    
                if(links[linkIndex]?.elem) {
                    let nextTitleOffset = links[linkIndex].elem.getBoundingClientRect().top + window.pageYOffset
                    let neededOffset = offset
                    
                    if(offsetElementSelector) {
                        const offsetElement = document.querySelector(offsetElementSelector)
                        if(offsetElement) {
                            const offsetElementH = offsetElement.clientHeight
                            neededOffset += offsetElementH
                        }
                    }
    
                    window.scrollTo(0, nextTitleOffset - neededOffset)
                }
            }
        }
        catch(e) {}
    }

    className = cl(
        "Contents",
        {"opened": opened},
        {"empty": links.length === 0},
        className,
    )

    return (
        <section className={className}>
            <header className="Contents__header" onClick={() => setOpened(!opened)}>
                <h4>Содержание</h4>
                <SVG
                    size="xs"
                    variant="dark"
                    icon="ArrowUp"
                />
            </header>

            <div className="Contents__links" onClick={scrollToTitle}>
                {links.map((link:any, index:number) => (
                    <div
                        key={index}
                        className="link"
                        data-link-index={index}
                    >
                        {link.title}
                    </div>
                ))}
            </div>
        </section>
    )

    function updateLinks() {
        if(refContent) {
            if(refContent.current) {
                const titles = [
                    ...(titlesLevels.indexOf("h1") !== -1 ? refContent.current.querySelectorAll("h1") : []),
                    ...(titlesLevels.indexOf("h2") !== -1 ? refContent.current.querySelectorAll("h2") : []),
                    ...(titlesLevels.indexOf("h3") !== -1 ? refContent.current.querySelectorAll("h3") : []),
                    ...(titlesLevels.indexOf("h4") !== -1 ? refContent.current.querySelectorAll("h4") : []),
                    ...(titlesLevels.indexOf("h5") !== -1 ? refContent.current.querySelectorAll("h5") : []),
                    ...(titlesLevels.indexOf("h6") !== -1 ? refContent.current.querySelectorAll("h6") : []),
                ]

                const nextLinks = titles.map(title => ({
                    title: title.innerHTML,
                    elem: title,
                }))

                setLinks(nextLinks)
            }
        }
    }
}

export default Contents