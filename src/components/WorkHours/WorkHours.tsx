import React, { useEffect } from 'react'

import { cl } from '../../utilities/cl'
import { isLength } from '../../utilities/is'
import { IWorks } from './interfaces'
import { safeCall } from '../../utilities/safeCall'

import SVG from '../FK/SVG'


const WorkHours:React.FC<{
    className?:string
    workHoursId:number // используется в контейнере
    works?:IWorks[]
    loadWorksHours:any
    onRender?:any
}> = ({
    className,
    works,
    loadWorksHours,
    onRender,
}) => {

    useEffect(() => {
        loadWorksHours()
    }, [])

    useEffect(() => safeCall(onRender, isLength(works)), [works])

    className = cl(
        "WorkHours",
        className,
    )

    if(!isLength(works)) return null

    return (
        <section className={className}>
            
            {works?.map((work, index) => (
                <section key={index} className="WorkHours__work">
                    <header className="header">
                        <h4>За {work.title} мы сделаем:</h4>
                    </header>
                    <div className="body">
                        <ul className="fk-text-skip">
                            {work.items.map((item, index) => (
                                <li key={index}>
                                    <SVG icon="Check"/>
                                    {item}
                                </li>
                            ))}
                        </ul>
                    </div>
                </section>
            ))}
        </section>
    )
}

export default WorkHours