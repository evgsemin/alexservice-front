import React from 'react'
import { connect } from 'react-redux'

import { loadWorksHours } from '../../store/WorkHours/actions'
import { workHours } from '../../store/WorkHours/select'

import WorkHours from './WorkHours'


const WorkHoursContainer = props => (
    <WorkHours
        className={props.className}
        works={props.workHours.rows}
        // works={[
        //     {
        //         title: "За 1-1.5 часа мы сделаем:",
        //         items: [
        //             "замена ножек",
        //             "замена сетевого шнура",
        //             "замена ручки люка",
        //             "замена дверцы люка",
        //             "замена УБЛ (устройства блокировки)",
        //             "устранение засоров",
        //             "замена сливного шланга",
        //             "замена аквастопа",
        //             "замена термостата"
        //         ]
        //     },
        //     {
        //         title: "За 2 часа мы сделаем:",
        //         items: [
        //             "замена манжеты люка",
        //             "замена ремня привода",
        //             "замена патрубков",
        //             "замена заливного / сливного клапана",
        //         ]
        //     },
        // ]}
        loadWorksHours={props.loadWorksHours}
        onRender={props.onRender}
    />
)

const stateToProps = (state, props) => ({
    workHours: workHours(state, props.workHoursId)
})

const actionsToProps = {
    loadWorksHours
}

export default connect(stateToProps, actionsToProps)(WorkHoursContainer)