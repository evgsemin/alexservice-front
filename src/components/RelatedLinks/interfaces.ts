export interface IRelatedLink {
    className?:string
    title:string
    href:string
    alt?:string
    attrs?:any
}