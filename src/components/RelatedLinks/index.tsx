import React from 'react'
import { Link } from 'react-router-dom'

import { cl } from '../../utilities/cl'
import { IRelatedLink } from './interfaces'

import SVG from '../FK/SVG'


const RelatedLinks:React.FC<{
    className?:string
    links:IRelatedLink[]
    labels?:boolean
}> = ({
    className,
    links = [],
    labels = true,
}) => {

    className = cl(
        "RelatedLinks",
        {"RelatedLinks_with-labels": labels},
        className,
    )

    return (
        <section className={className}>
            {links.map((link, index) => (
                <Link
                    key={index}
                    to={link.href}
                    title={link.alt || link.title}
                    className={cl(
                        "RelatedLinks__link",
                        "fk-text-skip",
                        link.className
                    )}
                >
                    {(index === 0 && links.length !== 1) && (
                        <div className="arrow-prev">
                            <SVG
                                title="Назад"
                                icon="ArrowLeft"
                                size="sm"
                                variant="dark"
                            />
                        </div>
                    )}
                    <div className="title">
                        {link.title}
                    </div>
                    {(index === links.length-1) && (
                        <div className="arrow-next">
                            <SVG
                                title="Далее"
                                icon="ArrowRight"
                                size="sm"
                                variant="dark"
                            />
                        </div>
                    )}
                </Link>
            ))}
        </section>
    )
}

export default RelatedLinks