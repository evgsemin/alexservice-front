import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { brandLogoBySize } from '../../store/Brands/select'
import { cl } from '../../utilities/cl'
import { isFunc, isLength } from '../../utilities/is'
import { safeCall } from '../../utilities/safeCall'
import { useIs } from '../../utilities/media'
import { IBrandItem } from './interfaces'

import Grid from '../FK/Grid'
import LazyImage from '../FK/LazyImage'
import Button from '../FK/Button'


const BrandToggler:React.FC<{
    className?:string
    state:any
    brands?:IBrandItem[]
    active?:number
    onChange?:any
    onRender?:any
}> = ({
    className,
    state,
    brands = [],
    active = -1,
    onChange,
    onRender,
}) => {

    const [activeBrand, setActiveBrand] = useState(active)
    const [showAll, setShowAll] = useState(false)
    const isDesktopAndUp = useIs("desktopAndUp")

    useEffect(() => {
        setActiveBrand(active)
    }, [active])

    useEffect(() => safeCall(onRender, isLength(brands)), [brands])

    const hClick = (e:any) => {
        if(isFunc(onChange)) {
            const item = e.target.closest(".BrandToggler__item")
            if(item) {
                e.preventDefault()
                const brandId = Number(item.dataset.brandId)
                if(!isNaN(brandId)) {
                    setActiveBrand(brandId)
                    safeCall(onChange, { value: brandId }, e)
                }
            }
        }
    }

    const hShowAll = () => {
        setShowAll(!showAll)
    }

    if(!isLength(brands)) return null

    className = cl(
        "BrandToggler",
        {"opened": showAll},
        {"actived": activeBrand !== -1},
        className,
    )

    return (
        <section className={className} onClick={hClick}>
            <nav className="BrandToggler__wrap">

                <h4 className="BrandToggler__title">
                    Выберите бренд
                </h4>

                <Grid
                    size={isDesktopAndUp ? "xs-1" : "sm"}
                    cols={{
                        desktopAndUp: 6,
                        tablet: 5,
                        mobileAndDown: 3
                    }}
                >
                    {brands.map((brand, index) => {
                        if(!showAll && (index+1) > 6) return null
                        
                        return (
                            <Link
                                key={brand.id}
                                className={cl(
                                    "BrandToggler__item",
                                    "fk-text-skip",
                                    {"active": brand.id === activeBrand},
                                    brand.className,
                                )}
                                to={brand.href}
                                title={brand.title}
                                data-brand-id={brand.id}
                            >
                                {
                                    // TODO: Картинка отображается не всегда
                                }
                                
                                <LazyImage
                                    type="div" 
                                    image={(brandLogoBySize as any)(state, brand.id, "medium")}
                                    format="md"
                                />
                            </Link>
                        )
                    })}
                </Grid>

                {brands.length > 6 && (
                    <Button
                        className="BrandToggler__show-all"
                        variant="prim-linear"
                        size="md"
                        title={showAll ? "Скрыть" : "Показать все"}
                        iconAfter="ArrowDown"
                        iconAfterSize="xs"
                        onClick={hShowAll}
                    />
                    // <div className="BrandToggler__show-all" onClick={hShowAll}>
                    //     {showAll ? "Скрыть" : "Показать все"}
                    //     <SVG icon="ArrowDown" size="xs" variant="dark"/>
                    // </div>
                )}
            </nav>
        </section>
    )
}

export default BrandToggler