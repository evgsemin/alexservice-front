export interface IBrandItem {
    className?:string
    id:number
    title:string
    href:string
}