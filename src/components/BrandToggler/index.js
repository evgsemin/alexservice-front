import React from 'react'
import { connect } from 'react-redux'

import BrandToggler from './BrandToggler'


const BrandTogglerContainer = props => (
    <BrandToggler
        className={props.className}
        active={props.active}
        state={props.state}
        brands={props.brands}
        onChange={props.onChange}
        onRender={props.onRender}
    />
)

const stateToProps = state => ({
    state,
})

export default connect(stateToProps, null)(BrandTogglerContainer)