import React from 'react'
import { connect } from 'react-redux'

import { loadBrands } from '../../store/Brands/actions'
import { loadServices, loadArticles, loadPages } from '../../store/Posts/actions'

import App from './App'


const AppContainer = props => (
    <App
        className={props.className}

        loadBrands={props.loadBrands}
        loadServices={props.loadServices}
        loadArticles={props.loadArticles}
        loadPages={props.loadPages}
    >
        {props.children}
    </App>
)

const actionsToProps = {
    loadBrands,
    loadServices,
    loadArticles,
    loadPages,
}

export default connect(null, actionsToProps)(AppContainer)