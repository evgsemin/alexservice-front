import React, { useEffect } from 'react'
import { useIsCitySelected } from '../DetermineCurrentCity/useIsCitySelected'

import AppOutside from '../AppOutside'


const App:React.FC<{
    loadBrands:any
    loadServices:any
    loadArticles:any
    loadPages:any
    children:any
}> = ({
    loadBrands,
    loadServices,
    loadArticles,
    loadPages,
    children
}) => {

    const isCurrentCitySelected = useIsCitySelected()

    useEffect(() => {
        if(isCurrentCitySelected) {
            loadServices()
            loadArticles()
            loadPages()
            loadBrands()
        }
    }, [isCurrentCitySelected])

    return (
        <>
            <div className="App">
                {children}
            </div>
    
            <AppOutside/>
        </>
    )
}

export default App