import React, { useEffect } from 'react'

import { cl } from '../../utilities/cl'
import { length } from '../../utilities/length'

import { IAccordionItem } from '../FK/Accordion/interfaces'

import Accordion from '../FK/Accordion'


const FAQ:React.FC<{
    className?:string
    faqsIds?:Number[] // используется в контейнере
    faqs?:IAccordionItem[]
    size?:string
    startOpened?:Array<number>
    loadFaqs:any
}> = ({
    className,
    faqs = [],
    size,
    startOpened,
    loadFaqs,
}) => {

    useEffect(() => {
        loadFaqs()
    }, [])

    if(!length(faqs)) return null

    className = cl(
        "FAQ",
        className,
    )

    return (
        <Accordion
            className={className}
            items={faqs}
            size={size}
            startOpened={startOpened}
        />
    )
}

export default FAQ