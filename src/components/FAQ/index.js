import React from 'react'
import { connect } from 'react-redux'

import { loadFaqs } from '../../store/FAQ/actions'
import { selectFaqs } from '../../store/FAQ/select'

import FAQ from './FAQ'


const FAQContainer = props => (
    <FAQ
        className={props.className}
        size={props.size}
        startOpened={props.startOpened}
        faqs={props.faqs}
        
        // faqs={[
        //     {title: "Как получить гарантию?", content: <p>По завершению ремонтных работ сотрудник обязательно оставит вам гарантийный талон на выполненные услуги. Срок гарантии зависит от типа поломки и составляет от 6 до 24 месяцев.</p>},
        //     {title: "Как получить скидку?", content: <><p>Для всех пенсионеров и многодетных семей мы предоставляем скидку в размере 20%. Периодически мы проводим дополнительные скидки и акции — вы всегда можете уточнить этот вопрос по телефону.</p><p>Также в случае ремонта специалист проводит диагностику бесплатно — это необходимая процедура, которая позволяет точно установить причину поломки. Она значительно повышает эффективность ремонта, и делает его дешевле. При отказе от ремонта диагностика оплачиваются согласно прайсу.</p></>},
        //     {title: "Скорость", content: <p>Мы понимаем, что поломки часто требуют оперативного решения, и поэтому всегда стараемся быть к вам ближе. Чаще всего, мы можем приступить к ремонту уже в течение часа.</p>},
        // ]}

        loadFaqs={props.loadFaqs}
    />
)

const stateToProps = (state, props) => ({
    faqs: selectFaqs(state, props.faqsIds)
})

const actionsToProps = {
    loadFaqs,
}

export default connect(stateToProps, actionsToProps)(FAQContainer)