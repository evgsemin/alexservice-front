export interface IFault {
    id:number
    title:string
    what:string
    tasks:Array<string>
    price:string | number
    hideCTAButton:boolean
}

export interface IFaultCard extends IFault {
    className?:string
    cardsVariant?:string
}