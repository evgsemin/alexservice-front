import React, { useEffect, useState } from 'react'
import { useIsCitySelected } from '../DetermineCurrentCity/useIsCitySelected'

import { cl } from '../../utilities/cl'
import { isLength, isNumb } from '../../utilities/is'
import { length } from '../../utilities/length'
import { safeCall } from '../../utilities/safeCall'
import { IFaultCard } from './interfaces'

import Grid from '../FK/Grid'
import Button from '../FK/Button'

import TopFaultsCard from './Card'


const TopFaults:React.FC<{
    className?:string
    cardsVariant?:string
    showMore?:number

    faultsIds?:string // используюется в TopFaultsContainer для вывода нужных. имеет приоритет над faultsCat
    faultsCat?:string // используюется в TopFaultsContainer для вывода нужных

    faults:IFaultCard[]
    gridProps?:any
    currentCityId:number

    onRender?:any
    loadFaults:any
}> = ({
    className,
    cardsVariant,
    showMore,
    faults = [],
    gridProps = {},
    onRender,
    loadFaults,
}) => {

    const isCitySelected = useIsCitySelected()

    const [iShowMore, setIShowMore] = useState(false)

    useEffect(() => {
        if(isCitySelected) {
            loadFaults()
        }
    }, [isCitySelected])

    useEffect(() => {
        safeCall(onRender, isLength(faults))
    }, [faults])

    className = cl(
        "TopFaults",
        {"fk-d-none": !isLength(faults)},
        className
    )

    if(!length(faults)) return null  

    if(!gridProps.cols) {
        gridProps = {
            ...gridProps,
            cols: {
                desktopAndUp: 4,
                tabletAndDown: 2,
                mobileAndDown: 1,
            }
        }
    }

    return (
        <div className={className}>
            <Grid {...gridProps}>
                {faults.map((card, index) => {

                    if(isNumb(showMore) && showMore < length(faults)) {
                        if(!(iShowMore || index+1 <= showMore))
                            return null
                    }

                    return (
                        <TopFaultsCard
                            key={card.id}
                            className={card.className}
                            cardsVariant={cardsVariant}
                            id={card.id}
                            title={card.title}
                            what={card.what}
                            tasks={card.tasks}
                            price={card.price}
                            hideCTAButton={card.hideCTAButton}
                        />
                    )
                })}
            </Grid>

            {(isNumb(showMore) && showMore < length(faults) && !iShowMore) && (
                <Button
                    className="TopFaults__show-more"
                    iconBefore="ArrowDown"
                    iconBeforeSize="mr-2"
                    title="Показать все"
                    size="xl"
                    variant="link-dark"
                    onClick={() => setIShowMore(true)}
                />
            )}
        </div>
    )
}

export default TopFaults