import React, { useState } from 'react'

import { cl } from '../../utilities/cl'
import { IFaultCard } from './interfaces'
import { isLength } from '../../utilities/is'
import { length } from '../../utilities/length'

import Button from '../FK/Button'
import SVG from '../FK/SVG'
import CardContent from '../Card/Content'


const TopFaultsCard:React.FC<IFaultCard> = ({
    className,
    cardsVariant,
    id,
    title,
    what,
    tasks,
    price,
    hideCTAButton,
}) => {

    const showMore = 4

    const [opened, setOpened] = useState(false)

    className = cl(
        "TopFaultsCard",
        className,
    )

    return (
        <CardContent
            className="TopFaultsCard"
            variant={cardsVariant}
        >
            <header className="header">
                <h4>{title}</h4>
            </header>

            {isLength(tasks) && (
                <div className="tasks">
                    {tasks.map((task, index) => {
                        if(!opened && index+1 > showMore) {
                            return null
                        }

                        return (
                            <div key={index}>
                                <SVG icon="Check"/>
                                {task}
                            </div>
                        )
                    })}

                    {(!opened && length(tasks) > showMore) && (
                        <div className="show-more" onClick={() => setOpened(true)}>
                            <SVG
                                icon="ArrowDown"
                                size="mr-1"
                                variant="dark"
                            />
                            Показать все
                        </div>
                    )}
                </div>
            )}

            <footer className="footer">
                <div className="price">
                    {price} руб.
                </div>
                {!hideCTAButton && (
                    <Button
                        title="Заказать"
                        iconBefore="Repair"
                        variant="prim"
                        size="md"
                        modalType="OrderRepair"
                        modalData={{
                            data: {
                                title: <>Заказать ремонт <b>{what}</b></>,
                            }
                        }}
                    />
                )}
            </footer>
        </CardContent>
    )
}

export default TopFaultsCard