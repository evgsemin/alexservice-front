import React from 'react'
import { connect } from 'react-redux'

import { loadFaults } from '../../store/TopFaults/actions'
import { faults, faultsByCat, faultsByIds } from '../../store/TopFaults/select'

import TopFaults from './TopFaults'


const TopFaultsContainer = props => (
    <TopFaults
        className={props.className}
        cardsVariant={props.cardsVariant}
        showMore={props.showMore}
        gridProps={props.gridProps}

        faults={
            props.faultsByIds.length
                ? props.faultsByIds
                : props.faultsByCat
        }

        onRender={props.onRender}
        loadFaults={props.loadFaults}
    />
)

const stateToProps = (state, props) => ({
    faultsByIds: faultsByIds(state, props.faultsIds),
    faultsByCat: faultsByCat(state, props.faultsCat),
    faultsTTT: faults(state)
})

const actionsToProps = {
    loadFaults
}

export default connect(stateToProps, actionsToProps)(TopFaultsContainer)