import React from 'react'

import { INavLink } from '../../../interfaces'
import { cl } from '../../../utilities/cl'


const LayoutFooterMenuList:React.FC<{
    title?:string
    variant: "normal" | "titles"
    links:INavLink[]
}> = ({
    title,
    variant = "normal",
    links = [],
}) => {

    const className = cl(
        "LayoutFooterMenuList",
        `LayoutFooterMenuList_variant-${variant}`
    )

    return (
        <div className={className}>
            {title && <div className="LayoutFooterMenuList__title">{title}</div>}
            {links.map((link, index) => (
                <a
                    key={index}
                    className="LayoutFooterMenuList__link fk-text-skip"
                    href={link.href}
                    title={link.seoTitle || link.title}
                    target={link.target}
                >
                    {link.title}
                </a>
            ))}
        </div>
    )
}

export default LayoutFooterMenuList