import React from 'react'

import { IMenuLinks } from './interfaces'
import { cl } from '../../../utilities/cl'
import { useIs } from '../../../utilities/media'

const svgLogo = '/dist/web/logo-text.svg'

import Group from '../../FK/Group'
import Button from '../../FK/Button'
import Container from '../../FK/Container'
import Grid from '../../FK/Grid'
import SelectLocation from '../../SelectLocation'
import Search from '../../Search'
import CallBox from '../../CallBox'

import MenuList from './MenuList'


const LayoutFooter: React.FC<{
    className?: string
    menuLinks:IMenuLinks[]
}> = ({
    className,
    menuLinks,
}) => {

    const companyName = ""

    const isMobileAndDown = useIs("mobileAndDown")
    const isTablet = useIs("tablet")

    const copyAndRights = (
        <div className="LayoutFooter__copy-and-rights">
            <div className="copy">
                {`© ${new Date().getFullYear()} Все права защищены. «ALEX SERVICE»`}
            </div>
            <div className="rights">
                {`Информация на сайте, в том числе цены, носят исключительно ознакомительный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 п.2 Гражданского кодекса РФ.`}
            </div>
        </div>
    )

    className = cl(
        "LayoutFooter",
        className
    )

    return (
        <footer className={className}>
            
            <section className="LayoutFooter__header">
                <Container size="xl">
                    <SelectLocation/>
                    <Search/>
                    <Group
                        className="contact"
                        alignX="end"
                        size="lg-1"
                    >
                        <CallBox/>
                        <Button
                            className="LayoutHeader__order"
                            variant="prim"
                            size="lg"
                            title="Заказать ремонт"
                            iconBefore="Repair"
                            modalType="OrderRepair"
                        />
                    </Group>
                </Container>
            </section>

            <section className="LayoutFooter__body">
                <Container size="xl">
                    <Grid
                        cols={{
                            desktopAndUp: 4,
                            tablet: 3,
                            mobileAndDown: 1,
                        }}
                    >
                        <div
                            className={cl(
                                "LayoutFooter__logo",
                                {"fk-grid__col-3": isTablet}
                            )}
                        >
                            <a className="logo fk-text-skip" href="/">
                                <img src={svgLogo} alt={companyName}/>
                            </a>
                            {!isMobileAndDown && copyAndRights}
                        </div>
                        {menuLinks.map((menu, index) => (
                            <MenuList
                                key={index}
                                variant={menu.variant}
                                title={menu.title}
                                links={menu.links}
                            />
                        ))}
                        {isMobileAndDown && copyAndRights}
                    </Grid>
                </Container>
            </section>

        </footer>
    )
}

export default LayoutFooter