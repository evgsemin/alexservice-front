import React from 'react'
import { connect } from 'react-redux'

import { ACTIONS, ARTICLES, ABOUT, CONTACTS, CORPS } from '../../../routes/routes'

import { servicesMenu } from '../../../store/Posts/select'

import LayoutFooter from './Footer'


const LayoutFooterContainer = props => (
    <LayoutFooter
        className={props.className}
        menuLinks={getMenuList(props.servicesMenu)}
    />
)

const stateToProps = state => ({
    servicesMenu: servicesMenu(state)
})

export default connect(stateToProps, null)(LayoutFooterContainer)

//
// Функции
//

let lastMenu = []
let lastDepend

function getMenuList(links) {
    if(links !== lastDepend) {
        return lastMenu = [
            ...links,
            // {
            //     title: "Ремонт бытовой техники",
            //     type: "normal",
            //     links: [
            //         {title: "Стиральная машина", seoTitle: "", href: "/", target: ""},
            //         {title: "Холодильник", seoTitle: "", href: "/", target: ""},
            //         {title: "Посудомоечная машина", seoTitle: "", href: "/", target: ""},
            //         {title: "Кофемашина", seoTitle: "", href: "/", target: ""},
            //         {title: "Духовой шкаф", seoTitle: "", href: "/", target: ""},
            //         {title: "Варочная панель", seoTitle: "", href: "/", target: ""},
            //         {title: "Микроволновая печь", seoTitle: "", href: "/", target: ""},
            //         {title: "Сушильная машина", seoTitle: "", href: "/", target: ""},
            //     ]
            // },
            // {
            //     title: "Ремонт кондиционеров",
            //     type: "normal",
            //     links: [
            //         {title: "Домашний кондиционер", seoTitle: "", href: "/", target: ""},
            //         {title: "Промышленный кондиционер", seoTitle: "", href: "/", target: ""},
            //     ]
            // },
            {
                variant: "titles",
                links: [
                    // {title: "Товары", seoTitle: "", href: "/", target: ""},
                    {title: "Блог", seoTitle: "", href: ARTICLES, target: ""},
                    // {title: "Акции", seoTitle: "", href: ACTIONS, target: ""},
                    {title: "О компании", seoTitle: "", href: ABOUT, target: ""},
                    {title: "Контакты и адреса", seoTitle: "", href: CONTACTS, target: ""},
                    {title: "Обслуживание организаций", seoTitle: "", href: CORPS, target: ""},
                ]
            }
        ]
    } else {
        return lastMenu
    }
}