import { INavLink } from '../../../interfaces'


export interface IMenuLinks {
    title?:string
    variant?:string
    links:INavLink[]
}