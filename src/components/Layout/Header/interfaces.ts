export interface INavLink {
    title:string
    seoTitle?:string
    href:string
    target?:string
}