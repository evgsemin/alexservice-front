import React from 'react'

import { cl } from '../../../utilities/cl'
import { useIs } from '../../../utilities/media'
import { isLength } from '../../../utilities/is'
import { INavLink } from './interfaces'

const svgLogo = '/dist/web/logo.svg'
const svgLogoText = '/dist/web/logo-text.svg'

import Container from '../../FK/Container'
import Button from '../../FK/Button'
import Group from '../../FK/Group'
import Search from '../../Search'
import SelectLocation from '../../SelectLocation'
import CallBox from '../../CallBox'
import NavLine from '../../NavLine'
import NavLineSticky from '../../NavLineSticky'


const LayoutHeader: React.FC<{
    className?: string
    companyName:string
    navLinks:INavLink[]
}> = ({
    className,
    companyName,
    navLinks = [],
}) => {

    const isTableAndDown = useIs("tabletAndDown")
    const isDesktop = useIs("desktop")

    const links = (
        <>
            <SelectLocation/>
            {isLength(navLinks) && (
                <nav className="LayoutHeader__menu-links">
                    {navLinks.map((link, index) => (
                        <Button
                            key={index}
                            variant="link-dark"
                            size="lg"
                            title={link.title}
                            href={link.href}
                            attrs={{
                                title: link.seoTitle || link.title,
                                target: link.target,
                            }}
                        />
                    ))}
                </nav>
            )}
        </>
    )

    className = cl(
        "LayoutHeader",
        className
    )

    return (
        <>
            <header className={className}>
                <Container size="xl">
                    <Group
                        size="sm"
                        alignY="center"
                        alignX={isDesktop ? "center" : "between"}
                    >

                        <div className="LayoutHeader__logo">
                            <a href="/" className="fk-text-skip">
                                <img
                                    alt={companyName}
                                    src={isTableAndDown ? svgLogoText : svgLogo}
                                />
                            </a>
                        </div>

                        {isTableAndDown && (
                            <div className="LayoutHeader__links">
                                {links}
                            </div>
                        )}

                        <div className="LayoutHeader__right">
                            <Group
                                alignY="center"
                                alignX={isTableAndDown ? "center" : undefined}
                                size={isTableAndDown ? "sm" : "lg-1"}
                            >
                                {!isTableAndDown && <Search/>}
                                {!isTableAndDown && links}
                                <CallBox/>
                                <Button
                                    className="LayoutHeader__order-button"
                                    variant="prim"
                                    size="lg"
                                    title="Заказать ремонт"
                                    iconBefore="Repair"
                                    modalType="OrderRepair"
                                />
                            </Group>
                        </div>

                    </Group>
                </Container>
            </header>
            
            <NavLine/>

            <NavLineSticky
                hiddenPointElementClassName="NavLine"
                hiddenPointOffset={200}
            />
        </>
    )
}

export default LayoutHeader