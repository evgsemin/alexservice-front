import React from 'react'

import { CONTACTS } from '../../../routes/routes'

import LayoutHeader from './Header'


const LayoutHeaderContainer = props => (
    <LayoutHeader
        className={props.className}
        companyName="AlexService — профессиональный подход к ремонту бытовой техники"
        navLinks={[
            {title: "Контакты и адреса", seoTitle: "", href: CONTACTS, target: ""}
        ]}
    />
)


export default LayoutHeaderContainer