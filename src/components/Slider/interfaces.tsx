export interface ISlide {
    type:string
    buttons?:{
        className?:string
        size?:string
        variant?:string
        title:string
        href?:string
        iconBefore?:string
        iconAfter?:string
        modalType?:string
        onClick?:string
        attrs?:any
    }[]
    [key:string]:any
}

export interface ISlidePromo extends ISlide {
    className?:string
    title:string
    subtitle?:string
    image:string
}

export interface ISlideText extends ISlide {
    className?:string
    variant?:string
    title:string
    subtitle?:string
    content?:any
    image:string
}