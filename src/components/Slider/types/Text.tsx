import React from 'react'

import { cl } from '../../../utilities/cl'
import { isLength } from '../../../utilities/is'
import { useIs } from '../../../utilities/media'
import { ISlideText } from '../interfaces'

import Group from '../../FK/Group'
import Button from '../../FK/Button'
import LazyImage from '../../FK/LazyImage'


const Text = ({
    className,
    variant,
    title,
    subtitle,
    content,
    buttons = [],
    image = "",
}:ISlideText) => {

    const isTabletAndDown = useIs("tabletAndDown")

    className = cl(
        "SliderText",
        `SliderText_variant-${variant}`,
        className
    )

    return (
        <div className={className}>

            <section className="SliderText__content">
                {title && <div className="title">{title}</div>}
                {subtitle && <div className="subtitle">{subtitle}</div>}
                {content && <div className="content">{content}</div>}
                {isLength(buttons) && (
                    <Group
                        size="xs"
                        className="buttons"
                        alignX={variant === "center" || isTabletAndDown ? "center" : "start"}
                    >
                        {buttons.map((btn, index) => (
                            <Button
                                key={index}
                                {...(btn.attrs || {})}
                                size={btn.size}
                                variant={btn.variant || "prim"}
                                className={btn.className}
                                title={btn.title}
                                href={btn.href || undefined}
                                iconBefore={btn.iconBefore}
                                iconAfter={btn.iconAfter}
                                modalType={btn.modalType}
                                onClick={btn.onClick}
                            />
                        ))}
                    </Group>
                )}
            </section>
        
            <LazyImage className="SliderText__image" type="div" image={image}/>

        </div>
    )
}

export default Text