import React from 'react'

import { cl } from '../../../utilities/cl'
import { isLength } from '../../../utilities/is'
import { ISlidePromo } from '../interfaces'

import Group from '../../FK/Group'
import Button from '../../FK/Button'
import LazyImage from '../../FK/LazyImage'


const Promo = ({
    className,
    title,
    subtitle,
    image = "",
    buttons = [],
}:ISlidePromo) => {

    className = cl(
        "SliderPromo",
        className
    )

    return (
        <div className={className}>

            <section className="SliderPromo__content">
                {title && <div className="title">{title}</div>}
                {subtitle && <div className="subtitle">{subtitle}</div>}
                {isLength(buttons) && (
                    <Group
                        size="xs"
                        className="SliderPromo__buttons"
                    >
                        {buttons.map((btn, index) => (
                            <Button
                                key={index}
                                {...(btn.attrs || {})}
                                size={btn.size}
                                variant={btn.variant || "prim"}
                                className={btn.className}
                                title={btn.title}
                                href={btn.href || undefined}
                                iconBefore={btn.iconBefore}
                                iconAfter={btn.iconAfter}
                                modalType={btn.modalType}
                                onClick={btn.onClick}
                            />
                        ))}
                    </Group>
                )}
            </section>

            <LazyImage className="SliderPromo__image" type="div" image={image}/>

        </div>
    )
}

export default Promo