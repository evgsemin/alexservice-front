import React, { useRef, useState, useEffect } from 'react'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { length } from '../../utilities/length'
import { ISlide } from './interfaces'

import SVG from '../FK/SVG'

import Promo from './types/Promo'
import Text from './types/Text'


let LAST_TOGGLE_TIME = 0

const Slider:React.FC<{
    className?:string
    slides:ISlide[]
    interval?:number
    buttons?:boolean
}> = ({
    className,
    slides = [],
    interval = -1,
    buttons = true
}) => {

    const refSlider = useRef(document.createElement("div"))
    const [activeSlideId, setActiveSlideId] = useState(1)
    const [prevActiveSlideId, setPrevActiveSlideId] = useState(-1)

    useEffect(() => autoToggle(), [activeSlideId])

    if(!length(slides)) return null

    const toggleSlide = ({ slideId, direction, required }:{slideId?:number, direction?:string, required?:boolean}) => {
        const timeOffset = Date.now() - LAST_TOGGLE_TIME

        if(timeOffset >= interval || required) {
            const futureSlideId:number =
                direction === "next" || direction === "prev"
                    ? getFutureSlideId(direction)
                    : testSlideId(slideId)
                    
            const futurePrevActiveSlideId:number = 
                slides.length === 1
                    ? -1
                    : activeSlideId

            setPrevActiveSlideId(futurePrevActiveSlideId)
            setActiveSlideId(futureSlideId)
            LAST_TOGGLE_TIME = Date.now()
        }
    }

    className = cl(
        "Slider",
        className
    )

    return (
        <div ref={refSlider} className={className}>
            {slides.map((slide, index) => {
                const slideId = index+1
                return (
                    <div
                        key={slideId}
                        className={cl(
                            "Slider__slide",
                            {"active": slideId === activeSlideId},
                            {"prev-active": slideId === prevActiveSlideId},
                        )}
                    >
                        {slide.type === "Promo" && (
                            <Promo
                                type={slide.type}
                                className={slide.className}
                                title={slide.title}
                                subtitle={slide.subtitle}
                                image={slide.image}
                                buttons={slide.buttons}
                            />
                        )}
                        {slide.type === "Text" && (
                            <Text
                                type={slide.type}
                                className={slide.className}
                                variant={slide.variant}
                                title={slide.title}
                                subtitle={slide.subtitle}
                                content={slide.content}
                                image={slide.image}
                                buttons={slide.buttons}
                            />
                        )}
                    </div>
                )
            })}
            {buttons && length(slides) > 1 && (
                <div className="Slider__buttons">
                    <SVG
                        className="prev"
                        title="Назад"
                        icon="ArrowLeft"
                        size="sm"
                        variant="dark"
                        onClick={() => toggleSlide({ direction: "prev", required: true })}
                        // disabled={activeSlideId === 1}
                    />
                    <SVG
                        className="next"
                        title="Вперёд"
                        icon="ArrowRight"
                        size="sm"
                        variant="dark"
                        onClick={() => toggleSlide({ direction: "next", required: true })}
                        // disabled={activeSlideId === slides.length}
                    />
                </div>
            )}
        </div>
    )

    function autoToggle() {
        if(slides.length > 1) {
            if(interval > 0) {
                const toggleTimeout = setTimeout(() => {
                    toggleSlide({ direction: "next" })
                }, interval)

                return () => clearTimeout(toggleTimeout)
            }
        }
    }

    function getFutureSlideId(direction:string) {
        let futureSlideId:number

        if(direction === "next") {
            futureSlideId = activeSlideId === slides.length ? 1 : activeSlideId+1
        } else {
            futureSlideId = activeSlideId === 1 ? slides.length : activeSlideId-1
        }

        futureSlideId = testSlideId(futureSlideId)

        return futureSlideId
    }

    function testSlideId(slideId:number | undefined) {
        if(slideId) {
            if(slideId > slides.length || slideId < 1) {
                return 1
            } else {
                return slideId
            }
        }
        return 1
    }
}

export default Slider