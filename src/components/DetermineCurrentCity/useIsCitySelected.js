import React from 'react'
import { useSelector } from 'react-redux'

import { currentId } from '../../store/DetermineCurrentCity/select'


export const useIsCitySelected = () => {
    const currentCityId = useSelector(state => currentId(state))
    return currentCityId !== -1
}