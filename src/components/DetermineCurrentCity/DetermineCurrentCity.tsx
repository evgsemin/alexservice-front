import React, { useEffect } from 'react'


const DetermineCurrentCity:React.FC<{
    currentId:number
    cities:any
    loadCities:any
    changeCity:any
}> = ({
    currentId,
    cities,
    loadCities,
    changeCity,
}) => {

    useEffect(() => {
        loadCities()
    }, [])

    useEffect(() => {
        setDefaultAsMoscow()
    }, [cities, currentId])

    function setDefaultAsMoscow() {
        if(currentId === -1) {
            for(let city in cities) {
                if(cities[city].ticket === "moscow") {
                    const moscowId = cities[city].id
                    changeCity({
                        cityId: moscowId
                    })
                }
            }
        }
    }

    return null
}

export default DetermineCurrentCity