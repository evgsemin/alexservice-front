import React from 'react'
import { connect } from 'react-redux'

import { loadCities, changeCity } from '../../store/DetermineCurrentCity/actions'
import { cities, currentId } from '../../store/DetermineCurrentCity/select'

import DetermineCurrentCity from './DetermineCurrentCity'


const DetermineCurrentCityContainer = props => (
    <DetermineCurrentCity
        currentId={props.currentId}
        cities={props.cities}
        loadCities={props.loadCities}
        changeCity={props.changeCity}
    />
)

const stateToProps = state => ({
    currentId: currentId(state),
    cities: cities(state),
})

const actionToProps = {
    loadCities,
    changeCity,
}

export default connect(stateToProps, actionToProps)(DetermineCurrentCityContainer)