import React from 'react'

import { cl } from '../../utilities/cl'
import { useIs } from '../../utilities/media'

import Container from '../FK/Container'
import NavMenu from '../NavMenu'
import Search from '../Search'


const NavLine:React.FC<{
    className?:string
}> = ({
    className
}) => {

    const isMobileAndDown = useIs("mobileAndDown")

    className = cl(
        "NavLine",
        className,
    )

    return (
        <div className={className}>
            <Container size="xl">
                <div className="NavLine__wrap">
                    
                    <NavMenu
                        {...{
                            menuTitle: "Главное меню",
                            variant: "inverse",
                            groupProps: {
                                size: "lg-1",
                                alignX: "center",
                            },
                            parentElementClassName: "NavLine",
                        }}
                    />

                    {isMobileAndDown && <Search/>}
                    
                </div>
            </Container>
        </div>
    )
}

export default NavLine