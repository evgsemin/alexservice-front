import React from 'react'
import { connect } from 'react-redux'

import { breadcrumbs } from '../../store/Breadcrumbs/select'

import Breadcrumbs from './Breadcrumbs'


const BreadcrumbsContainer = props => (
    <Breadcrumbs
        className={props.className}
        lastIsCurrent={props.lastIsCurrent}
        containerProps={props.containerProps}
        links={props.links}
    />
)

const stateToProps = state => ({
    links: breadcrumbs(state)
})

export default connect(stateToProps, null)(BreadcrumbsContainer)