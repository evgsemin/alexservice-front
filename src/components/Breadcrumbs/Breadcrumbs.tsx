import React from 'react'

import { cl } from '../../utilities/cl'
import { length } from '../../utilities/length'
import { INavLink } from '../../interfaces'

import SVG from '../FK/SVG'
import Container from '../FK/Container'


interface IBreadcrumbsLink extends INavLink {
    current?:boolean
}

const Breadcrumbs:React.FC<{
    className?:string
    links?:IBreadcrumbsLink[]
    lastIsCurrent?:boolean
    containerProps?:any
}> = ({
    className,
    links = [],
    lastIsCurrent = true,
    containerProps = {},
}) => {

    className = cl(
        "Breadcrumbs",
        className,
    )

    if(!length(links)) return null

    return (
        <section className={className}>
            <Container size="lg" {...containerProps}>
                <div itemType="https://schema.org/BreadcrumbList">

                    {links.map((link, index) => {
                        let isCurrent = (link.current || link.href === "" || typeof link.href === undefined)
                        
                        if(lastIsCurrent && index+1 === links.length) isCurrent = true

                        // Ссылка
                        if(!isCurrent) {
                            return (
                                <span
                                    key={index}
                                    className="Breadcrumbs__item"
                                    itemProp="itemListElement"
                                    itemType="https://schema.org/ListItem"
                                    typeof="ListItem"
                                >
                                    <a
                                        className="fk-text-skip"
                                        title={link.seoTitle || link.title}
                                        href={link.href}
                                        target={link.target}
                                        itemProp="item"
                                        typeof="WebPage"
                                    >
                                        <span itemProp="name">{link.title}</span>
                                    </a>
                                    <meta itemProp="position" content="1"/>
                                    <SVG
                                        icon="ArrowRight"
                                        variant="dark"
                                        size="mr-2"
                                    />
                                </span>
                            )
                        }

                        // Текущая страница
                        else {
                            return (
                                <span
                                    key={index}
                                    className="Breadcrumbs__item"
                                    itemProp="itemListElement"
                                    itemType="https://schema.org/ListItem"
                                    typeof="ListItem"
                                >
                                    <span itemProp="name">{link.title}</span>
                                    <meta itemProp="position" content="3"/>
                                </span>
                            )
                        }
                    })}

                </div>
            </Container>
        </section>
    )
}

export default Breadcrumbs