import React, { useEffect } from 'react'
import { useIsCitySelected } from '../DetermineCurrentCity/useIsCitySelected'

import { cl } from '../../utilities/cl'
import { isLength } from '../../utilities/is'
import { safeCall } from '../../utilities/safeCall'
import { IAction } from './interfaces'

import CardActionInform from '../Card/ActionInform'
import Grid from '../FK/Grid'


const Actions:React.FC<{
    className?:string
    actions:IAction[]
    loadActions:any
    onRender?:any
}> = ({
    className,
    actions = [],
    loadActions,
    onRender,
}) => {

    const isCitySelected = useIsCitySelected()

    useEffect(() => {
        if(isCitySelected) {
            loadActions()
        }
    }, [isCitySelected])

    useEffect(() => {
        safeCall(onRender, isLength(actions))
    }, [actions])

    className = cl(
        "Actions",
        className,
    )

    if(!isLength(actions)) return null

    return (
        <div className={className}>
            <Grid
                size="xs"
                cols={{
                    tabletAndUp: 2,
                    mobileAndDown: 1,
                }}
            >
                {actions.map(action => (
                    <CardActionInform
                        key={action.id}
                        id={action.id}
                        className={action.className}
                        title={action.title}
                        subtitle={action.subtitle}
                        alt={action.alt}
                        href={action.href}
                        image={action.imageMedium}
                        onClick={action.onClick}
                    />
                ))}
            </Grid>
        </div>
    )
}

export default Actions