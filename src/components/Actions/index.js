import React from 'react'
import { connect } from 'react-redux'

import { loadActions } from '../../store/Actions/actions'
import { actionsArr } from '../../store/Actions/select'

import Actions from './Actions'


const ActionsContainer = props => (
    <Actions
        className={props.className}
        actions={props.actions}
        loadActions={props.loadActions}
        onRender={props.onRender}
    />
)

const stateToProps = state => ({
    actions: actionsArr(state)
})

const actionsToProps = {
    loadActions,
}

export default connect(stateToProps, actionsToProps)(ActionsContainer)