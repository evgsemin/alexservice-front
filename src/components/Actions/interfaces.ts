import { ICardActionInform } from '../Card/ActionInform/interfaces'

export interface IAction extends ICardActionInform {
    imageMedium:string
}