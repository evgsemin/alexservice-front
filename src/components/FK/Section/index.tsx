import React from 'react'

import { cl } from '../../../utilities/cl'
import { isBool } from '../../../utilities/is'
import { IHeader, IFooter} from './interfaces'

import Container from '../Container'


const Section:React.FC<{
    className?:string
    textVariant?: "normal" | "inverse"
    align?: "left" | "center" | "right"
    header?:IHeader
    footer?:IFooter
    offsetType?: "normal" | "narrow"
    size?:string
    containerSize?:string
    containerProps?:any
    containerOn?:boolean
    children:any
}> = ({
    className,
    textVariant = "normal",
    align = "center",
    header = {},
    footer,
    offsetType = "normal",
    size = "lg",
    containerSize,
    containerProps,
    containerOn = true,
    children
}) => {

    className = cl(
        "fk-section",
        `fk-section_text-variant-${textVariant}`,
        `fk-section_offset-type-${offsetType}`,
        `fk-section_size-${size}`,
        className,
    )

    return (
        <section className={className}>

            {/* header */}

            {(header.title || header.subtitle || header.contentBefore || header.contentAfter) && (
                <>
                    {containerOn && (
                        <Container {...(containerProps || {})} size={containerSize || header.containerSize}>
                            {getHeader()}
                        </Container>
                    )}
                    {!containerOn && getHeader()}
                </>
                
            )}

            {/* content */}

            {containerOn && (
                <Container {...(containerProps || {})} size={containerSize}>
                    {children}
                </Container>
            )}
            {!containerOn && children}

            {/* footer */}
            
            {containerOn && (
                footer && (
                    <Container {...(containerProps || {})} size={containerSize || footer.containerSize}>
                        {getFooter()}
                    </Container>
                )
            )}
            {!containerOn && (
                footer && getFooter()
            )}
        </section>
    )

    function getHeader() {
        let titleTag   = header.titleTag || "h2",
            titleClass = cl(
                "fk-section__title",
                {"fk-text-skip": isBool(header.titleTextSkip) ? header.titleTextSkip : true},
                `fk-ta-${align}`,
            )

        let titleElem = null

        if(titleTag === "h1") titleElem = <h1 className={titleClass}>{header.title}</h1>
        if(titleTag === "h2") titleElem = <h2 className={titleClass}>{header.title}</h2>
        if(titleTag === "h3") titleElem = <h3 className={titleClass}>{header.title}</h3>
        if(titleTag === "h4") titleElem = <h4 className={titleClass}>{header.title}</h4>
        if(titleTag === "h5") titleElem = <h5 className={titleClass}>{header.title}</h5>

        return (
            <header className={cl("fk-section__header", header.className)}>
                {header.contentBefore && (
                    header.contentBefore
                )}
                {header.title && titleElem}
                {header.subtitle && (
                    <p className={cl("fk-section__subtitle", `fk-ta-${align}`)}>{header.subtitle}</p>
                )}
                {header.contentAfter && (
                    header.contentAfter
                )}
            </header>
        )
    }

    function getFooter() {
        return (
            <footer className={cl("fk-section__footer", footer.className)}>
                {footer.content && (
                    footer.content
                )}
            </footer>
        )
    }
}

export default Section