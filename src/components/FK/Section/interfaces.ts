export interface IHeader {
    className?:string
    title?:any
    titleTag?: "h1" | "h2" | "h3" | "h4" | "h5"
    titleTextSkip?:boolean
    subtitle?:any
    contentBefore?:any
    contentAfter?:any,
    containerSize?:string
}

export interface IFooter {
    className?:string
    content?:any
    containerSize?:string
}