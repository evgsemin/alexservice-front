import React from 'react'

import { cl } from '../../../utilities/cl'


interface ITablePart {
    attrs?:any
    tr?:{
        id?:number
        attrs?:any
        items?:{
            id?:number
            type?: "th" | "td"
            attrs?:any
            title?:string
            content?:any
        }[]
    }[]
}

interface ITable {
    caption?:string
    thead?:ITablePart
    tbody?:ITablePart
}

const Table:React.FC<{
    className?:string
    table?:ITable
    size?:string
    notBorder?:boolean
}> = ({
    className,
    table = {},
    size = "sm-1",
    notBorder = false,
}) => {

    className = cl(
        "fk-table",
        `fk-table_size-${size}`,
        {"fk-table_not-border": notBorder},
        {"fk-table_with-header": (table.thead) ? true : false},
        className,
    )

    return (
        <div className={className}>
            <table>
                {table.caption && (
                    <caption>{table.caption}</caption>
                )}

                {table.thead && (
                    <thead {...(table.thead.attrs || {})}>
                        {(table.thead.tr || []).map((tr, index) => (
                            <tr key={tr.id || index} {...(tr.attrs || {})}>
                                {(tr.items || []).map((item, index) => (
                                    (item.type === "th")
                                        ? <th key={item.id || index} {...(item.attrs || {})}>
                                            {item.title && item.title}
                                            {item.content && item.content}
                                        </th>
                                        : <td key={item.id || index} {...(item.attrs || {})}>
                                            {item.title && item.title}
                                            {item.content && item.content}
                                        </td>
                                ))}
                            </tr>
                        ))}
                    </thead>
                )}

                {table.tbody && (
                    <tbody {...(table.tbody.attrs || {})}>
                        {(table.tbody.tr || []).map((tr, index) => (
                            <tr key={tr.id || index} {...(tr.attrs || {})}>
                                {(tr.items || []).map((item, index) => (
                                    (item.type === "th")
                                        ? <th key={item.id || index} {...(item.attrs || {})}>
                                            {item.title && item.title}
                                            {item.content && item.content}
                                        </th>
                                        : <td key={item.id || index} {...(item.attrs || {})}>
                                            {item.title && item.title}
                                            {item.content && item.content}
                                        </td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                )}
            </table>
        </div>
    )
}

export default Table