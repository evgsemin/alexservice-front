import React, { useEffect, useState, useRef } from 'react'

import { cl } from '../../../utilities/cl'
import { outside } from '../../../utilities/outside'
import { isBrowser } from '../../../utilities/is'

import Simple from './types/Simple'
import Location from './types/Location'
import OrderProducts from './types/OrderProducts'
import OrderRepair from './types/OrderRepair'
import OrderCorp from './types/OrderCorp'


let isOpened = false

const Modal:React.FC<{
    className?:string
    divClassName?:string
    type:string
    data?:any
    children:any
}> = ({
    className,
    divClassName,
    type,
    data = {},
    children,
}) => {

    const refModal = useRef<any>()
    const [opened, setOpened] = useState(false)

    useEffect(() => {
        window.addEventListener("keydown", closeByEsc)
        return () => window.removeEventListener("keydown", closeByEsc)
    }, [])

    useEffect(() => {
        try {            
            isOpened = opened
            if(opened) {
                document.documentElement.classList.add("fk-modal-page-no-scroll")
            } else {
                document.documentElement.classList.remove("fk-modal-page-no-scroll")
            }
        } catch(e) {}
    }, [opened])

    const hClick = (e:any) => {
        if(e.target === refModal.current) {
            if(opened === true) {
                setOpened(false)
            }
        }
    }

    className = cl(
        "fk-modal",
        className,
    )

    return (
        <>
            <div className={divClassName} onClick={() => setOpened(true)}>
                {children}
            </div>

            {opened && outside(
                <>
                    <div ref={refModal} className={className} onClick={hClick}>
                        {type === "Simple" && (
                            <Simple
                                {...data}
                                opened={opened}
                                setOpened={setOpened}
                            />
                        )}
                        {type === "Location" && (
                            <Location
                                {...data}
                                opened={opened}
                                setOpened={setOpened}
                            />
                        )}
                        {type === "OrderProducts" && (
                            <OrderProducts
                                {...data}
                                opened={opened}
                                setOpened={setOpened}
                            />
                        )}
                        {type === "OrderRepair" && (
                            <OrderRepair
                                {...data}
                                opened={opened}
                                setOpened={setOpened}
                            />
                        )}
                        {type === "OrderCorp" && (
                            <OrderCorp
                                {...data}
                                opened={opened}
                                setOpened={setOpened}
                            />
                        )}
                    </div>
                </>
            )}
        </>
    )

    function closeByEsc(e:any) {
        if(e.code === "Escape") {
            if(isOpened === true) {
                setOpened(false)
            }
        }
    }
}

export default Modal