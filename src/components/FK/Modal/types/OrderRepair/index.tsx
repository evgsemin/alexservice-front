import React from 'react'

import { cl } from '../../../../../utilities/cl'

import OrderFormService from '../../../../OrderForm/Service'

import Simple from '../Simple'


const OrderRepair:React.FC<{
    className?:string
    title?:any
    subtitle?:any
    opened:boolean
    setOpened:any
}> = ({
    className,
    title = "Заказать ремонт",
    subtitle = "Мы перезвоним в течении 10 минут.",
    opened,
    setOpened,
}) => {

    className = cl(
        "fk-modal-order-repair",
        className,
    )

    return (
        <div className={className}>
            <Simple
                title={<>{title}</>}
                subtitle={<>{subtitle}</>}
                size="md-1"
                containerSize="sm"
                opened={opened}
                setOpened={setOpened}
                content={
                    <OrderFormService
                        groupProps={{
                            size: "xs-1"
                        }}
                    />
                }
            />
        </div>
    )
}

export default OrderRepair