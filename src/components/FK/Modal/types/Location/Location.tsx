import React from 'react'

import { cl } from '../../../../../utilities/cl'

import Simple from '../Simple'


const Location:React.FC<{
    className?:string
    opened:boolean
    setOpened:any
}> = ({
    className,
    opened,
    setOpened,
}) => {

    className = cl(
        "fk-modal-location",
        className,
    )

    return (
        <div className={className}>
            <Simple
                title="Выберите город"
                subtitle="Выбирайте внимательно!"
                size="sm"
                opened={opened}
                setOpened={setOpened}
                content={<>
                    <h4>Окно</h4>
                </>}
            />
        </div>
    )
}

export default Location