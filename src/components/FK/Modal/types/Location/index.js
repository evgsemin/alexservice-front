import React from 'react'

import Location from './Location'


const LocationContainer = props => (
    <Location
        className={props.className}
        setOpened={props.setOpened}
    />
)

export default LocationContainer