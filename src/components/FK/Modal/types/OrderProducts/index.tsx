import React from 'react'

import { cl } from '../../../../../utilities/cl'

import OrderFormService from '../../../../OrderForm/Service'

import Simple from '../Simple'


const OrderProducts:React.FC<{
    className?:string
    opened:boolean
    setOpened:any
}> = ({
    className,
    opened,
    setOpened,
}) => {

    className = cl(
        "fk-modal-order-products",
        className,
    )

    return (
        <div className={className}>
            <Simple
                title="Оформление заказа"
                size="md-1"
                containerSize="md"
                opened={opened}
                setOpened={setOpened}
                content={
                    <OrderFormService
                        groupProps={{
                            size: "xs-1"
                        }}
                    />
                }
            />
        </div>
    )
}

export default OrderProducts