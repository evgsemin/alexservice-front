import React, { useState } from 'react'

import { cl } from '../../../../../utilities/cl'

import Container from '../../../../FK/Container'
import SVG from '../../../../FK/SVG'


const Modal:React.FC<{
    className?:string
    opened:boolean
    size:string
    containerSize?:string
    title?:any
    subtitle?:any
    content?:any
    setOpened:any
}> = ({
    className,
    opened,
    size = "md",
    containerSize,
    title,
    subtitle,
    content = null,
    setOpened,
}) => {

    className = cl(
        "fk-modal-simple",
        `fk-modal-simple_size-${size}`,
        {"opened": opened},
        className,
    )

    return (
        <Container size={containerSize || size} by="margin">
            <section className={className}>

                <header className="fk-modal-simple__header">
                    <div className="left">
                        {title && <h2 className="title fk-text-skip">{title}</h2>}
                        {subtitle && <p className="subtitle fk-text-skip">{subtitle}</p>}
                    </div>

                    <div className="right">
                        <SVG
                            icon="Cancel"
                            onClick={() => setOpened(false)}
                        />
                    </div>
                </header>

                <div className="fk-modal-simple__body">
                    {content}
                </div>
            </section>
        </Container>
    )
}

export default Modal