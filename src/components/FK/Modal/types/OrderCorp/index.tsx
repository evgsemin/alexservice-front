import React from 'react'

import { cl } from '../../../../../utilities/cl'

import OrderFormCorp from '../../../../OrderForm/Corp'

import Simple from '../Simple'


const OrderRepair:React.FC<{
    className?:string
    opened:boolean
    setOpened:any
}> = ({
    className,
    opened,
    setOpened,
}) => {

    className = cl(
        "fk-modal-order-repair",
        className,
    )

    return (
        <div className={className}>
            <Simple
                title={<>Запрос о сотрудничестве</>}
                subtitle={<>Сотрудник нашей компании перезвонит вам в течении нескольких минут.</>}
                size="md-1"
                containerSize="sm"
                opened={opened}
                setOpened={setOpened}
                content={
                    <OrderFormCorp
                        groupProps={{
                            size: "xs-1"
                        }}
                    />
                }
            />
        </div>
    )
}

export default OrderRepair