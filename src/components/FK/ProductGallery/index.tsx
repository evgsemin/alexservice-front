import React, { useState, useEffect, useRef } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'
import { IImageProductGallery } from '../../../interfaces'

import LazyImage from '../LazyImage'
import SVG from '../SVG'
import FullView from '../FullView'


const ProductGallery:React.FC<{
    className?:string
    direction?: "x" | "y"
    images?:IImageProductGallery[]
    startActive?:number
    active?:number
    onChange?:any
}> = ({
    className,
    direction = "y",
    images = [],
    startActive,
    active,
    onChange,
}) => {

    const refHidden = useRef(document.createElement("div"))
    const refWrap = useRef(document.createElement("div"))
    const refScroll = useRef(document.createElement("div"))
    const [iActive, setIActive] = useState(typeof startActive === "number" ? startActive : (typeof active === "number" ? active : 0))
    const [fewImages, setFewImages] = useState(false)
    const [fullViewImageId, setFullViewImageId] = useState(-1)

    useEffect(() => {
        window.addEventListener("resize", updateFewImages)
        return () => window.removeEventListener("resize", updateFewImages)
    }, [])

    useEffect(() => {
        refScroll.current.addEventListener("wheel", hScroll, {passive: false})
        return () => refScroll.current.removeEventListener("wheel", hScroll)
    }, [])
    
    useEffect(updateFewImages, [images])

    useEffect(() => {
        toggle(active)
    }, [active])

    const hClick = (e:any) => {
        const thumb = e.target.closest(".thumb")
        if(thumb) {
            const nextActive = Number(thumb.dataset.imageId)
            toggle(nextActive)
        }
    }

    function hScroll(e:any) {
        if(e) {
            if(direction === "x") {
                if(e.deltaY > 0) {
                    refScroll.current.scrollLeft += 30
                }
                if(e.deltaY < 0) {
                    refScroll.current.scrollLeft -= 30
                }
                e.preventDefault ? e.preventDefault() : (e.returnValue = false)
                return false
            }
        }
    }

    className = cl(
        "fk-product-gallery",
        `fk-product-gallery_direction-${direction}`,
        {"fk-product-gallery_without-fade": fewImages},
        className,
    )

    return (
        <div className={className}>

            <FullView
                images={images}
                active={fullViewImageId}
                opened={fullViewImageId !== -1}
                onClose={() => setFullViewImageId(-1)}
            />

            <div className="fk-product-gallery__image">
                {images[iActive]
                    ? <LazyImage
                        className="image"
                        type={direction === "y" ? "img" : "div"}
                        format="md"
                        alt={images[iActive].alt}
                        image={images[iActive].image}
                        attrs={{
                            onClick: () => setFullViewImageId(iActive)
                        }}
                    />
                    : <div className="empty">
                        Изображений для просмотра нет.
                    </div>
                }
            </div>

            <div className="fk-product-gallery__thumbs">
                {!fewImages && (
                    <SVG
                        className="icon-prev"
                        variant="dark"
                        icon={direction === "y" ? "ArrowUp" : "ArrowLeft"}
                        title="Предыдущий"
                        size={direction === "y" ? "xs" : "sm"}
                        onClick={() => toggle("prev")}
                    />
                )}

                <div className="hidden" ref={refHidden}>
                    {images.length && (
                        <div className="scroll" ref={refScroll}>
                            <div className="wrap" onClick={hClick} ref={refWrap}>
                                {images.map((img, index) => (
                                    <LazyImage
                                        key={index}
                                        className={cl(
                                            "thumb",
                                            {"active": index === iActive},
                                        )}
                                        attrs={{"data-image-id": index}}
                                        type="div"
                                        format="md"
                                        alt={img.alt}
                                        image={img.thumb || img.image}
                                    />
                                ))}
                            </div>
                        </div>
                    )}
                </div>

                {!fewImages && (
                    <SVG
                        className="icon-next"
                        variant="dark"
                        icon={direction === "y" ? "ArrowDown" : "ArrowRight"}
                        title="Следующий"
                        size={direction === "y" ? "xs" : "sm"}
                        onClick={() => toggle("next")}
                    />
                )}
            </div>

        </div>
    )

    function toggle(next?:number | string) {
        if(typeof next === "number") {
            change(next)
        } else if(next === "next") {
            change(iActive + 1)
        } else if(next === "prev") {
            change(iActive - 1)
        }
    }

    function change(nextActive:number) {
        if(typeof nextActive === "number") {
            if(nextActive < 0) nextActive = (images.length-1)
            if(nextActive > (images.length-1)) nextActive = 0
            setIActive(nextActive)
            safeCall(onChange, {
                value: iActive,
            })
        }
    }

    function updateFewImages() {
        if(direction === "y") {
            setFewImages( (refHidden.current.clientHeight > refWrap.current.clientHeight) )
        } else if(direction === "x") {
            setFewImages( (refHidden.current.clientWidth > refWrap.current.clientWidth) )
        }
    }
}

export default ProductGallery