import React from 'react'

import { cl } from '../../../utilities/cl'


const Container:React.FC<{
    className?:string
    size?:string
    by?:string
    children:any
}> = ({
    className,
    size = "lg",
    by = "padding",
    children
}) => {

    className = cl(
        "fk-container",
        `fk-container_by-${by}`,
        `fk-container_size-${size}`,
        className,
    )

    return (
        <div className={className}>
            {children}
        </div>
    )
}

export default Container