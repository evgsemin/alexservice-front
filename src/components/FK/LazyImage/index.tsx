import React, { useState, useEffect, useRef } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { isElemAtView } from '../../../utilities/is'


const LazyImage:React.FC<{
    className?:string
    type: "img" | "div" | "figure"
    format?:string // if type equal "div"
    maskVariant?:string // if type equal "div"
    maskLevel?:string // if type equal "div"
    caption?:string // if type equal "figure"
    url?:string // if type equal "figure"
    image:string
    alt?:string
    parent?:HTMLElement
    offset?:number
    timeInterval?:number
    attrs?:any
    children?:any
}> = ({
    className,
    type = "img",
    format,
    maskVariant,
    maskLevel,
    caption,
    url,
    image,
    alt,
    offset = 200,
    timeInterval = 750,
    attrs = {},
    children,
}) => {

    const refImage = useRef(document.createElement("div"))
    const [lazyImage, setLazyImage] = useState<string | undefined>()
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const lazyInterval = setInterval(() => {
            if(refImage.current) {
                if(isElemAtView({
                    elem: refImage.current,
                    offset
                })) {
                    setLazyImage(image)
                    setLoaded(true)
                    clearInterval(lazyInterval)
                }
            }
        }, timeInterval)
    }, [])

    useEffect(() => {
        if(loaded) {
            setLazyImage(image)
        }
    }, [image])

    if(type === "div" && format) {
        className = cl(
            className,
            `fk-format-${format}`
        )
    }

    if(type === "div" && maskVariant) {
        className = cl(
            className,
            "fk-mask",
            `fk-mask_variant-${maskVariant}`,
            `fk-mask_level-${maskLevel || "md"}`,
        )
    }

    return (
        <>
            {type === "img" && (
                <img alt={alt} title={alt} {...attrs} ref={refImage} className={className} src={lazyImage}/>
            )}
            {type === "div" && (
                <div title={alt} {...attrs} ref={refImage} className={className} role="img" style={{ backgroundImage:`url(${lazyImage})` }}>{children}</div>
            )}
            {type === "figure" && (
                <figure
                    itemScope={true}
                    {...attrs}
                    ref={refImage}
                    itemProp="image"
                    itemType="http://schema.org/ImageObject"
                    className={className}
                >
                    {url && <meta itemProp="url" content={url}/>}
                    <img itemProp="image" alt={alt} src={lazyImage}/>
                    {(caption || alt) && (
                        <figcaption itemProp="caption">
                            <span>{caption || alt}</span>
                        </figcaption>
                    )}
                    {children}
                </figure>
            )}
        </>
    )
}

export default LazyImage