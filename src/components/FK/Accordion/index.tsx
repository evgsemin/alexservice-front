import React, { useState } from 'react'

import { cl } from '../../../utilities/cl'
import { length } from '../../../utilities/length'

import { IAccordionItem } from './interfaces'

import SVG from '../SVG'
import Div from '../../Div'


const Accordion:React.FC<{
    className?:string
    size?:string
    items?:IAccordionItem[]
    startOpened?:Array<number>
}> = ({
    className,
    size = "sm",
    items = [],
    startOpened = [1],
}) => {

    const [opened, setOpened] = useState(startOpened)

    const hClick = (e:any) => {
        const itemTitle = e.target.closest(".fk-accordion__item .title")
        if(itemTitle) {
            const itemNumber = Number(itemTitle.dataset.itemNumber)
            if(!isNaN(itemNumber)) {
                let nextOpened = [...opened]
                let itemIndex = nextOpened.indexOf(itemNumber)

                if(itemIndex === -1) {
                    nextOpened.push(itemNumber)
                } else {
                    nextOpened.splice(itemIndex, 1)
                }
                setOpened(nextOpened)
            }
        }
    }

    className = cl(
        "fk-accordion",
        `fk-accordion_size-${size}`,
        className,
    )

    if(!length(items)) return null

    return (
        <div className={className} onClick={hClick}>

            {items.map((item, index) => {
                const itemNumber = index+1

                return (
                    <section
                        key={item.id || index}
                        className={cl(
                            "fk-accordion__item",
                            {"opened":  opened.indexOf(itemNumber) !== -1}
                        )}
                    >
                        <header className="title" data-item-number={itemNumber}>
                            <div className="left">
                                {(typeof item.title === "string")
                                    ? <h4 className="fk-text-skip">{item.title}</h4>
                                    : item.title
                                }
                            </div>
                            <div className="right">
                                <SVG icon="ArrowDown"/>
                            </div>
                        </header>

                        <Div className="content">
                            {item.content}
                        </Div>
                    </section>
                )
            })}
        </div>
    )
}

export default Accordion