import React from 'react'

import { cl } from '../../../utilities/cl'
import { isObj, isNumb } from '../../../utilities/is'
import { useMedia } from '../../../utilities/media'


const Grid:React.FC<{
    className?:string
    cols?:number | any
    defaultCols?:number
    size?:string
    by?:string
    align?: "start" | "end" | "center"
    children:any
}> = ({
    className,
    cols = {},
    defaultCols = 4,
    size = "sm",
    by = "margin",
    align = "",
    children,
}) => {

    const iCols        = isObj(cols) ? cols : {}
    const iDefaultCols = isNumb(cols) ? cols : defaultCols
    const nextCols     = useMedia(iCols, iDefaultCols, [cols])

    className = cl(
        "fk-grid",
        `fk-grid_size-${size}`,
        `fk-grid_cols-${nextCols}`,
        `fk-grid_by-${by}`,
        {[`fk-grid_align-${align}`]: align.length ? true : false},
        className,
    )

    return (
        <div className={className}>
            <div className="fk-grid__wrap">
                {children}
            </div>
        </div>
    )
}

export default Grid