import React from 'react'

import { cl } from '../../../utilities/cl'


const Loading:React.FC<{
    className?:string
    title?:string
    variant?:string
    size?:string
    align?:string
}> = ({
    className,
    title = "Загрузка...",
    variant = "dark-lg",
    size = "sm",
    align = "left",
}) => {

    className = cl(
        "fk-loading",
        `fk-ta-${align}`,
        {[`fk-loading_variant-${variant}`]: typeof variant === "string"},
        {[`fk-loading_size-${size}`]: typeof size === "string"},
        className,
    )

    return (
        <div className={className}>
            <i className="fk-text-skip" title={title}></i>
        </div>
    )
}

export default Loading