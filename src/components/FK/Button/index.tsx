import React from 'react'

import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'
import { isObj } from '../../../utilities/is'

import SVG from '../SVG'
import Modal from '../Modal'
import Loading from '../Loading'


const Button: React.FC<{
    className?:string
    variant?:string
    title?:string
    size?:string
    iconBefore?:string
    iconAfter?:string
    iconBeforeSize?:string
    iconAfterSize?:string
    href?:string
    onClick?:any
    name?:string
    payload?:any
    attrs?:any
    loading?:boolean
    disabled?:boolean
    modalType?:string
    modalData?:any
    setRef?:any
    children?:any
}> = ({
    className,
    variant = "prim",
    title,
    size = "md",
    iconBefore,
    iconAfter,
    iconBeforeSize,
    iconAfterSize,
    href = "-1",
    onClick,
    name,
    payload,
    attrs = {},
    loading = false,
    disabled = false,
    modalType,
    modalData = {},
    setRef,
    children,
}) => {

    const hClick = (event:any) => {
        if(disabled !== true && loading !== true) {
            safeCall(onClick, {
                name,
                payload,
            }, event)
        }
    }

    if(modalType) {
        if(!isObj(modalData)) modalData = {}
        modalData.divClassName = cl(modalData.divClassName, className)
    }

    className = cl(
        "fk-btn",
        `fk-btn_variant-${variant}`,
        `fk-btn_size-${size}`,
        {"disabled": disabled || loading},
        className,
    )

    return (
        <>
            {modalType
                ? (<Modal
                    {...modalData}
                    type={modalType}
                >
                    {getButton()}
                </Modal>)
                : getButton()
            }
        </>
    )

    function getButton() {
        // <a>
        if(href !== "-1") {
            return (
                <a {...attrs} className={`${className} fk-text-skip`} onClick={hClick} href={href} ref={setRef}>
                    {loading ? <Loading variant="auto"/> : getContent()}
                </a>
            )
        }

        // <button>
        else {
            return (
                <button {...attrs} className={className} onClick={hClick} disabled={disabled || loading} ref={setRef}>
                    {loading ? <Loading variant="auto"/> : getContent()}
                </button>
            )
        }
    }
    
    function getContent() {
        return (
            <>
                {children && children}

                {iconBefore && (
                    <SVG
                        className="fk-btn__svg-before"
                        size={iconBeforeSize}
                        icon={iconBefore}
                    />
                )}
                
                {title && (
                    <span className="fk-btn__title">
                        {title}
                    </span>
                )}

                {iconAfter && (
                    <SVG
                        className="fk-btn__svg-after"
                        size={iconAfterSize}
                        icon={iconAfter}
                    />
                )}
            </>
        )
    }
}

export default Button