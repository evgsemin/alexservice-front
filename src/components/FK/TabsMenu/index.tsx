import React, { useEffect, useState } from 'react'

import { cl } from '../../../utilities/cl'
import { length } from '../../../utilities/length'
import { safeCall } from '../../../utilities/safeCall'

import Container from '../Container'


interface ITab {
    className?:string
    title:string
    text?:string // <p>{tab.text}</p>
    content?:any
}

const TabsMenu:React.FC<{
    className?:string
    size?:string
    tabs?:ITab[]
    startActive?:number
    active?:number
    containerProps?:any
    onChange?:any
}> = ({
    className,
    size = "sm",
    tabs = [],
    startActive,
    active = 0,
    containerProps = {},
    onChange,
}) => {

    const [iActive, setIActive] = useState((startActive !== undefined) ? startActive : active)

    useEffect(() => {
        if(!isNaN(active)) {
            setIActive(active)
        }
    }, [active])

    const hClick = (e:any) => {
        const tab = e.target.closest(".tab")
        if(tab) {
            const nextTabId = Number(tab.dataset.tabId)
            if(!isNaN(nextTabId)) {
                setIActive(nextTabId)
                safeCall(onChange, {
                    value: nextTabId,
                }, e)
            }
        }
    }

    className = cl(
        "fk-tabs-menu",
        `fk-tabs-menu_size-${size}`,
        className,
    )

    if(!length(tabs)) return null

    return (
        <div className={className}>
            <div className="fk-tabs-menu__header" onClick={hClick}>
                <Container {...containerProps}>
                    <div className="wrap">
                        {tabs.map((tab, index) => (
                            <div
                                key={index}
                                data-tab-id={index}
                                className={cl(
                                    "tab",
                                    "tab-controller",
                                    {"active": index === iActive},
                                    tab.className
                                )}
                            >
                                <span>{tab.title}</span>
                            </div>
                        ))}
                    </div>
                </Container>
            </div>

            <div className="fk-tabs-menu__body">
                <Container {...containerProps}>
                    {tabs.map((tab, index) => (
                        <section
                            key={index}
                            className={cl(
                                "tab",
                                {"active": index === iActive},
                                tab.className
                            )}
                        >
                            <header className="tab-header">
                                <h2>{tab.title}</h2>
                            </header>
                            {tab.text && <p>{tab.text}</p>}
                            {tab.content && tab.content}
                        </section>
                    ))}
                </Container>
            </div>
        </div>
    )
}

export default TabsMenu