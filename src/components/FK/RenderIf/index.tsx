import React from 'react'

import { renderIf } from '../../../utilities/renderIf'


const RenderIf:React.FC<{
    className?:string
    isHide?:boolean
    condition:any
    children:any
}> = ({
    className,
    isHide,
    condition,
    children,
}) => {

    return renderIf({
        className,
        isHide,
        condition,
        children,
    })
}

export default RenderIf