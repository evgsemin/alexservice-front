import React, { useRef, useState, useEffect } from 'react'

import { cl } from '../../../utilities/cl'
import { isBrowser, isNumb } from '../../../utilities/is'


const Sticky:React.FC<{
    className?:string
    offset?:number
    offsetElementSelector?:string
    children:any
}> = ({
    className,
    children,
    offset = 0,
    offsetElementSelector,
}) => {

    const [position, setPosition] = useState("none")
    const [styles, setStyles] = useState({})
    const refSticky = useRef<any>()

    useEffect(() => {
        if(isBrowser()) {
            updateStickied()
            window.addEventListener("scroll", updateStickied)
            return () => window.removeEventListener("scroll", updateStickied)
        }
    }, [])

    className = cl(
        "fk-sticky",
        position,
        className,
    )

    return (
        <div
            className={className}
            ref={refSticky}
            style={styles}
        >
            {children}
        </div>
    )

    function updateStickied() {
        let neededOffset = isNumb(offset) && !isNaN(offset) ? offset : 0

        if(offsetElementSelector) {
            try {
                const offsetElement = document.querySelector(offsetElementSelector)
                if(offsetElement) {
                    const offsetElementH = offsetElement.clientHeight
                    neededOffset += offsetElementH
                }
            } catch(e) {}
        }

        const elem = refSticky.current
        const elemH = elem?.clientHeight || 0
        const isElemHeight = (window.innerHeight - (elemH+neededOffset)) > 0

        const parent = elem?.parentElement

        let nextStyles = {}
        let position = "error"

        if(parent && isElemHeight) {
            const parentH = parent?.clientHeight || 0
            const parentW = parent?.clientWidth || 0
            const parentLeft = parent.getBoundingClientRect().left + window.scrollX
            const parentPL = Number(window.getComputedStyle(parent).getPropertyValue("padding-left").replace("px", ""))
            const parentPR = Number(window.getComputedStyle(parent).getPropertyValue("padding-right").replace("px", ""))
            const parentPT = Number(window.getComputedStyle(parent).getPropertyValue("padding-top").replace("px", ""))
            const parentPB = Number(window.getComputedStyle(parent).getPropertyValue("padding-bottom").replace("px", ""))
            const isParentHeight = ((parentH-parentPT-parentPB) - (elemH+neededOffset)) > 0

            if(isParentHeight) {
                const scroll = window.scrollY

                const pointTop = (parent.getBoundingClientRect().top + scroll) + parentPT - neededOffset
                const pointBottom = (parent.getBoundingClientRect().bottom + scroll) - parentPB - elemH - neededOffset

                // Решение

                if(scroll <= pointTop) {
                    position = "none"
                    nextStyles = {}
                }

                else if(scroll < pointBottom) {
                    position = "fixed"
                    nextStyles = {
                        position: "fixed",
                        top: `${neededOffset}px`,
                        left: `${parentLeft+parentPL}px`,
                        width: `${parentW - parentPL - parentPR}px`
                    }
                }

                else if(scroll >= pointBottom) {
                    position = "absolute"
                    nextStyles = {
                        position: "absolute",
                        top: `${parentH - parentPB - elemH}px`,
                        left: 0,
                        right: 0,
                    }
                }   
            }
        }

        setPosition(position)
        setStyles(nextStyles)
    }
}

export default Sticky