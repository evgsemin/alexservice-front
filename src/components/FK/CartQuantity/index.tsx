import React, { useEffect, useState, useRef } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'

import Input from '../Input'
import SVG from '../SVG'


let lastValue = 1 // костыль

const CartQuantity:React.FC<{
    className?:string
    startValue?:number
    value?:number
    name?:any
    onChange?:any
}> = ({
    className,
    startValue = 1,
    value,
    name,
    onChange,
}) => {

    const refCartQuantity = useRef(document.createElement("div"))

    const [iValue, setIValue] = useState(typeof value === "number" ? value : startValue)

    useEffect(() => {
        refCartQuantity.current.addEventListener("wheel", hScroll, {passive: false})
        return () => refCartQuantity.current.removeEventListener("wheel", hScroll)
    }, [])

    useEffect(() => {
        change(value)
    }, [value])

    function change(val:number | undefined, event?:any) {
        if(typeof val === "number" && val >= 1) {
            setIValue(val)
            lastValue = val
            safeCall(onChange, {
                name,
                value: val,
            }, event)
        }
    }

    const hQuantity = (type:string) => {
        if(type === "minus") {
            change(iValue - 1)
        } else if(type === "plus") {
            change(iValue + 1)
        }
    }

    const hKeyDown = (vals:any, e:any) => {
        if(e.keyCode === 38) {
            hQuantity("plus")
        } else if(e.keyCode === 40) {
            hQuantity("minus")
        }
    }

    const filter = (val:any) => {
        val = Number(val)
        if(isNaN(val) || val < 1) return false
        return true
    }

    className = cl(
        "fk-cart-quantity",
        className,
    )

    return (
        <div className={className} ref={refCartQuantity}>
            <Input
                type="text"
                size="lg"
                iconBefore={<SVG
                    icon="MinusCircleFilled"
                    variant="prim"
                    onClick={() => hQuantity("minus")}
                    disabled={iValue === 1}
                />}
                iconAfter={<SVG
                    icon="PlusCircleFilled"
                    variant="prim"
                    onClick={() => hQuantity("plus")}
                />}
                placeholder={`${startValue}`}
                value={iValue}
                filter={filter}
                onKeyDown={hKeyDown}
            />

            <div className="fk-cart-quantity__label">Количество</div>
        </div>
    )

    function hScroll(e:any) {
        if(e) {
            if(e.deltaY > 0) {
                change(lastValue - 1)
            }
            if(e.deltaY < 0) {
                change(lastValue + 1)
            }
            e.preventDefault ? e.preventDefault() : (e.returnValue = false)
            return false
        }
    }
}

export default CartQuantity