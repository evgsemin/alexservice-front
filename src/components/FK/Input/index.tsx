import React, { useEffect, useState } from 'react'
import InputPhone from 'react-input-mask'

import { cl } from '../../../utilities/cl'
import { isFunc } from '../../../utilities/is'
import { safeCall } from '../../../utilities/safeCall'


const Input:React.FC<{
    className?:string
    type?: "text" | "search" | "textarea" | "phone" | "number" | string
    startValue?:string | number
    value?:string | number
    min?:number // only for (type: "text" | "number")
    max?:number // only for (type: "text" | "number")
    filter?:any // must returns true or false
    placeholder?:string
    variant?: "normal" | "inverse"
    title?:string
    size?:string
    iconBefore?:any
    iconAfter?:any
    iconBeforeSize?:string
    iconAfterSize?:string
    href?:string
    onClick?:any
    onChange?:any
    onKeyDown?:any
    onKeyUp?:any
    name?:string
    payload?:any
    attrs?:any
    disabled?:boolean
}> = ({
    className,
    type = "text",
    startValue,
    value,
    min,
    max,
    filter,
    placeholder = "",
    variant = "normal",
    title,
    size = "md",
    iconBefore,
    iconAfter,
    iconBeforeSize,
    iconAfterSize,
    onClick,
    onChange,
    onKeyDown,
    onKeyUp,
    name,
    payload,
    attrs = {},
    disabled,
}) => {

    const [iValue, setIValue] = useState(value)

    useEffect(() => {
        if(startValue !== undefined) {
            setIValue(startValue)
        }
    }, [])

    useEffect(() => {
        change(value)
    }, [value])

    function change(val:any, event?:any) {
        if(isFilter(val)) {
            setIValue(val)
            safeCall(onChange, {
                name,
                payload,
                value: val,
            }, event)
        }
    }

    const hClick = (event:any) => {
        if(disabled !== true) {
            safeCall(onClick, {
                name,
                payload,
                value: iValue,
            }, event)
        }
    }

    const hChange = (event:any) => {
        if(disabled !== true) {
            change(event.target.value, event)
        }
    }

    const hKeyDown = (e:any) => {
        safeCall(onKeyDown, {
            name,
            payload,
            value: iValue,
        }, e)
    }
    const hKeyUp = (e:any) => {
        safeCall(onKeyUp, {
            name,
            payload,
            value: iValue,
        }, e)
    }

    className = cl(
        "fk-input",
        `fk-input_type-${type}`,
        `fk-input_variant-${variant}`,
        `fk-input_size-${size}`,
        {"fk-input_with-icon-before": iconBefore ? true : false},
        {"fk-input_with-icon-after": iconAfter ? true : false},
        className
    )

    return (
        <div className={className}>
            {(type !== "textarea" && type !== "phone" )&& (
                <input
                    {...attrs}
                    className="fk-input__field"
                    disabled={disabled}
                    title={title}
                    name={name}
                    type={type}
                    value={iValue}
                    placeholder={placeholder}
                    onClick={hClick}
                    onChange={hChange}
                    onKeyDown={hKeyDown}
                    onKeyUp={hKeyUp}
                />
            )}
            {type === "phone" && (
                <InputPhone
                    {...attrs}
                    className="fk-input__field"
                    disabled={disabled}
                    title={title}
                    name={name}
                    type={type}
                    value={iValue}
                    placeholder={placeholder}
                    onClick={hClick}
                    onChange={hChange}
                    onKeyDown={hKeyDown}
                    onKeyUp={hKeyUp}
                    mask="+7 (999) 999-99-99"
                    maskChar="_"
                />
            )}
            {type === "textarea" && (
                <textarea
                    {...attrs}
                    className="fk-input__field"
                    disabled={disabled}
                    title={title}
                    name={name}
                    value={iValue}
                    onClick={hClick}
                    onChange={hChange}
                    onKeyDown={hKeyDown}
                    onKeyUp={hKeyUp}
                >
                    {placeholder}
                </textarea>
            )}
            {iconBefore && (
                <div
                    className={cl(
                        "fk-input__icon-before",
                        {[`icon-size-${iconBeforeSize}`]: iconBeforeSize ? true : false}
                    )}>
                    {iconBefore}
                </div>
            )}
            {iconAfter && (
                <div
                    className={cl(
                        "fk-input__icon-after",
                        {[`icon-size-${iconAfterSize}`]: iconAfterSize ? true : false}
                    )}>
                    {iconAfter}
                </div>
            )}
        </div>
    )

    function isFilter(val:any) {
        if(isFunc(filter)) {
            if(!filter(val)) return false
        }

        if(typeof min === "number" || typeof max === "number") {
            // text
            if(type === "text" || type === "textarea") {
                if(typeof min === "number" && val.length < min) {
                    return false
                }
                if(typeof max === "number" && val.length > max) {
                    return false
                }
                return true
            }
            // number
            else if(type === "number") {
                if(typeof min === "number" && val < min) {
                    return false
                }
                if(typeof max === "number" && val > max) {
                    return false
                }
                return true
            }
            // other
            else {
                return true
            }
        } else {
            return true
        }
    }
}

export default Input