import React from 'react'

import { cl } from '../../../utilities/cl'


const Group: React.FC<{
    className?:string
    size?:string
    stretch?:boolean
    direction?: "row" | "row-reverse" | "column" | "column-reverse"
    alignY?: "start" | "center" | "end" | "between" | "around"
    alignX?: "start" | "center" | "end" | "between" | "around"
    nowrap?:boolean
    children:any
}> = ({
    className,
    size = "xs",
    stretch = false,
    direction = "row",
    alignY = "start",
    alignX = "start",
    nowrap = false,
    children,
}) => {

    className = cl(
        "fk-group",
        `fk-group_size-${size}`,
        {"fk-group_stretch": stretch},
        className,
    )

    const wrapClassName = cl(
        "fk-group__wrap",
        getFlexClasses(),
    )

    return (
        <div className={className}>
            <div className={wrapClassName}>
                {children}
            </div>
        </div>
    )

    function getFlexClasses() {
        const isX = direction.indexOf("row") !== -1
        const flexClasses = cl(
            `fk-flex-d-${direction}`,
            `fk-flex-${isX ? "j" : "a"}-${alignX}`,
            `fk-flex-${isX ? "a" : "j"}-${alignY}`,
            {"fk-flex-w-wrap": !nowrap},
            {"fk-flex-w-nowrap": nowrap},
        )
        return flexClasses
    }
}

export default Group