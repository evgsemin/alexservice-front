import React from 'react'

export default ({ title = "Отмечено" }) => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3.014 2.117">
        <title>{title}</title>
        <g strokeWidth="40.559"><path d="M2.982.034a.111.111 0 00-.16 0L1.008 1.848l-.82-.825a.111.111 0 00-.157.158l.899.903a.111.111 0 00.157 0L2.98.191a.111.111 0 00.002-.157z"/></g></svg>
)