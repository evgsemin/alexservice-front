import React from 'react'

import { cl } from '../../../utilities/cl'
import { isFunc } from '../../../utilities/is'
import { safeCall } from '../../../utilities/safeCall'

import Approved from './Approved'
import ArrowDown from './ArrowDown'
import ArrowLeft from './ArrowLeft'
import ArrowRight from './ArrowRight'
import ArrowUp from './ArrowUp'
import Cancel from './Cancel'
import CancelCircle from './CancelCircle'
import Check from './Check'
import Clock from './Clock'
import Fast from './Fast'
import Location from './Location'
import Menu from './Menu'
import MinusCircleFilled from './MinusCircleFilled'
import NewTab from './NewTab'
import Phone from './Phone'
import PhoneFilled from './PhoneFilled'
import PlusCircleFilled from './PlusCircleFilled'
import Power from './Power'
import Pulse from './Pulse'
import Repair from './Repair'
import Search from './Search'
import Settings from './Settings'
import Shield from './Shield'


const SVG: React.FC<{
    className?:string,
    icon:string,
    title?:string,
    variant?:string,
    size?:string,
    onClick?:any,
    disabled?:boolean,
}> = ({
    className,
    icon,
    title,
    variant,
    size,
    onClick,
    disabled = false,
}) => {

    const hClick = (event:any) => {
        if(disabled !== true) {
            safeCall(onClick, event)
        }
    }

    className = cl(
        "fk-svg",
        "fk-text-skip",
        {"fk-svg_button": isFunc(onClick)},
        {[`fk-svg_variant-${variant}`]: variant ? true : false},
        {[`fk-svg_size-${size}`]: size ? true : false},
        {"disabled": disabled},
        className,
    )

    return (
        <i
            className={className}
            onClick={hClick}
        >
            {icon === "Approved" && <Approved title={title}/>}
            {icon === "ArrowDown" && <ArrowDown title={title}/>}
            {icon === "ArrowLeft" && <ArrowLeft title={title}/>}
            {icon === "ArrowRight" && <ArrowRight title={title}/>}
            {icon === "ArrowUp" && <ArrowUp title={title}/>}
            {icon === "Cancel" && <Cancel title={title}/>}
            {icon === "CancelCircle" && <CancelCircle title={title}/>}
            {icon === "Check" && <Check title={title}/>}
            {icon === "Clock" && <Clock title={title}/>}
            {icon === "Fast" && <Fast title={title}/>}
            {icon === "Location" && <Location title={title}/>}
            {icon === "Menu" && <Menu title={title}/>}
            {icon === "MinusCircleFilled" && <MinusCircleFilled title={title}/>}
            {icon === "NewTab" && <NewTab title={title}/>}
            {icon === "Phone" && <Phone title={title}/>}
            {icon === "PhoneFilled" && <PhoneFilled title={title}/>}
            {icon === "PlusCircleFilled" && <PlusCircleFilled title={title}/>}
            {icon === "Power" && <Power title={title}/>}
            {icon === "Pulse" && <Pulse title={title}/>}
            {icon === "Repair" && <Repair title={title}/>}
            {icon === "Search" && <Search title={title}/>}
            {icon === "Settings" && <Settings title={title}/>}
            {icon === "Shield" && <Shield title={title}/>}
        </i>
    )
}

export default SVG