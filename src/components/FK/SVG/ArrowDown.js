import React from 'react'

export default ({ title = "Вниз" }) => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4.233 2.309">
        <title>{title}</title>
        <g strokeWidth="20.625"><path d="M4.177.056a.192.192 0 00-.272 0L2.117 1.845.328.056a.192.192 0 00-.272.272l1.925 1.925a.192.192 0 00.272 0L4.177.328a.192.192 0 000-.272z"/></g></svg>
)