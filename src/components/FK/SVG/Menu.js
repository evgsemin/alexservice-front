import React from 'react'

export default ({ title = "Меню" }) => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.021 4.233">
        <title>{title}</title>
        <path d="M.235 0a.235.235 0 100 .47h5.55a.235.235 0 100-.47zm0 1.882a.235.235 0 100 .47h5.55a.235.235 0 100-.47zm0 1.881a.235.235 0 100 .47h5.55a.235.235 0 100-.47z"/></svg>
)