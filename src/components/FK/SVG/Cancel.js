import React from 'react'

export default ({ title = "Отмена" }) => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 4.233 4.233">
        <title>{title}</title>
        <path d="M2.432 2.117L4.168.38a.223.223 0 00-.315-.315L2.117 1.802.38.065A.223.223 0 10.065.38l1.737 1.737L.065 3.853a.223.223 0 10.315.315l1.737-1.736 1.736 1.736a.223.223 0 00.315-.315z"/></svg>
)