import React, { useState, useEffect, useRef, lazy } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { isElemAtView } from '../../../utilities/is'


const LazyMap:React.FC<{
    className?:string
    map:string
    offset?:number
    timeInterval?:number
    attrs?:any
}> = ({
    className,
    map = "",
    offset = 200,
    timeInterval = 750,
    attrs = {},
}) => {

    const refMap = useRef(document.createElement("div"))
    const [lazyMap, setLazyMap] = useState<string>("")
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const lazyInterval = setInterval(() => {
            if(refMap.current) {
                if(isElemAtView({
                    elem: refMap.current,
                    offset
                })) {
                    setLoaded(true)
                    clearInterval(lazyInterval)
                }
            }
        }, timeInterval)
    }, [])

    useEffect(() => {
        if(loaded) {
            setLazyMap(map)
        }
    }, [map, loaded])

    useEffect(() => {
        if(loaded) {
            innerMap()
        }
    }, [lazyMap])

    className = cl(
        "fk-lazy-map",
        className,
    )

    return (
        <div
            {...attrs}
            className={className}
            ref={refMap}
        ></div>
    )

    function innerMap() {
        if(refMap.current) {
            refMap.current.innerHTML = map
        }
    }
}

export default LazyMap