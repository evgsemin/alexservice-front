import React, { useEffect, useState, useRef } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'
import { outside } from '../../../utilities/outside'
import { IImageProductGallery } from '../../../interfaces'

import SVG from '../SVG'
import LazyImage from '../LazyImage'


let lastActive = 0 // костыль

const FullView:React.FC<{
    className?:string
    images?:IImageProductGallery[]
    startActive?:number
    active?:number
    opened:boolean
    onClose:any
    onChange?:any
}> = ({
    className,
    images = [],
    startActive,
    active,
    opened,
    onClose,
    onChange,
}) => {

    const refScroll = useRef(document.createElement("div"))
    const refImage = useRef(document.createElement("div"))
    const [iActive, setIActive] = useState(typeof startActive === "number" ? startActive : (typeof active === "number" ? active : 0))
    const [iOpened, setIOpened] = useState(opened)

    useEffect(() => {
        toggle(active)
    }, [active])

    useEffect(() => {
        if(iOpened) {
            document.body.classList.add("fk-of-y-hidden")
        }
        window.addEventListener("keydown", hKeyDown)
        return () => window.removeEventListener("keydown", hKeyDown)
    }, [])

    useEffect(() => {
        if(!iOpened) {
            document.body.classList.remove("fk-of-y-hidden")
        } else if(iOpened) {
            document.body.classList.add("fk-of-y-hidden")
        }
    }, [iOpened])

    useEffect(() => {
        if(iOpened === false) onClose()
    }, [iOpened])

    useEffect(() => {
        if(opened === true) setIOpened(opened)
    }, [opened])

    const hClose = () => setIOpened(false)

    const hImageClick = (e:any) => {
        if(e.target === refImage.current) {
            hClose()
        }
    }

    const hChange = (e:any) => {
        const thumb = e.target.closest(".thumb")
        if(thumb) {
            const nextActive = Number(thumb.dataset.imageId)
            toggle(nextActive)
        }
    }

    const hScroll = (e?:any) => {
        if(e.deltaY > 0) {
            refScroll.current.scrollLeft += 30
        }
        if(e.deltaY < 0) {
            refScroll.current.scrollLeft -= 30
        }
    }

    className = cl(
        "fk-full-view",
        {"opened": iOpened},
        className,
    )

    return (
        outside(
            <>
                {iOpened && (
                    <div className={className}>
                        <SVG
                            className="icon-close"
                            variant="light"
                            icon="Cancel"
                            title="Закрыть"
                            size="sm"
                            onClick={hClose}
                        />
                        
                        <div className="fk-full-view__image" onClick={hImageClick} ref={refImage}>
                            {images[iActive] && (
                                <>
                                    <LazyImage
                                        className="image"
                                        type="img"
                                        format="md"
                                        alt={images[iActive].alt}
                                        image={images[iActive].image}
                                        timeInterval={200}
                                    />
                                    <SVG
                                        className="icon-prev"
                                        variant="light"
                                        icon="ArrowLeft"
                                        title="Предыдущий"
                                        size="sm"
                                        onClick={() => toggle("prev")}
                                    />
                                    <SVG
                                        className="icon-next"
                                        variant="light"
                                        icon="ArrowRight"
                                        title="Следующий"
                                        size="sm"
                                        onClick={() => toggle("next")}
                                    />
                                </>
                            )}
                            {!(images[iActive]) && (
                                <div className="empty">
                                    Изображений для просмотра нет.
                                </div>
                            )}
                        </div>
                        
                        <div className="fk-full-view__thumbs" onClick={hChange}>
                            <div className="hidden">
                                {images.length && (
                                    <div className="scroll" onWheel={hScroll} ref={refScroll}>
                                        <div className="wrap">
                                            {images.map((img, index) => (
                                                <LazyImage
                                                    key={index}
                                                    className={cl(
                                                        "thumb",
                                                        {"active": index === iActive},
                                                    )}
                                                    attrs={{"data-image-id": index}}
                                                    type="div"
                                                    format="md"
                                                    alt={img.alt}
                                                    image={img.thumb || img.image}
                                                    timeInterval={200}
                                                />
                                            ))}
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                )}
            </>
        )
    )

    function toggle(next?:number | string) {
        if(typeof next === "number") {
            change(next)
        } else if(next === "next") {
            change(lastActive + 1)
        } else if(next === "prev") {
            change(lastActive - 1)
        }
    }

    function change(nextActive:number) {
        if(typeof nextActive === "number") {
            if(nextActive < 0) nextActive = (images.length-1)
            if(nextActive > (images.length-1)) nextActive = 0
            setIActive(nextActive)
            lastActive = nextActive
            safeCall(onChange, {
                value: iActive,
            })
        }
    }

    function hKeyDown(e:any) {
        if(e.keyCode === 27) hClose()
        if(e.keyCode === 37) toggle("prev")
        if(e.keyCode === 39) toggle("next")
    }
}

export default FullView