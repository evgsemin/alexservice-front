import React from 'react'

import { cl } from '../../utilities/cl'
import { IBullet } from './interfaces'


const BulletItem:React.FC<IBullet> = ({
    className,
    variant,
    icon,
    title,
    subtitle,
}) => (

    <div className={cl(
        "BulletsItem",
        `BulletsItem_variant-${variant}`,
        className,
    )}>
        <div className="left">
            <div className="icon">
                {icon}
            </div>
        </div>
        <div className="right">
            <h4 className="title">{title}</h4>
            <div className="subtitle">{subtitle}</div>
        </div>
    </div>
)

export default BulletItem