export interface IBullet {
    className?:string
    variant:string
    icon:any
    title:string
    subtitle:string
}