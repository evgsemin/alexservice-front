import React from 'react'

import { cl } from '../../utilities/cl'
import { useIs } from '../../utilities/media'
import { isObj, isNumb } from '../../utilities/is'
import { IBullet } from './interfaces'

import Section from '../FK/Section'
import Grid from '../FK/Grid'

import BulletsItem from './Item'


const Bullets:React.FC<{
    className?:string
    type: "section" | "line"
    cols?:number | any
    title?:string // only `type=section`
    titleTag?:string
    titleTextSkip?:boolean
    titleAlign?:string // only `type=section`
    containerOn?:boolean // only `type=section`
    sectionProps?:any // only `type=section`
    itemVariant?:string
    items:IBullet[]
}> = ({
    className,
    type,
    cols,
    title = "Почему стоит выбрать наш сервис?",
    titleTag,
    titleTextSkip,
    titleAlign = "left",
    containerOn = true,
    sectionProps = {},
    itemVariant,
    items = [],
}) => {

    const isDesktopAndUp = useIs("desktopAndUp")

    className = cl(
        "Bullets",
        `Bullets_type-${type}`,
        className,
    )

    // Секцией

    if(type === "section") {
        if(!isNumb(cols) && !isObj(cols)) {
            cols = {
                desktopAndUp: 2,
                tabletAndDown: 1,
            }
        }

        if(!itemVariant) itemVariant = "x"

        return (
            <Section
                {...sectionProps}
                className={className}
                align={titleAlign}
                header={{
                    title,
                    titleTag,
                    titleTextSkip,
                }}
                size="lg"
                containerOn={containerOn}
            >
                <div className="Bullets__container">
                    <div className="place">
                        <Grid
                            size={isDesktopAndUp ? "xl" : "md"}
                            cols={cols}
                        >
                            {items.map((item, index) => (
                                <BulletsItem
                                    key={index}
                                    variant={itemVariant}
                                    className={item.className}
                                    icon={item.icon}
                                    title={item.title}
                                    subtitle={item.subtitle}
                                />
                            ))}
                        </Grid>
                    </div>
                </div>
            </Section>
        )
    }

    // Строкой

    if(type === "line") {
        if(!isNumb(cols) && !isObj(cols)) {
            cols = {
                desktopAndUp: 3,
                tablet: 2,
                mobileAndDown: 1,
            }
        }

        if(!itemVariant) itemVariant = "y"

        return (
            <div className={className}>
                <Grid
                    size={isDesktopAndUp ? "xl" : "md"}
                    cols={cols}
                >
                    {items.map((item, index) => (
                        <BulletsItem
                            key={index}
                            variant={itemVariant}
                            className={item.className}
                            icon={item.icon}
                            title={item.title}
                            subtitle={item.subtitle}
                        />
                    ))}
                </Grid>
            </div>
        )
    }

    return null
}

export default Bullets