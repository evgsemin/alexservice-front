import React from 'react'

import SVG from '../FK/SVG'

import Bullets from './Bullets'

const items = [
    {
        icon: <SVG icon="Shield"/>,
        title: "Гарантия на ремонт",
        subtitle: "По окончанию ремонта мастер в обязательном порядке выдаёт гарантийный талон на срок от 6 до 24 месяцев (в зависимости от типа поломки и техники).",
    },
    {
        icon: <SVG icon="Settings"/>,
        title: "Запчасти в наличии",
        subtitle: "У нас всегда есть запчасти для каждой модели техники. Оригинальные или качественные заменители — на Ваш выбор.",
    },
    {
        icon: <SVG icon="Pulse"/>,
        title: "Держим руку на пульсе качества",
        subtitle: "В случае плохого сервиса обратитесь к нам любым удобным способом — обязательно разберёмся в каждом вопросе.",
    },
    {
        icon: <SVG icon="Fast"/>,
        title: "Высокая скорость приезда",
        subtitle: "В любое время в рабочие часы мастер готов выехать на ремонт в течении 15 минут.",
    },
    {
        icon: <SVG icon="Power"/>,
        title: "Работаем быстро, отвечаем сразу",
        subtitle: "Не верите? Проверьте сами.",
    },
]

const BulletsContainer = props => (
    <Bullets
        className={props.className}
        type={props.type}
        cols={props.cols}
        title={props.title}
        titleTag={props.titleTag}
        titleTextSkip={props.titleTextSkip}
        titleAlign={props.titleAlign}
        itemVariant={props.itemVariant}
        sectionProps={props.sectionProps}
        containerOn={props.containerOn}
        items={items}
    />
)

export default BulletsContainer