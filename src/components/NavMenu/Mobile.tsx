import React, { useEffect, useState, useRef } from 'react'
import { Link } from 'react-router-dom'

import { useIsBrowser } from '../../utilities/useIsBrowser'
import { document } from '../../utilities/uni'
import { INavLink } from '../../interfaces'
import { cl } from '../../utilities/cl'
import { outside } from '../../utilities/outside'

import SVG from '../FK/SVG'


const NavMenuMobile:React.FC<{
    className?:string
    menuTitle:string
    size?: "mr" | "xs" | "sm" | "md" | "lg" | "mg"
    variant?: "normal" | "inverse"
    navLinks:INavLink[]
    groupProps?:any
}> = ({
    className,
    menuTitle,
    size = "sm",
    variant = "normal",
    navLinks = [],
}) => {

    const isBrowser = useIsBrowser()

    const [opened, setOpened] = useState(false)
    const [openedSubmenus, setOpenedSubmenus] = useState<string[]>([])

    useEffect(() => {
        document.body.style.overflowY = opened ? "hidden" : "auto"
    }, [opened])

    const hSubmenuToggle = (e:any) => {
        const link = e.target.closest(".link")
        if(link) {
            const submenuId = link.dataset.submenuId
            if(submenuId) {
                const nextOpenedSubmenus = [...openedSubmenus]
                const submenuIndex = nextOpenedSubmenus.indexOf(submenuId)

                // Открываем подменю
                if(submenuIndex === -1) {
                    nextOpenedSubmenus.push(submenuId)
                }
                // Закрываем подменю
                else {
                    nextOpenedSubmenus.splice(submenuIndex, 1)
                }

                setOpenedSubmenus(nextOpenedSubmenus)
            }
        }
    }

    className = cl(
        "NavMenuMobile",
        className,
    )

    return (
        <>
            <div className={className}>
                <SVG
                    variant={variant === "normal" ? "dark" : "light"}
                    size={size}
                    icon="Menu"
                    title="Открыть меню"
                    onClick={() => setOpened(!opened)}
                />
            </div>

            {isBrowser && outside(getMenu())}
            {!isBrowser && getMenu()}
        </>
    )

    function getMenu() {
        return (
            <div className={cl("NavMenuMobileMenu", {"opened": opened})}>
                <div className="NavMenuMobileMenu__header">
                    <div className="title">{menuTitle}</div>
                    <SVG
                        variant="dark"
                        size="sm"
                        icon="Cancel"
                        title="Закрыть меню"
                        onClick={() => setOpened(false)}
                    />
                </div>

                <nav className="NavMenuMobileMenu__list" onClick={hSubmenuToggle}>
                    {getMenuList(navLinks, 0)}
                </nav>
            </div>
        )
    }

    function getMenuList(links:INavLink[], level:number) {
        return (
            <>
                {links.map((link, index) => {
                    const isSubmenu = (link.links || []).length > 0
                    const submenuId = `${level}-${index}`
                    const submenuOpened = openedSubmenus.indexOf(submenuId) !== -1
                    const submenuOffset = `${(level+1)*5}%`

                    // Список подменю
                    if(isSubmenu) {
                        return (
                            <>
                                <div
                                    key={index}
                                    className={cl("link", {"opened": submenuOpened})}
                                    title={link.seoTitle || link.title}
                                    data-submenu-id={submenuId}
                                >
                                    {link.title}
                                    <SVG icon="ArrowDown" title={opened ? "Свернуть" : "Раскрыть"}/>
                                </div>

                                <div className={cl("submenu", {"opened": submenuOpened})}>
                                    <div style={{paddingLeft: submenuOffset}}>
                                        {getMenuList(link.links, level+1)}
                                    </div>
                                </div>
                            </>
                        )
                    }

                    // Ссылка
                    else {
                        return (
                            <Link
                                key={index}
                                className="link fk-text-skip"
                                to={link.href}
                                title={link.seoTitle || link.title}
                                target={link.target}
                            >
                                {link.title}
                            </Link>
                        )
                    }
                })}
            </>
        )
    }
}

export default NavMenuMobile