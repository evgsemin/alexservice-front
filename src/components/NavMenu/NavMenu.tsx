import React from 'react'

import { INavLink } from '../../interfaces'
import { cl } from '../../utilities/cl'
import { useMedia } from '../../utilities/media'

import NavMenuDesktop from './Desktop'
import NavMenuMobile from './Mobile'


const NavMenu:React.FC<{
    className?:string

    size?: "mr" | "xs" | "sm" | "md" | "lg" | "mg"
    variant?: "normal" | "inverse"
    navLinks:INavLink[]
    hidden?:boolean

    // Desktop
    groupProps?:any
    parentElementClassName:string

    // Mobile
    menuTitle:string
}> = ({
    className,
    menuTitle,
    size,
    variant,
    navLinks,
    groupProps,
    hidden,
    parentElementClassName
}) => {

    const currentMenu = useMedia({
        mobileAndDown:
            <NavMenuMobile
                menuTitle={menuTitle}
                size={size}
                variant={variant}
                navLinks={navLinks}
            />,
        tabletAndUp:
            <NavMenuDesktop
                size={size}
                variant={variant}
                navLinks={navLinks}
                groupProps={groupProps}
                parentElementClassName={parentElementClassName}
            />,
    }, null, [hidden, navLinks])

    className = cl(
        "NavMenu",
        className,
    )

    return (
        <div className={className}>
            {currentMenu}
        </div>
    )
}

export default NavMenu