import React, { useEffect, useState, useRef } from 'react'
import { Link } from 'react-router-dom'

import { document } from '../../utilities/uni'
import { INavLink } from '../../interfaces'
import { cl } from '../../utilities/cl'
import { useIsBrowser } from '../../utilities/useIsBrowser'
import { outside } from '../../utilities/outside'

import Group from '../FK/Group'
import SVG from '../FK/SVG'


const NavMenuDesktop:React.FC<{
    className?:string
    size?: "basic" | "mr" | "xs" | "sm" | "md" | "lg" | "mg"
    variant?: "normal" | "inverse"
    navLinks:INavLink[]
    groupProps?:any
    parentElementClassName:string
}> = ({
    className,
    size = "basic",
    variant = "normal",
    navLinks = [],
    groupProps = {},
    parentElementClassName,
}) => {

    const isBrowser = useIsBrowser()

    const refNavMenuDesktop = useRef(document.createElement("div"))

    const [submenuOpened, setSubmenuOpened] = useState(-1)
    const [submenuLinks, setSubmenuLinks] = useState<any[]>([])
    const [outisdePosition, setOutisdePosition] = useState({top:"40px",left:"40px",minWidth:"100px"})

    // Получаем все подменю
    useEffect(() => {
        const nextSubmenuLinks:any[] = []
        navLinks.map((link, submenuId) => {
            const links = link.links || []
            if(links.length) {
                links.map(link => {
                    nextSubmenuLinks.push({
                        ...link,
                        submenuId,
                    })
                })
            }
        })
        setSubmenuLinks(nextSubmenuLinks)
    }, [navLinks])

    // Позиция списка
    useEffect(() => {
        if(submenuOpened !== -1) {
            updateOutsidePosition()
            window.addEventListener("scroll", updateOutsidePosition)
        } else {
            window.removeEventListener("scroll", updateOutsidePosition)
        }
    }, [submenuOpened])

    // Закрытие при клике в другом месте
    useEffect(() => {
        const closeByOutsideClick = (e:any) => {
            if(refNavMenuDesktop.current !== null) {
                if(!refNavMenuDesktop.current.contains(e.target)) {
                    setSubmenuOpened(-1)
                    window.removeEventListener("click", closeByOutsideClick)
                }
            }
        }
        if(submenuOpened !== -1) {
            window.addEventListener("click", closeByOutsideClick)
        } else {
            window.removeEventListener("click", closeByOutsideClick)
        }
    }, [submenuOpened])

    const hClick = (event:any) => {
        const link = event.target.closest(".link")
        if(link) {
            const submenuIndex = Number(link.dataset.submenuId)
            // Открываем подменю
            if(submenuOpened !== submenuIndex) {
                setSubmenuOpened(submenuIndex)
            }
            // Закрываем подменю
            else {
                setSubmenuOpened(-1)
            }
        }
    }

    className = cl(
        "NavMenuDesktop",
        `NavMenuDesktop_size-${size}`,
        `NavMenuDesktop_variant-${variant}`,
        className,
    )

    return (
        <>
            <nav
                ref={refNavMenuDesktop}
                className={className}
                onClick={hClick}
            >
                <Group
                    size="sm"
                    {...groupProps}
                >
                    {navLinks.map((link, submenuId) => {
                        const isSubmenu = (link.links || []).length > 0

                        // Список подменю
                        if(isSubmenu) {
                            const opened = submenuOpened === submenuId
                            return (
                                <div
                                    key={submenuId}
                                    className={cl("link", {"opened": opened})}
                                    title={link.seoTitle || link.title}
                                    data-submenu-id={submenuId}
                                >
                                    <SVG icon="Menu" title={opened ? "Свернуть" : "Раскрыть"}/>
                                    {link.title}
                                </div>
                            )
                        }

                        // Ссылка
                        else {
                            return (
                                <Link
                                    key={submenuId}
                                    className="link fk-text-skip"
                                    to={link.href}
                                    title={link.seoTitle || link.title}
                                    target={link.target}
                                >
                                    {link.title}
                                </Link>
                            )
                        }
                    })}
                </Group>
            </nav>

            {isBrowser && outside(
                <div
                    className={cl(
                        "NavMenuDesktopSubmenu",
                        {"opened": (submenuOpened !== -1)}
                    )}
                    style={outisdePosition}
                >
                    <nav>
                        {submenuLinks.map((link, index) => (
                            <Link
                                key={index}
                                className={cl(
                                    "submenu-link",
                                    "fk-text-skip",
                                    {"hidden": link.submenuId !== submenuOpened}
                                )}
                                to={link.href}
                                title={link.seoTitle || link.title}
                                target={link.target}
                            >
                                {link.title}
                            </Link>
                        ))}
                    </nav>
                </div>
            )}
        </>
    )

    function updateOutsidePosition() {
        const link = refNavMenuDesktop.current?.querySelector(".link.opened")
        const parent = refNavMenuDesktop.current?.closest(`.${parentElementClassName}`)

        if(link && parent) {
            const minWidth = link.getBoundingClientRect().width
            const left = link.getBoundingClientRect().left + window.scrollX
            const top = parent.getBoundingClientRect().bottom + window.scrollY

            setOutisdePosition({
                top: `${top}px`,
                left: `${left}px`,
                minWidth: `${minWidth}px`,
            })
        }
    }
}

export default NavMenuDesktop