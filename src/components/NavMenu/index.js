import React from 'react'
import { connect } from 'react-redux'

import { MAIN, ARTICLES, ABOUT, CORPS, ACTIONS } from '../../routes/routes'

import { servicesMenu } from '../../store/Posts/select'

import NavMenu from './NavMenu'


const NavMenuContainer = props => (
    <NavMenu
        className={props.className}
        menuTitle={props.menuTitle}

        size={props.size}
        variant={props.variant}
        groupProps={props.groupProps}
        parentElementClassName={props.parentElementClassName}

        navLinks={getMenuList(props.servicesMenu)}
    />
)

const stateToProps = state => ({
    servicesMenu: servicesMenu(state)
})

export default connect(stateToProps, null)(NavMenuContainer)

//
// Функции
//

let lastMenu = []
let lastDepend

function getMenuList(links) {
    if(links !== lastDepend) {
        return lastMenu = [
            {title: "Главная", seoTitle: "", href: MAIN, target: ""},
            ...links,
            // {title: "Ремонт бытовой техники", seoTitle: "", href: "/service", target: "",
            //     links: [
            //         {title: "Ремонт холодильников", seoTitle: "", href: "/service", target: ""},
            //         {title: "Ремонт стиральных машин", seoTitle: "", href: "/service", target: ""},
            //         {title: "Ремонт кофемашин", seoTitle: "", href: "/service", target: ""},
            //     ]
            // },
            // {title: "Ремонт кондиционеров", seoTitle: "", href: "/service", target: "",
            //     links: [
            //         {title: "Ремонт домашних кондиционеров", seoTitle: "", href: "/service", target: ""},
            //         {title: "Ремонт промышленных кондиционеров", seoTitle: "", href: "/service", target: ""},
            //     ]
            // },
            {title: "Обслуживание организаций", seoTitle: "", href: CORPS, target: ""},
            // {title: "Товары", seoTitle: "", href: "/products", target: ""},
            {title: "Блог", seoTitle: "", href: ARTICLES, target: ""},
            {title: "О компании", seoTitle: "", href: ABOUT, target: ""},
            // {title: "Акции", seoTitle: "", href: ACTIONS, target: ""},
        ]
    } else {
        return lastMenu
    }
}