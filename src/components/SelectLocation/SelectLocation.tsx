import React, { useState } from 'react'

import { cl } from '../../utilities/cl'
import { safeCall } from '../../utilities/safeCall'
import { length } from '../../utilities/length'
import { outside } from '../../utilities/outside'
import { useIsBrowser } from '../../utilities/useIsBrowser'

import Button from '../FK/Button'
import SVG from '../FK/SVG'


const SelectLocation: React.FC<{
    className?:string
    currentId:number
    availCities:any // ICity
    changeCity:any
}> = ({
    className,
    currentId = -1,
    availCities = {},
    changeCity,
}) => {

    const isBrowser = useIsBrowser()
    const [opened, setOpened] = useState(false)

    const hSelectCity = (cityId:number) => {
        safeCall(changeCity, {
            cityId
        })
        setOpened(!opened)
    }

    className = cl(
        "SelectLocation",
        {"opened": opened},
        className,
    )

    return (
        <>
            <div className={className}>
                <Button
                    variant={opened ? "link-prim" : "link-dark"}
                    size="lg"
                    title={availCities[currentId]?.title || "Москва"}
                    iconBefore="Location"
                    onClick={() => {
                        if(length(availCities) > 1) {
                            setOpened(!opened)
                        }
                    }}
                />
            </div>

            {isBrowser && outside(
                <section className={cl(
                    "SelectLocationModal",
                    className,
                )}>
                    <h4 className="title">
                        <span>Выберите свой город</span>
                        <SVG
                            variant="dark"
                            size="xs-2"
                            icon="Cancel"
                            title="Закрыть"
                            onClick={() => setOpened(false)}
                        />
                    </h4>
                    {Object.keys(availCities).map(cityId => {
                        const city = availCities[cityId] || {}
                        return (
                            <h5
                                key={city.id}
                                className={cl(
                                    "city",
                                    {"active": city.id === currentId}
                                )}
                                onClick={() => hSelectCity(city.id)}
                            >
                                {city.id === currentId && (
                                    <SVG
                                        variant="prim"
                                        size="xs"
                                        icon="Check"
                                        title="Выбран"
                                    />
                                )}
                                <span>
                                    {city.title}
                                </span>
                            </h5>
                        )
                    })}
                </section>
            )}
        </>
    )
}

export default SelectLocation