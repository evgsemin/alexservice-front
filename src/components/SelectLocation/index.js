import React from 'react'
import { connect } from 'react-redux'

import { changeCity } from '../../store/DetermineCurrentCity/actions'
import { currentId, cities } from '../../store/DetermineCurrentCity/select'

import SelectLocation from './SelectLocation'


const SelectLocationContainer = props => (
    <SelectLocation
        className={props.className}
        currentId={props.currentId}
        availCities={props.cities}

        changeCity={props.changeCity}
    />
)

const stateToProps = state => ({
    currentId: currentId(state),
    cities: cities(state),
})

const actionsToProps = {
    changeCity
}

export default connect(stateToProps, actionsToProps)(SelectLocationContainer)