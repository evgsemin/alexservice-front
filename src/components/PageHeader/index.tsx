import React from 'react'

import { cl } from '../../utilities/cl'

import Container from '../FK/Container'
import Breadcrumbs from '../Breadcrumbs'


const PageHeader:React.FC<{
    className?:string
    title?:string
    subtitle?:string
    description?:string
}> = ({
    className,
    title,
    subtitle,
    description,
}) => {

    className = cl(
        "PageHeader",
        className,
    )

    return (
        <header className={className}>
            <Breadcrumbs/>
            <div className="PageHeader__header">
                <Container size="md">
                    <h1>{title}</h1>
                    {subtitle && (
                        <p className="subtitle">{subtitle}</p>
                    )}
                    {description && (
                        <p className="description">{description}</p>
                    )}
                </Container>
            </div>
        </header>
    )
}

export default PageHeader