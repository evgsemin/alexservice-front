import React, { useEffect, useState, useRef, useMemo } from 'react'
import { useEventStatus } from '../useEventStatus'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { isObj } from '../../utilities/is'
import { outside } from '../../utilities/outside'
import { position } from '../../utilities/position'
import { print } from '../../utilities/print'
import { debounce } from '../../utilities/debounce'

import { ITopic, IResult } from './interfaces'

import Input from '../FK/Input'
import SVG from '../FK/SVG'


const carryDebounce = debounce(1000)

const Search:React.FC<{
    className?:string
    results?:IResult[]
    loadResults:any
}> = ({
    className,
    results = [],
    loadResults,
}) => {

    const loading = useEventStatus({
        type: "loading",
        name: "Search",
    })

    const refSearch = useRef(document.createElement("div"))
    const refResults = useRef(document.createElement("div"))

    const [phrase, setPhrase] = useState("")
    const [opened, setOpened] = useState(false)
    const [active, setActive] = useState(false)
    const [isFocus, setIsFocus] = useState(false)
    const [resultsPosition, setResultsPosition] = useState<any>({})
    const resultsTopics = useMemo<ITopic[]>(splitResults, [results])

    useEffect(() => {
        window.addEventListener("click", updateIsFocus)
        return () => window.removeEventListener("click", updateIsFocus)
    }, [])

    useEffect(() => {
        carryDebounce(() => loadResults({ phrase }))
    }, [phrase])

    useEffect(() => {
        setOpened((phrase?.length > 0 && isFocus) ? true : false)
    }, [phrase, isFocus])

    useEffect(() => {
        if(opened) {
            const nextResultsPosition = position({
                parent: refSearch?.current,
                child: refResults?.current,
                offsets: {
                    left: 10,
                    right: 10,
                },
                isWidthAuto: false,
                isSqueeze: true,
                isOverlay: false,
            })
            setResultsPosition(nextResultsPosition)
        }
    }, [opened])

    const hInput = ({ value }:any, e:any) => {
        setPhrase(value)
    }

    className = cl(
        "Search",
        {"active": active},
        {"opened": opened},
        {"squeezed": resultsPosition.squeezed},
        className,
    )

    return (
        <>
            <div className={className} ref={refSearch}>
                <Input
                    className="Search__input"
                    size="lg"
                    placeholder="Искать по услугам, товарам, блогу"
                    iconBefore={<SVG icon="Search"/>}
                    onChange={hInput}
                    attrs={{
                        onFocus: () => setActive(true),
                        onBlur: () => setActive(false),
                    }}
                />
                <div className="Search__area"></div>
            </div>

            {opened && outside(
                <div
                    ref={refResults}
                    className={cl(
                        "SearchResults",
                        {"squeezed": resultsPosition.squeezed}
                    )}
                    style={resultsPosition}
                >
                    <div className="wrap">
                        {print({
                            items: resultsTopics,
                            loading,
                            content: resultsTopics.map((topic, index) => (
                                <div className="SearchResults__topic" key={index}>
                                    <h4 className={topic.className}>{topic.title}</h4>
                                    {topic.links.map((link, index) => (
                                        <a
                                            key={index}
                                            className={cl(
                                                link.className,
                                                "fk-text-skip",
                                            )}
                                            href={link.href}
                                        >
                                            {link.title}
                                        </a>
                                    ))}
                                </div>
                            )),
                            empty: <div className="SearchResults__empty">По данному запросу ничего не найдено.</div>,
                            loadingProps: {
                                align: "center",
                            }
                        })}
                    </div>
                </div>
            )}
        </>
    )

    function splitResults() {
        let topics:any = [],
            cache:any = {},
            order:string[] = ["Услуги", "Товары", "Статьи"],
            added:string[] = []

        results.map(result => {
            if(!cache[result.topic]) {
                cache[result.topic] = {
                    title: result.topic,
                    links: []
                }
            }

            cache[result.topic].links.push({
                title: result.title,
                href: result.href,
                className: result.className,
            })
        })

        order.map(t => {
            if(isObj(cache[t])) {
                topics.push({
                    ...cache[t],
                })
                added.push(t)
            }
        })

        for(let t in cache) {
            if(added.indexOf(t) === -1) {
                topics.push({
                    ...cache[t],
                })
            }
        }

        return topics
    }

    function updateIsFocus(e:any) {
        if(e.target === refSearch.current || e.target === refResults.current || refSearch.current?.contains(e.target) || refResults.current?.contains(e.target)) {
            setIsFocus(true)
        } else {
            setIsFocus(false)
        }
    }
}

export default Search