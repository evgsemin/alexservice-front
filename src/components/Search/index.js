import React from 'react'
import { connect } from 'react-redux'

import { loadResults } from '../../store/Search/actions'
import { results } from '../../store/Search/select'

import Search from './Search'


const SearchContainer = props => (
    <Search
        className={props.className}
        results={props.results}
        loadResults={props.loadResults}
    />
)

const stateToProps = state => ({
    results: results(state),
})

const actionsToProps = {
    loadResults,
}

export default connect(stateToProps, actionsToProps)(SearchContainer)