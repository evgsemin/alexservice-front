export interface ITopicLink {
    className?:string
    title:string
    href:string
}

export interface ITopic {
    className?:string
    title:string
    links:ITopicLink[]
}

export interface IResult {
    className?:string
    title:string
    href:string
    topic:string
}