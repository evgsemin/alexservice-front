import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'


export const useEventStatus = ({
    type, // тип события (например, loading)
    name, // идентификатор (например, Search)
}) => {

    const statuses = useSelector(state => state.EventStatus[type] || [])
    const [status, setStatus] = useState()

    useEffect(() => {
        setStatus(statuses.indexOf(name) !== -1)
    }, [statuses])
    
    return status
}