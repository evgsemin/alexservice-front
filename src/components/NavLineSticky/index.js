import React from 'react'

import NavLineSticky from './NavLineSticky'


const NavLineStickyContainer = props => (
    <NavLineSticky
        className={props.className}
        hiddenPointElementClassName={props.hiddenPointElementClassName}
        hiddenPointOffset={props.hiddenPointOffset}
    />
)

export default NavLineStickyContainer