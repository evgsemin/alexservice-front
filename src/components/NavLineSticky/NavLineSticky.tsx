import React, { useState, useEffect } from 'react'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { isBrowser } from '../../utilities/is'
import { useIs } from '../../utilities/media'
import { useIsBrowser } from '../../utilities/useIsBrowser'
import { outside } from '../../utilities/outside'

import Container from '../FK/Container'
import Group from '../FK/Group'
import Button from '../FK/Button'
import SVG from '../FK/SVG'
import NavMenu from '../NavMenu'
import CallBox from '../CallBox'


const NavLineSticky:React.FC<{
    className?:string
    hiddenPointElementClassName?:string
    hiddenPointOffset?:number
}> = ({
    className,
    hiddenPointElementClassName,
    hiddenPointOffset = 0,
}) => {

    const isBrow = useIsBrowser()

    const [hidden, setHidden] = useState(true)
    const isTablet = useIs("tablet")

    useEffect(() => {
        if(isBrowser()) {
            window.addEventListener("scroll", updateHidden)
            return () => window.removeEventListener("scroll", updateHidden)
        }
    }, [])

    className = cl(
        "NavLineSticky",
        {"hidden": hidden},
        className,
    )

    return (
        <>
            {isBrow && outside(
                <section className={className}>
                    {!hidden && (
                        <Container size="xl">
                            <Group
                                size="md"
                                alignY="center"
                                alignX="end"
                                nowrap={true}
                            >
                                <NavMenu
                                    {...{
                                        menuTitle: "Главное меню",
                                        groupProps: {
                                            size: "sm-1"
                                        },
                                        parentElementClassName: "NavLineSticky",
                                    }}
                                />
                                <Group
                                    className="NavLineSticky__right"
                                    size={isTablet ? "xs" : "md"}
                                    alignY="center"
                                    alignX="end"
                                    nowrap={!isTablet}
                                >
                                    <CallBox/>
                                    <Button
                                        title="Заказать ремонт"
                                        iconBefore="Repair"
                                        variant="prim"
                                        size="md"
                                        modalType="OrderRepair"
                                    />
                                </Group>
                                {/* <SVG
                                    className="NavLineSticky__phone"
                                    icon="PhoneFilled"
                                    variant="prim"
                                    size="xs-2"
                                /> */}

                            </Group>
                        </Container>
                    )}
                </section>
            )}
        </>
    )

    function updateHidden() {
        let hiddenPoint = 500
        
        if(hiddenPointElementClassName) {
            const el = document.querySelector(`.${hiddenPointElementClassName}`)
            if(el) {
                hiddenPoint = el.getBoundingClientRect().bottom
            }
        }

        hiddenPoint += hiddenPointOffset

        setHidden(window.scrollY < hiddenPoint)
    }
}

export default NavLineSticky