import React, { useMemo } from 'react'
import { Link } from 'react-router-dom'

import { cl } from '../../../utilities/cl'
import { cutStr } from '../../../utilities/cutStr'
import { clearHTMLTags } from '../../../utilities/clearHTMLTags'
import { IArticleCard } from './interfaces'

import LazyImage from '../../FK/LazyImage'


const CardArticle:React.FC<IArticleCard> = ({
    className,
    id,
    title,
    image,
    description,
    href,
}) => {

    const descriptionText = useMemo(() => clearHTMLTags(description), [description])

    className = cl(
        "CardArticle",
        className,
    )

    return (
        <section className={className} data-card-article-id={id}>
            <Link className="fk-text-skip" to={href} title={title}>
    
                <header className="CardArticle__header">
                    <LazyImage type="div" format="sm" image={image}/>
                    <h4>{title}</h4>
                </header>
                
                <div className="CardArticle__body">
                    {cutStr(descriptionText, 175, "…")}
                </div>

            </Link>
        </section>
    )
}

export default CardArticle