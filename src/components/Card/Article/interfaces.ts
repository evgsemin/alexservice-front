export interface IArticleCard {
    className?:string
    id:string
    title:string
    image:string
    description:string
    href:string
}