import React, { useRef } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { cutStr } from '../../../utilities/cutStr'
import { IProductShort } from '../../../interfaces'

import CardContent from '../Content'
import LazyImage from '../../FK/LazyImage'
import Button from '../../FK/Button'
import Group from '../../FK/Group'

import IconMarker from '../../IconMarker'


interface ICardProduct extends IProductShort {
    className?:string
}

const CardProduct:React.FC<ICardProduct> = ({
    className,
    id,
    image,
    title,
    short,
    recommend,
    price,
    href,
}) => {

    const refButtons = useRef(document.createElement("div"))

    const hClick = (e:any) => {
        if(refButtons.current.contains(e.target)) {
            e.preventDefault()
        }
    }

    className = cl(
        "CardProduct",
        className,
    )

    return (
        <section className={className}>
            <CardContent
                href={href}
                onClick={hClick}
                title={title}
                variant="light"
            >
                <div className="CardProduct__image">
                    <LazyImage
                        type="div"
                        format="md"
                        image={image}
                        alt={title}
                    />
                    <Group
                        className="markers"
                        alignY="center"
                        size="xs"
                    >
                        {recommend === true && <IconMarker marker="recommend" size="lg"/>}
                    </Group>
                </div>
                
                <header className="CardProduct__title">
                    <h4 className="fk-text-skip">{title}</h4>
                    {short && (
                        <p className="fk-text-skip">
                            {cutStr(short, 100, "…")}
                        </p>
                    )}
                </header>
                
                <footer className="CardProduct__order" ref={refButtons}>
                    <Group
                        alignY="center"
                        alignX="between"
                        size="xs"
                    >
                        <div className="price">
                            {price} ₽
                        </div>
                        <Button
                            size="lg"
                            title="Купить"
                            variant="prim"
                        />
                    </Group>
                </footer>
            </CardContent>
        </section>
    )
}

export default CardProduct