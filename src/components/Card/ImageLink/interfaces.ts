export interface ICardImageLink {
    className?:string
    title?:string
    alt?:string
    href:string
    image:string
    onClick?:any
}