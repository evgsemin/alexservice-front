import React from 'react'
import { Link } from 'react-router-dom'

import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'
import { ICardImageLink } from './interfaces'

import LazyImage from '../../FK/LazyImage'


const CardImageLink = ({
    className,
    title,
    alt,
    href,
    image,
    onClick,
}:ICardImageLink) => {

    const hClick = (e:any) => {
        safeCall(onClick, {
            title,
            href,
        }, e)
    }

    className = cl(
        "CardImageLink",
        className,
    )

    return (
        <section className={className}>
            <LazyImage
                className="CardImageLink__image"
                type="div"
                format="sm-1"
                maskVariant="dark"
                maskLevel="sm"
                image={image}
                alt={alt || title}
            />
            <Link
                className="CardImageLink__link fk-text-skip"
                title={alt || title}
                to={href}
                onClick={hClick}
            >
                <div className="wrap">
                    <h4>{title}</h4>
                </div>
            </Link>
        </section>
    )
}

export default CardImageLink