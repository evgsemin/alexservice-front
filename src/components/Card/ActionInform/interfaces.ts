export interface ICardActionInform {
    id:number
    className?:string
    title?:string
    subtitle?:string
    alt?:string
    href:string
    image?:string
    onClick?:any
}