import React from 'react'

import { cl } from '../../../utilities/cl'
import { isFunc } from '../../../utilities/is'
import { ICardActionInform } from './interfaces'

import CardContent from '../Content'

import LazyImage from '../../FK/LazyImage'


const CardActionInform = ({
    className,
    title,
    subtitle,
    alt,
    href,
    image,
    onClick,
}:ICardActionInform) => {

    className = cl(
        "CardActionInform",
        className,
    )

    return (
        <CardContent
            className={className}
            href={href}
            onClick={onClick}
            title={alt || title}
        >
            <LazyImage
                className="CardActionInform__image"
                type="div"
                maskVariant="prim-dark"
                maskLevel="mg"
                image={image}
            />

            <div className="CardActionInform__content fk-text-inverse">
                <h3 className="title">{title}</h3>
                <p className="subtitle">{subtitle}</p>
            </div>
        </CardContent>
    )
}

export default CardActionInform