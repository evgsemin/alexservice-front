export interface ICardLink {
    className?:string,
    title?:string,
    description?:string,
    price?:string,
    buttonTitle?:string,
    href:string
    alt?:string
    onClick?:any
}