import React, { useRef } from 'react'

import { document } from '../../../utilities/uni'
import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'
import { ICardLink } from './interfaces'

import Button from '../../FK/Button'

import CardContent from '../Content'


const CardLink:React.FC<ICardLink> = ({
    className,
    title,
    description,
    price,
    buttonTitle = "Подробнее",
    href,
    alt,
    onClick,
}) => {

    const refButtonMore = useRef(document.createElement("div"))

    const hClick = (e:any) => {
        safeCall(onClick, {
            refButton: refButtonMore,
        }, e)
    }

    className = cl(
        "CardLink",
        className,
    )

    return (
        <section className={className}>
            <CardContent
                // variant="forth"
                size="sm"
                title={alt || title}
                href={href}
                onClick={hClick}
            >
                <div className="CardLink__content">
                    <div className="CardLink__left">
                        {title && (
                            <header>
                                <h4>{title}</h4>
                            </header>
                        )}
                        {description && <p>{description}</p>}
                    </div>

                    <div className="CardLink__right">
                        {price && (
                            <div className="price">
                                {price} ₽
                            </div>
                        )}
                        <Button
                            className="more"
                            title={buttonTitle}
                            variant="link-light"
                            size="xl"
                            iconAfter="ArrowRight"
                            setRef={refButtonMore}
                        />
                    </div>
                </div>
            </CardContent>
        </section>
    )
}

export default CardLink