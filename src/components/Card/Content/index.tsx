import React from 'react'
import { Link } from 'react-router-dom'

import { cl } from '../../../utilities/cl'
import { safeCall } from '../../../utilities/safeCall'


const CardContent:React.FC<{
    className?:string
    size?:string
    variant?:string
    shadow?:string
    href?:string
    title?:string // Используется для ссылки
    onClick?:any
    children:any
}> = ({
    className,
    size = "sm-1",
    variant = "light-lg",
    shadow,
    href,
    title,
    onClick,
    children,
}) => {

    const hClick = (e:any) => {
        safeCall(onClick, e)
    }

    className = cl(
        "CardContent",
        `CardContent_size-${size}`,
        `CardContent_variant-${variant}`,
        {[`CardContent_shadow-${shadow}`]: typeof shadow === "string"},
        "fk-text-skip",
        className,
    )

    return (
        <>
            {href && (
                <Link className={className} to={href} title={title} onClick={hClick}>
                    {children}
                </Link>
            )}
            {!href && (
                <section className={className} title={title} onClick={hClick}>
                    {children}
                </section>
            )}
        </>
    )
}

export default CardContent