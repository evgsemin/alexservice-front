import React from 'react'

import { cl } from '../../../utilities/cl'
import { useIs } from '../../../utilities/media'
import { ICardArticleXL } from './interfaces'

import Group from '../../FK/Group'
import LazyImage from '../../FK/LazyImage'
import Button from '../../FK/Button'
import Div from '../../Div'


const CardArticleXL = ({
    className,
    image,
    title,
    subtitle,
    content,
    href,
    useButton = false,
    buttonTitle,
    buttonProps = {},
}:ICardArticleXL) => {

    const isDesktopAndUp = useIs("desktopAndUp")

    className = cl(
        "CardArticleXL",
        className,
    )

    return (
        <section className={className}>
            {image && (
                <LazyImage
                    className="CardArticleXL__image"
                    type="div"
                    format="sm"
                    image={image}
                />
            )}

            <header className="CardArticleXL__header">
                <Group
                    size="sm"
                    nowrap={isDesktopAndUp}
                >
                    <div className="CardArticleXL__title">
                        <h3 className="title">{title}</h3>
                        {subtitle && <p className="subtitle">{subtitle}</p>}
                    </div>
                    {useButton && (
                        <Button
                            className="CardArticleXL__button"
                            title={buttonTitle}
                            href={href || undefined}
                            variant="prim-linear"
                            size="md"
                            {...buttonProps}
                        />
                    )}
                </Group>
            </header>

            <Div className="CardArticleXL__content">
                {content}
            </Div>
        </section>
    )
}

export default CardArticleXL