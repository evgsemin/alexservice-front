export interface ICardArticleXL {
    id?:number
    className?:string
    image?:string
    title:string
    subtitle?:string
    content?:string
    href?:string
    useButton?:boolean
    buttonTitle?:string
    buttonProps?:any
}