import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'

import { printSeo } from '../../utilities/printSeo'

import { currentCity } from '../../store/DetermineCurrentCity/select'


export const usePrintSeo = ({
    seo = {},
    postTitle = "",
}) => {

    const city = useSelector(state => currentCity(state))
    
    useEffect(() => {
        printSeo({
            postTitle,
            seoData: seo,
            cityData: city?.seo,
        })
    }, [
        city,
        postTitle,
        seo, seo.title,
        seo.description,
        seo.keywords,
        seo.cover
    ])
}