import { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { getNumbers } from '../../utilities/get'

export const useOpening = ({ from, to }:{from:string, to:string}) => {
    const [opened, setOpened] = useState(true)
    
    useEffect(() => {
        updateOpening()
        const timeInterval = setInterval(updateOpening, 60000)
        return () => clearInterval(timeInterval)
    }, [from, to])

    function updateOpening() {
        const h = new Date().getHours()
        const m = new Date().getMinutes()
        const currentTime = Number(`${h}${m < 10 ? "0"+m : m}`)
        const openedFrom = Number(from.replace(":",""))
        const openedTo = Number(to.replace(":",""))

        if(!isNaN(currentTime) && !isNaN(openedFrom) && !isNaN(openedTo)) {
            setOpened(currentTime > openedFrom && currentTime < openedTo)
        } else {
            setOpened(true)
        }
    }

    return opened
}