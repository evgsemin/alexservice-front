import React from 'react'

import { cl } from '../../utilities/cl'
import { useOpening } from './useOpening'

import Button from '../FK/Button'
import Loading from '../FK/Loading'


const CallBox:React.FC<{
    className?:string
    currentId:number
    mainPhone:string
    openingHours: {
        from:string,
        to:string,
    }
}> = ({
    className,
    currentId,
    mainPhone,
    openingHours,
}) => {

    const opened = useOpening(openingHours)

    className = cl(
        "CallBox",
        {"opened": opened},
        className,
    )

    if(currentId === -1) return <Loading variant="light-lg"/>

    return (
        <div className={className}>
            <Button
                className="CallBox__phone"
                variant="link-dark"
                size="md"
                href={`tel:${mainPhone}`}
                title={mainPhone}
                iconBefore="Phone"
            />

            <div className="CallBox__opening" title={opened ? "Сейчас открыты" : "Сейчас закрыты"}>
                Ежедневно с {openingHours.from} до {openingHours.to}
            </div>
        </div>
    )
}

export default CallBox