import React from 'react'
import { connect } from 'react-redux'

import { currentId, currentCity } from '../../store/DetermineCurrentCity/select'

import CallBox from './CallBox'


const CallBoxContainer = props => (
    <CallBox
        className={props.className}
        currentId={props.currentId}
        mainPhone={props.mainPhone}
        openingHours={props.openingHours}
    />
)

const stateToProps = state => ({
    currentId: currentId(state),
    mainPhone: currentCity(state).mainPhone,
    openingHours: currentCity(state).openingHours,
})


export default connect(stateToProps)(CallBoxContainer)