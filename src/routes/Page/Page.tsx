import React from 'react'
import { useEventStatus } from '../../components/useEventStatus'
import { usePrintSeo } from '../../components/usePrintSeo'

import { cl } from '../../utilities/cl'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'
import Loading from '../../components/FK/Loading'

import SectionSteps from '../../sections/Steps'
import SectionContacts from '../../sections/Contacts'

import PageHeader from '../../components/PageHeader'
import Div from '../../components/Div'


const RoutePage: React.FC<{
    className?: string
    page:any

    includeSteps:boolean
    includeContacts:boolean
}> = ({
    className,
    page,
    includeSteps,
    includeContacts,
}) => {

    const loading = useEventStatus({
        type: "loading",
        name: "PostsPages",
    })

    usePrintSeo({
        postTitle: page.title,
        seo: page.seo,
    })

    className = cl(
        "RoutePage",
        {"RoutePage_without-pb": includeSteps || includeContacts},
        className,
    )

    return (
        <section className={className}>
            <Header/>

            <PageHeader
                className="RoutePage__header"
                title={page.title}
            />

            <div className="RoutePage__body">
                <Container size="md">
                    {loading
                        ? <Loading/>
                        : <Div className="RoutePage__content">
                            {page.content}
                        </Div> 
                    }
                </Container>

                {includeSteps && <SectionSteps titleTag="h4"/>}

                {includeContacts && <SectionContacts/>}
            </div>

            <Footer/>
        </section>
    )
}

export default RoutePage