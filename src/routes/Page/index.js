import React from 'react'
import { connect } from 'react-redux'

import { page } from '../../store/Posts/select'

import RoutePage from './Page'


const RoutePageContainer = props => (
    <RoutePage
        className={props.className}
        page={props.page}
        includeSteps={props.includeSteps}
        includeContacts={props.includeContacts}
    />
)

const stateToProps = (state, props) => ({
    page: page(state, props.match.params.postId),
    includeSteps: props.match.params.postId === "o-kompanii",
    includeContacts: props.match.params.postId === "o-kompanii",
})

export default connect(stateToProps, null)(RoutePageContainer)