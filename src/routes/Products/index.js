import React from 'react'

import RouteProducts from './Products'

const imgProduct = '/dist/images/product.jpg'


const RouteProductsContainer = props => (
    <RouteProducts
        className={props.className}
        pageTitle="Товары | AlexService — профессиональный подход к ремонту бытовой техники"

        products={[
            {
                id: 1,
                recommend: true,
                image: imgProduct,
                title: "Mobilgrease XHP",
                short: "В арсенале компании имеются современные сканеры на различные модели автомобилей. В арсенале компании имеются современные сканеры на различные модели автомобилей. В арсенале компании имеются современные сканеры на различные модели автомобилей.",
                price: "125",
                href: "/product",
            },
            {
                id: 2,
                image: imgProduct,
                title: "Лампа ксеноновая",
                short: "В наличии все расходные материалы для ремонта и обслуживания.",
                price: "244",
                href: "/product",
            },
            {
                id: 3,
                image: imgProduct,
                title: "OSRAM H7 PX26d",
                short: "Новых и восстановленных АКПП, гидротрансформаторов, редукторов и раздаточных коробок.",
                price: "997",
                href: "/product",
            }
        ]}
    />
)


export default RouteProductsContainer