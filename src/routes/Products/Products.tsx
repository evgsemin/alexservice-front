import React, { useEffect } from 'react'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { print } from '../../utilities/print'
import { IProductShort } from '../../interfaces'

const imageMainBg = '/dist/images/main-slider-image.jpg'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'
import Grid from '../../components/FK/Grid'

import PageHeader from '../../components/PageHeader'
import CardProduct from '../../components/Card/Product'
import OrderFormSection from '../../components/OrderForm/Section'

import SectionHowToRepair from '../../sections/HowToRepair'


const RouteProducts: React.FC<{
    className?: string
    pageTitle:string
    products:IProductShort[]
    loading:boolean
}> = ({
    className,
    pageTitle,
    products = [],
    loading,
}) => {

    useEffect(() => {
        document.title = pageTitle
    }, [])

    className = cl(
        "RouteProducts",
        className
    )

    return (
        <section className="RouteProducts">
            <Header/>
            
            <PageHeader
                className="RouteProducts__header"
                title="Товары"
                subtitle="Мы продаём лучшие товары и ручаемся за их качество"
            />

            <div className="RouteProducts__body">
                <Container>
                    <>
                        {print({
                            content: <>
                                <Grid size="md-1" cols={4}>
                                    {products.map(product => (
                                        <CardProduct
                                            key={product.id}
                                            className={product.className}
                                            id={product.id}
                                            image={product.image}
                                            title={product.title}
                                            short={product.short}
                                            recommend={product.recommend}
                                            price={product.price}
                                            href={product.href}
                                        />
                                    ))}
                                </Grid>
                            </>,
                            empty: <h4>Товаров нет.</h4>,
                            items: products,
                            loading,
                        })}
                    </>
                </Container>
            </div>

            <Footer/>
        </section>
    )
}

export default RouteProducts