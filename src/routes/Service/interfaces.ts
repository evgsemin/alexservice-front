import { ISeo } from '../../components/usePrintSeo/interfaces'


export interface IService {
    title:string
    cityTitle:string
    id:number
    href:string
    parentId:number
    isChild:boolean
    contentBeforeTables?:string
    contentAfterTables?:string
    assocBrand:number
    tablePrices:number
    tableWorks:number
    faultsTitle?:string
    faults?:Number[]
    faqs?:Number[]
    seo:ISeo
    sectionStepsVisible:boolean
    sectionContactsVisible:boolean
}