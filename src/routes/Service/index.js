import React from 'react'
import { connect } from 'react-redux'

import { service, serviceBrands } from '../../store/Posts/select'

import RouteService from './Service'


const RouteServiceContainer = props => (
    <RouteService
        className={props.className}
        service={props.service}
        serviceBrands={props.serviceBrands}
    />
)

const stateToProps = (state, props) => ({
    service: service(state, props.match.params.postId),
    serviceBrands: serviceBrands(state, props.match.params.postId)
})

export default connect(stateToProps, null)(RouteServiceContainer)