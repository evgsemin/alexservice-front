import React, { useRef, useState } from 'react'
import { usePrintSeo } from '../../components/usePrintSeo'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { useIs } from '../../utilities/media'
import { renderIf } from '../../utilities/renderIf'
import { IBrandItem } from '../../components/BrandToggler/interfaces'
import { IService } from './interfaces'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'
import Breadcrumbs from '../../components/Breadcrumbs'
import Grid from '../../components/FK/Grid'
import Sticky from '../../components/FK/Sticky'
import Loading from '../../components/FK/Loading'

import PricesByBrand from '../../components/PricesByBrand'
import TopFaults from '../../components/TopFaults'
import FAQ from '../../components/FAQ'
import OrderFormAside from '../../components/OrderForm/Aside'
import Contents from '../../components/Contents'
import Div from '../../components/Div'
import Bullets from '../../components/Bullets'
import CTA from '../../components/CTA'
import SectionSteps from '../../sections/Steps'
import SectionContacts from '../../sections/Contacts'


const RouteService: React.FC<{
    className?: string
    service:IService
    serviceBrands:IBrandItem[]
}> = ({
    className,
    service = {},
    serviceBrands = [],
}) => {

    const refContent = useRef(document.createElement("div"))
    const isDesktopAndUp = useIs("desktopAndUp")
    const [renderPricesTitle, setRenderPricesTitle] = useState(false)

    usePrintSeo({
        postTitle: service.cityTitle,
        seo: service.seo,
    })

    className = cl(
        "RouteService",
        className
    )

    return (
        <section className={className}>
            <Header/>
            <Breadcrumbs/>

            <div className="RouteService__body">
                <Container>
                    <Grid
                        size={isDesktopAndUp ? "xl" : "md"}
                        cols={{
                            desktopAndUp: 3,
                            tabletAndDown: 1,
                        }}
                    >
                        <section
                            ref={refContent}
                            className={cl(
                                "RouteService__left",
                                {"fk-grid__col-2": isDesktopAndUp}
                            )}
                        >
                            {service.title
                                ? <>
                                    <h1>{service.cityTitle}</h1>
                                    <Div>
                                        {service.contentBeforeTables}
                                    </Div>

                                    {renderIf({
                                        condition: service.faults,
                                        children: (
                                            <>
                                                <h2>{service.faultsTitle || "Топ-4 поломки"}</h2>
                                                <TopFaults
                                                    className="fk-text-offsets-p"
                                                    gridProps={{
                                                        cols: {
                                                            tabletAndUp: 2,
                                                            mobileAndDown: 1,
                                                        }
                                                    }}
                                                    faultsIds={service.faults}
                                                />
                                            </>
                                        )
                                    })}

                                    <Div>
                                        {service.contentAfterTables}
                                    </Div>

                                    {renderPricesTitle && (
                                        <h2>Цены</h2>
                                    )}

                                    <PricesByBrand
                                        className="fk-text-offsets-p"
                                        active={service.assocBrand}
                                        brands={serviceBrands}
                                        pricesTableId={service.tablePrices}
                                        onRender={setRenderPricesTitle}
                                    />

                                    <Bullets
                                        type="section"
                                        cols={{
                                            laptopAndUp: 2,
                                            desktop: 1,
                                            tablet: 2,
                                            mobileAndDown: 1,
                                        }}
                                        containerOn={false}
                                    />

                                    <CTA
                                        className="fk-text-offsets-p"
                                        type="double"
                                    />
                                </>
                                : <Loading/>
                            }
                        </section>
                        
                        {renderIf({
                            condition: service.faqs,
                            children: (
                                <section className="RouteService__right">
                                    <Sticky
                                        offset={32}
                                        offsetElementSelector=".NavLineSticky"
                                    >
                                        <section className="RouteService__FAQ">
                                            <h3>Ответы на частые вопросы</h3>
                                            <FAQ
                                                faqsIds={service.faqs}
                                                startOpened={[]}
                                            />
                                        </section>
                                            
                                        {/* <OrderFormAside/>
                                        <Contents
                                            offset={32}
                                            offsetElementSelector=".NavLineSticky"
                                            refContent={refContent}
                                            dependence={service}
                                            titlesLevels={["h2", "h3"]}
                                        /> */}
                                    </Sticky>
                                </section>
                            )
                        })}
                    </Grid>
                </Container>
            </div>

            {service.sectionStepsVisible && (
                <SectionSteps titleTag="h4"/>
            )}

            {service.sectionContactsVisible && (
                <SectionContacts/>
            )}

            <Footer/>
        </section>
    )
}

export default RouteService