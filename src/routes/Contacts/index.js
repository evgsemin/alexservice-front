import React from 'react'

import Contacts from './Contacts'


const ContactsContainer = props => (
    <Contacts
        className={props.className}
    />
)

export default ContactsContainer