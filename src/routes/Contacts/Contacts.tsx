import React from 'react'
import { usePrintSeo } from '../../components/usePrintSeo'

import { cl } from '../../utilities/cl'

import Breadcrumbs from '../../components/Breadcrumbs'
import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import SectionContacts from '../../sections/Contacts'


const Contacts:React.FC<{
    className?:string
}> = ({
    className,
}) => {

    usePrintSeo({
        postTitle: "Контакты",
    })

    className = cl(
        "RouteContacts",
        className,
    )

    return (
        <section className={className}>
            <Header/>

            <Breadcrumbs/>

            <SectionContacts/>

            <Footer/>
        </section>
    )
}

export default Contacts