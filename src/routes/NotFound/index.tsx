import React from 'react'

import { cl } from '../../utilities/cl'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'


const RouteNotFound: React.FC<{
    className?: string
}> = ({
    className
}) => {

    className = cl(
        "RouteNotFound",
        className
    )

    return (
        <section className="RouteNotFound">
            <Header
                data={{
                    companyName: "AlexService — профессиональный подход к ремонту бытовой техники",
                    pageTitle: "Главная | AlexService — профессиональный подход к ремонту бытовой техники",
                }}
            />
            <h2>Такой страницы нет.</h2>
            <Footer/>
        </section>
    )
}

export default RouteNotFound