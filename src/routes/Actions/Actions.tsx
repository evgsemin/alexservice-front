import React, { useEffect } from 'react'
import { useIsCitySelected } from '../../components/DetermineCurrentCity/useIsCitySelected'
import { useEventStatus } from '../../components/useEventStatus'
import { usePrintSeo } from '../../components/usePrintSeo'

import { cl } from '../../utilities/cl'
import { print } from '../../utilities/print'

import { IRouteAction } from './interfaces'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'
import Grid from '../../components/FK/Grid'
import CardArticleXL from '../../components/Card/ArticleXL'

import PageHeader from '../../components/PageHeader'


const RouteActions: React.FC<{
    className?: string
    actions:IRouteAction[]
    loadActions:any
}> = ({
    className,
    actions = [],
    loadActions,
}) => {

    const postTitle = "Акции"
    
    const isCitySelected = useIsCitySelected()
    const loading = useEventStatus({
        type: "loading",
        name: "Actions",
    })

    useEffect(() => {
        if(isCitySelected) {
            loadActions()
        }
    }, [isCitySelected])

    usePrintSeo({ postTitle })

    className = cl(
        "RouteActions",
        className
    )

    return (
        <section className={className}>
            <Header/>

            <PageHeader
                className="RouteActions__header"
                title={postTitle}
            />

            <div className="RouteActions__body">
                <Container>
                    {print({
                        content: <>
                            <Grid
                                cols={{
                                    desktopAndUp: 3,
                                    tabletAndDown: 1,
                                }}
                            >
                                <div className="fk-grid__col-2">
                                    {actions.map(action => (
                                        <CardArticleXL
                                            key={action.id}
                                            className={action.className}
                                            image={action.imageLarge}
                                            title={action.title}
                                            subtitle={action.subtitle}
                                            content={action.content}
                                            href={action.href}
                                            useButton={action.useButton}
                                            buttonTitle="Воспользоваться акцией"
                                            buttonProps={{
                                                iconAfter: "ArrowRight",
                                                modalType: "OrderRepair",
                                            }}
                                        />
                                    ))}
                                </div>
                            </Grid>
                        </>,
                        empty: <h4>В данный момент акций нет.</h4>,
                        items: actions,
                        loading,
                        loadingProps: {
                            size: "sm-1",
                            align: "center",
                        },
                    })}
                </Container>
            </div>

            <Footer/>
        </section>
    )
}

export default RouteActions