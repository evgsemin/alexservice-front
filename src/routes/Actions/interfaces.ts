import { ICardActionInform } from '../../components/Card/ActionInform/interfaces'

export interface IRouteAction extends ICardActionInform {
    imageLarge:string
    content?:string
    useButton?:boolean
}