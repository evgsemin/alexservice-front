import React from 'react'
import { connect } from 'react-redux'

import { loadActions } from '../../store/Actions/actions'
import { actionsArr } from '../../store/Actions/select'

import RouteActions from './Actions'


const RouteActionsContainer = props => (
    <RouteActions
        className={props.className}
        actions={props.actions}
        loadActions={props.loadActions}
    />
)

const stateToProps = state => ({
    actions: actionsArr(state)
})

const actionsToProps = {
    loadActions
}

export default connect(stateToProps, actionsToProps)(RouteActionsContainer)