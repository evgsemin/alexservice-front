import React, { useEffect, useMemo } from 'react'
import { useEventStatus } from '../../components/useEventStatus'
import { usePrintSeo } from '../../components/usePrintSeo'

import { cl } from '../../utilities/cl'
import { print } from '../../utilities/print'
import { convertObjToArr } from '../../utilities/convert'

import { IArticleCard } from '../../components/Card/Article/interfaces'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'
import Grid from '../../components/FK/Grid'
import CardArticle from '../../components/Card/Article'

import PageHeader from '../../components/PageHeader'


const RouteArticles: React.FC<{
    className?: string
    articles:IArticleCard[]
}> = ({
    className,
    articles = [],
}) => {

    const postTitle = "Блог"

    const loading = useEventStatus({
        type: "loading",
        name: "PostsArticles",
    })

    usePrintSeo({ postTitle })

    className = cl(
        "RouteArticles",
        className
    )

    return (
        <section className={className}>
            <Header/>

            <PageHeader
                className="RouteArticles__header"
                title={postTitle}
            />

            <div className="RouteArticles__body">
                <Container>
                    {print({
                        content: <>
                            <Grid
                                size="md-1"
                                cols={{
                                    desktopAndUp: 3,
                                    tablet: 2,
                                    mobileAndDown: 1,
                                }}
                            >
                                {articles.map(article => (
                                    <CardArticle
                                        key={article.id}
                                        className={article.className}
                                        id={article.id}
                                        title={article.title || ""}
                                        image={article.image || ""}
                                        description={article.description || ""}
                                        href={article.href || ""}
                                    />
                                ))}
                            </Grid>
                        </>,
                        empty: <h4>Записей нет!</h4>,
                        items: articles,
                        loading,
                        loadingProps: {
                            size: "sm-1",
                            align: "center",
                        },
                    })}
                </Container>
            </div>

            <Footer/>
        </section>
    )
}

export default RouteArticles