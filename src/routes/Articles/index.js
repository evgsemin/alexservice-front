import React, {useEffect, useState} from 'react'
import { connect } from 'react-redux'

import { allArticlesArr } from '../../store/Posts/select'

const imageMainBg = '/dist/images/main-slider-image.jpg'

import RouteArticles from './Articles'


const RouteArticlesContainer = props => (
    <RouteArticles
        className={props.className}
        articles={props.articles}
    />
)

const stateToProps = state => ({
    articles: allArticlesArr(state)
})

export default connect(stateToProps, null)(RouteArticlesContainer)