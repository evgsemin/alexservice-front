import React from 'react'
import { usePrintSeo } from '../../components/usePrintSeo'

import { cl } from '../../utilities/cl'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Group from '../../components/FK/Group'
import Section from '../../components/FK/Section'
import SVG from '../../components/FK/SVG'


const Route404:React.FC<{
    className?:string
}> = ({
    className,
}) => {

    const postTitle = "Страница не найдена"

    usePrintSeo({
        postTitle
    })

    className = cl(
        "Route404",
        className
    )

    return (
        <section className="Route404">
            <Header/>

            <section className="Route404__body">
                <Section
                    size="mg"
                    containerSize="md"
                >
                    <Group size="lg">
                        <div className="left">
                            <SVG
                                title={postTitle}
                                size="lg-2"
                                variant="prim"
                                icon="CancelCircle"
                            />
                        </div>
                        <div className="right">
                            <h1>{postTitle}</h1>
                            <p>
                                Адрес страницы введён с ошибкой, или она была удалена. <br/>
                                Вы можете перейти на <a href="/" title="Главная страница">главную страницу</a>, или воспользоваться поиском для нужной информации.
                            </p>
                        </div>
                    </Group>
                </Section>
            </section>

            <Footer/>
        </section>
    )
}

export default Route404