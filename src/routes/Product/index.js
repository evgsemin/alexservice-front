import React from 'react'

const imgProduct = '/dist/images/product.jpg'

import RouteProduct from './Product'


const RouteProductContainer = props => (
    <RouteProduct
        className={props.className}
        pageTitle="Mobilgrease XHP купить онлайн с доставкой по Москве и МО | AlexService — профессиональный подход к ремонту бытовой техники"
        product={{
            id: 1,
            recommend: true,
            image: imgProduct,
            images: [
                {
                    alt: "1",
                    image: "https://cherepah.ru/wp-content/uploads/1/9/a/19af89f11e207ecf70387e8f67304892.jpeg",
                },
                {
                    alt: "2",
                    image: "https://avatars.mds.yandex.net/get-zen_doc/175604/pub_5ad2e6a6f03173b2ad9223b6_5ad2eb4977d0e6219e50dad4/scale_1200",
                },
                {
                    alt: "3",
                    image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",
                },
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
                {image: "https://vplate.ru/images/article/orig/2019/03/kak-opredelit-pol-morskoj-svinki.jpg",},
            ],
            title: "Mobilgrease XHP",
            short: "В арсенале компании имеются современные сканеры на различные модели автомобилей. В арсенале компании имеются современные сканеры на различные модели автомобилей. В арсенале компании имеются современные сканеры на различные модели автомобилей.",
            price: "125",
            href: "/product",
        }}
    />
)


export default RouteProductContainer