import React, { useEffect } from 'react'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { IProduct } from '../../interfaces'

import Grid from '../../components/FK/Grid'
import Group from '../../components/FK/Group'
import Button from '../../components/FK/Button'
import CartQuantity from '../../components/FK/CartQuantity'
import Table from '../../components/FK/Table'
import Sticky from '../../components/FK/Sticky'
import TabsMenu from '../../components/FK/TabsMenu'
import ProductGallery from '../../components/FK/ProductGallery'

import Breadcrumbs from '../../components/Breadcrumbs'
import IconMarker from '../../components/IconMarker'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'


const RouteProduct: React.FC<{
    className?: string
    pageTitle:string
    product:IProduct
}> = ({
    className,
    pageTitle,
    product = {},
}) => {

    useEffect(() => {
        document.title = pageTitle
    }, [])

    className = cl(
        "RouteProduct",
        className
    )

    return (
        <section className="RouteProduct">
            <Header/>
            <Breadcrumbs/>

            <div className="RouteProduct__body">
                <div className="RouteProduct__main">
                    <Container size="xl">
                        <Grid cols={2} size="md">

                            {/* Лево */}

                            <div className="RouteProduct__left">
                                <Sticky>
                                    <ProductGallery
                                        direction="y"
                                        images={product.images}
                                    />
                                </Sticky>
                            </div>

                            {/* Право */}

                            <header className="RouteProduct__right">
                                <h1 className="title">
                                    {product.title}
                                </h1>
                                <div className="markers">
                                    {product.recommend === true && (
                                        <IconMarker
                                            marker="recommend"
                                            size="mr"
                                            type="text"
                                            title="Товар рекомендован"
                                        />
                                    )}
                                </div>
                                <p className="description">
                                    {product.short}
                                </p>
                                <Group size="xs">
                                    <CartQuantity
                                        className="buy-quantity"
                                    />
                                    <Button
                                        className="buy-btn"
                                        variant="prim"
                                        title="Купить"
                                        size="lg"
                                        modalType="OrderProducts"
                                    />
                                </Group>
                                <Table
                                    className="buy-table"
                                    size="sm"
                                    notBorder={true}
                                    table={{
                                        // thead: {
                                        //     tr: [
                                        //         {
                                        //             items: [
                                        //                 {title: "Стоимость:"},
                                        //                 {title: `${product.price} ₽`},
                                        //             ]
                                        //         },
                                        //     ]
                                        // },
                                        thead: {
                                            tr: [
                                                {
                                                    items: [
                                                        {
                                                            title: "Информация",
                                                            attrs: {
                                                                colspan: "2"
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        tbody: {
                                            tr: [
                                                {
                                                    items: [
                                                        {title: "Стоимость:"},
                                                        {
                                                            title: `${product.price} ₽`,
                                                            type: "th",
                                                            attrs: {
                                                                className: "buy-price",
                                                            }
                                                        },
                                                    ]
                                                },
                                                {
                                                    items: [
                                                        {title: "В наличии:"},
                                                        {title: "Да", type: "th"},
                                                    ]
                                                },
                                                {
                                                    items: [
                                                        {title: "Доставка:"},
                                                        {title: "По России", type: "th"},
                                                    ]
                                                },
                                                // {
                                                //     items: [
                                                //         {title: "Рекомендован:"},
                                                //         {title: "Да", type: "th"},
                                                //     ]
                                                // },
                                            ]
                                        }
                                    }}
                                />
                            </header>
                        </Grid>
                    </Container>
                </div>

                <div className="RouteProduct__advanced">
                    <TabsMenu
                        tabs={[
                            {
                                className: "",
                                title: "Описание",
                                text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores laborum modi optio odio, fugiat, inventore doloremque quas adipisci et fugit quasi aliquam totam provident nam quam libero aspernatur? Culpa distinctio repellendus ex et recusandae. Culpa, inventore? Repudiandae consequuntur facilis esse.",
                            },
                            {
                                className: "",
                                title: "Технические характеристики",
                                content: <><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores laborum modi optio odio, fugiat, inventore doloremque quas adipisci et fugit quasi aliquam totam provident nam quam libero aspernatur? Culpa distinctio repellendus ex et recusandae. Culpa, inventore? Repudiandae consequuntur facilis esse.</p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores laborum modi optio odio, fugiat, inventore doloremque quas adipisci et fugit quasi aliquam totam provident nam quam libero aspernatur? Culpa distinctio repellendus ex et recusandae. Culpa, inventore? Repudiandae consequuntur facilis esse.</p></>
                            },
                            {
                                className: "",
                                title: "Доставка и оплата",
                                text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores laborum modi optio odio, fugiat, inventore doloremque quas adipisci et fugit quasi aliquam totam provident nam quam libero aspernatur? Culpa distinctio repellendus ex et recusandae. Culpa, inventore? Repudiandae consequuntur facilis esse.",
                            },
                        ]}
                    />
                </div>
            </div>

            <Footer/>
        </section>
    )
}

export default RouteProduct