import React from 'react'
import { connect } from 'react-redux'

import { currentCity } from '../../store/DetermineCurrentCity/select'

import RouteMain from './Main'


const RouteMainContainer = props => (
    <RouteMain
        className={props.className}
        city={props.currentCity}
    />
)

const stateToProps = state => ({
    currentCity: currentCity(state),
})

export default connect(stateToProps, null)(RouteMainContainer)