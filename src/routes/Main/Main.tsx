import React, { useMemo } from 'react'
import { usePrintSeo } from '../../components/usePrintSeo'

import { cl } from '../../utilities/cl'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'

import Bullets from '../../components/Bullets'
import OrderFormSection from '../../components/OrderForm/Section'

import SectionMainSlider from '../../sections/MainSlider'
import SectionImageMenu from '../../sections/ImageMenu'
import SectionCorps from '../../sections/Corps'
import SectionAbout from '../../sections/About'
import SectionSteps from '../../sections/Steps'
import SectionTopFaults from '../../sections/TopFaults'
import SectionActions from '../../sections/Actions'
import SectionContacts from '../../sections/Contacts'


const RouteMain: React.FC<{
    className?: string
    city:any
}> = ({
    className,
    city = {},
}) => {

    usePrintSeo({ seo: city.seo })

    const bulletsSectionProps = useMemo(() => ({
        containerSize: "md",
    }), [className, city])

    className = cl(
        "RouteMain",
        className
    )

    return (
        <section className="RouteMain">
            <Header/>

            <div className="RouteMain__body">
                <h1 className="fk-d-none">
                    AlexService — Ремонт техники в Москве и МО
                </h1>

                <SectionMainSlider/>

                <SectionImageMenu type="appliances"/>

                <SectionImageMenu type="conditioners"/>

                <SectionCorps/>

                <SectionTopFaults/>

                <SectionAbout/>

                <Bullets
                    type="section"
                    titleAlign="center"
                    titleTag="h3"
                    titleTextSkip={false}
                    sectionProps={bulletsSectionProps}
                />

                <SectionSteps titleTag="h4"/>

                <SectionContacts/>
            </div>

            <Footer/>
        </section>
    )
}

export default RouteMain