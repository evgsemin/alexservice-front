import { ISeo } from '../../components/usePrintSeo/interfaces'
import { ICardLink } from '../../components/Card/Link/interfaces'
import { IRelatedLink } from '../../components/RelatedLinks/interfaces'


export interface IArticle {
    title:string
    image:{
        image:string
        url?:string
        alt?:string
    }
    description?:string,
    relatedServices?:ICardLink[]
    relatedLinks?:IRelatedLink[]
    content?:any
    seo:ISeo
}