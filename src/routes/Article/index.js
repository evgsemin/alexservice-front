import React from 'react'
import { connect } from 'react-redux'

import { article } from '../../store/Posts/select'

import RouteArticle from './Article'


const RouteArticleContainer = props => (
    <RouteArticle
        className={props.className}

        article={{
            title: props.article.title,
            image: {
                image: props.article.image,
                url: props.match.url,
                alt: props.article.title,
            },
            content: props.article.content,
            description: props.article.description,
            relatedServices: props.article.relatedServices,
            relatedLinks: props.article.relatedArticles, // FIXME: ссылки не отображаются, если перейти к записи по прямой ссылке
            seo: props.article.seo,
        }}
    />
)

const stateToProps = (state, props) => ({
    article: article(state, props.match.params.postId),
    // city: 
})

export default connect(stateToProps, null)(RouteArticleContainer)