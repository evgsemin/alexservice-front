import React, { useEffect, useRef } from 'react'
import { usePrintSeo } from '../../components/usePrintSeo'

import { document } from '../../utilities/uni'
import { cl } from '../../utilities/cl'
import { useIs } from '../../utilities/media'
import { renderIf } from '../../utilities/renderIf'
import { IArticle } from './interfaces'

import Header from '../../components/Layout/Header'
import Footer from '../../components/Layout/Footer'
import Container from '../../components/FK/Container'
import Grid from '../../components/FK/Grid'
import Sticky from '../../components/FK/Sticky'
import Group from '../../components/FK/Group'
import LazyImage from '../../components/FK/LazyImage'
import Section from '../../components/FK/Section'
import Button from '../../components/FK/Button'
import Loading from '../../components/FK/Loading'

import Breadcrumbs from '../../components/Breadcrumbs'
import OrderFormAside from '../../components/OrderForm/Aside'
import CardLink from '../../components/Card/Link'
import RelatedLinks from '../../components/RelatedLinks'
import Contents from '../../components/Contents'
import Div from '../../components/Div'


const RouteArticle: React.FC<{
    className?: string
    article:IArticle
}> = ({
    className,
    article = {},
}) => {

    const refContent = useRef(document.createElement("div"))
    const isDesktopAndUp = useIs("desktopAndUp")

    usePrintSeo({
        postTitle: article.title,
        seo: article.seo,
    })

    const componentContents = (
        <Contents
            offset={32}
            offsetElementSelector=".NavLineSticky"
            refContent={refContent}
            dependence={article.content}
        />
    )

    className = cl(
        "RouteArticle",
        className
    )

    return (
        <section className="RouteArticle">
            <Header/>

            <section className="RouteArticle__body">
                <Breadcrumbs/>

                <Container>
                    <Grid
                        size="md"
                        cols={{
                            desktopAndUp: 3,
                            tabletAndDown: 1,
                        }}
                    >
                        
                        {/* Лево */}

                        <article
                            className={cl(
                                "RouteArticle__left",
                                {"fk-grid__col-2": isDesktopAndUp}
                            )}
                            itemType="http://www.schema.org/BlogPosting"
                            itemScope={true}
                        >
                            {article.title
                                ? <>
                                    <h1 className="title" itemProp="headline">
                                        {article.title}
                                    </h1>

                                    {article.description && (
                                        <Div className="description">
                                            {article.description}
                                        </Div>
                                    )}

                                    {!isDesktopAndUp && componentContents}

                                    {
                                        // TODO: ссылка работает не всегда
                                    }

                                    {renderIf({
                                        condition: article.relatedServices,
                                        children: <Group
                                            className="services fk-text-offsets-p"
                                            direction="columns"
                                            stretch={true}
                                            size="xs-2"
                                        >
                                            {article.relatedServices.map((service, index) => (
                                                <CardLink
                                                    key={index}
                                                    title={service.title} 
                                                    alt={service.alt} 
                                                    description={service.description}
                                                    price={service.price}
                                                    href={service.href || ""}
                                                />
                                            ))}
                                        </Group>
                                    })}

                                    <div itemProp="articleBody">
                                        <LazyImage
                                            className="image fk-text-offsets-p"
                                            type="figure"
                                            image={article.image?.image || ""}        
                                            url={article.image?.url}
                                            alt={article.image?.alt}
                                        />
                                    
                                        {article.content && (
                                            <Div className="content" setRef={refContent}>
                                                {article.content}
                                            </Div>
                                        )}
                                    </div>

                                    {renderIf({
                                        condition: article.relatedLinks,
                                        children: <>
                                            <h4>Читайте также:</h4>
                                            <RelatedLinks
                                                className="fk-text-offsets-p"
                                                links={article.relatedLinks || []}
                                            />
                                        </>
                                    })}
                                </>
                                : <Loading/>
                            }
                            
                        </article>

                        {/* Право */}

                        {isDesktopAndUp && (
                            <div className="RouteArticle__right">
                                <OrderFormAside/>

                                <div className="RouteArticle__contents">
                                    <Sticky
                                        offset={32}
                                        offsetElementSelector=".NavLineSticky"
                                    >
                                        {componentContents}
                                    </Sticky>
                                </div>
                            </div>
                        )}
                    </Grid>
                </Container>
            </section>

            <Footer/>
        </section>
    )
}

export default RouteArticle