import React from 'react'

import { isArr, isObj, isLength } from './is'

import Loading from '../components/FK/Loading'


export function print({
    content,
    empty,
    loading,
    items,
    loadingProps = {},
}) {
    
    // Загрузка
    if(loading === true) {
        return (
            <Loading
                variant="dark-lg"
                {...loadingProps}
            />
        )
    }

    else {
        // Если выводится массив и он пуст
        if(isArr(items) || isObj(items)) {
            if(!isLength(items)) {
                return empty
            }
        }

        return content
    }
}