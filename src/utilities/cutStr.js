export function cutStr(str, count, after = "") {
    if(str.length > count) {
        str = `${str.substr(0, count)}${after}`
    }
    return str
}