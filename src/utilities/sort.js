import { isObj, isArr, isLength, isStr, isSet } from './is'


export function sort({ list = [], by, type, order }) {
    if(isLength(list)) {

        if(isArr(list[0])) {
            return sortArr({ list, type, order })
        }
        
        else if(isObj(list[0])) {
            return sortObj({ list, by, type, order })
        }
    } else {
        return list
    }
}

export function sortArr({ list, type, order }) {
    if(isLength(list)) {

        if(isStr(type) && isStr(order)) {
            list.sort((a, b) => {
                if(isSet(a) && isSet(b)) {
                    return sortMap()[type][order](a, b)
                }
            })
        }
    }
    return list
}

export function sortObj({ list, by, type, order }) {
    if(isLength(list)) {

        if(isArr(by) && isStr(type) && isStr(order)) {
            list.sort((a, b) => {
                if(isObj(a) && isObj(b)) {
                    let aVal, bVal,
                        aPl = a,
                        bPl = b

                    by.map((byName, index) => {
                        if(isObj(aPl)) aPl = aPl[byName]
                        if(isObj(bPl)) bPl = bPl[byName]
                        if(by.length-1 === index) {
                            aVal = aPl
                            bVal = bPl
                        }
                    })

                    if(isSet(aVal) && isSet(bVal) && !isObj(aVal) && !isObj(bVal)) {
                        return sortMap()[type][order](aVal, bVal)
                    }
                }
            })
        }
    }
    return list
}

//
// Карта сортировки
//

function sortMap() {
    return {
        number: {
            "ASC": (a, b) => a - b,
            "DESC": (a, b) => b - a,
        },
        string: {
            "ASC": (a, b) => {
                if(a.toLowerCase() < b.toLowerCase()) return -1
                if(a.toLowerCase() > b.toLowerCase()) return 1
                return 0
            },
            "DESC": (a, b) => {
                if(a.toLowerCase() > b.toLowerCase()) return -1
                if(a.toLowerCase() < b.toLowerCase()) return 1
                return 0
            }
        },
        time: {
            "ASC": (a, b) => Number(a.replace(/:/g, "")) - Number(b.replace(/:/g, "")),
            "DESC": (a, b) => Number(b.replace(/:/g, "")) - Number(a.replace(/:/g, "")),
        },
    }
}