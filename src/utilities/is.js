import { length } from './length'


export const isFunc = (func) => typeof func === "function"

export const isObj = (obj) => (typeof obj === "object" && !Array.isArray(obj) && obj !== null)

export const isArr = (arr) => Array.isArray(arr)

export const isElem = (elem) => typeof elem === "object"

export const isNumb = (numb) => (typeof numb === "number" && !isNaN(numb))

export const isStr = (func) => typeof func === "string"

export const isBool = (bool) => typeof bool === "boolean"

export const isEmpty = (v) => typeof v === "undefined"

export const isSet = (v) => !isEmpty(v)

export const isLength = (s) => {
    if(s === undefined) return false
    if(isFunc(s)) return isLength(s())
    return length(s) > 0
}

export const isBrowser = () => {
    try {
        if(window) {
            if(window.document) {
                return true
            }
        }
    } catch(e) {}
    return false
}

export const isServer = () => !isBrowser()

export function isElemAtView({ elem, offset = 0 }) {
    const windowH     = document.documentElement.clientHeight
    const windowW     = document.documentElement.clientWidth
    const elemRect    = elem.getBoundingClientRect()
    const offsetsRect = {
        top:    getWithOffset(elemRect.top, windowH),
        bottom: getWithOffset(elemRect.bottom, windowH),
        left:   getWithOffset(elemRect.left, windowW),
        right:  getWithOffset(elemRect.right, windowW),
    }

    if(offsetsRect.top === 0 && offsetsRect.bottom === 0 && offsetsRect.left === 0 && offsetsRect.right === 0) {
        return false
    }

    let isTop = (offsetsRect.top < 1 || offsetsRect.top > windowH) ? false : true
    let isBottom = (offsetsRect.bottom < 1 || offsetsRect.bottom > windowH) ? false : true
    let isLeft = (offsetsRect.left < 1 || offsetsRect.left > windowW) ? false : true
    let isRight = (offsetsRect.right < 1 || offsetsRect.right > windowW) ? false : true

    return ((isTop || isBottom) && (isLeft || isRight))

    function getWithOffset(point, by) {
        if(point < 0) {
            return point + offset
        } else {
            if(point > by) {
                return point - offset
            }
        }
        return point
    }
}