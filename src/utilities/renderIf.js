import React from 'react'

import { isArr, isObj, isFunc, isLength } from './is'
import { cl } from './cl'


export function renderIf({
    className = "",
    isHide = false,
    condition,
    children,
}) {

    const iCondition = isArr(condition) || isObj(condition)
        ? isLength(condition)
        : isFunc(condition) ? condition() : condition

    // Если компоненты обязательно нужно рендерить и скрывать при отстутствии данных
    if(isHide) {
        className = cl(
            "fk-render-if",
            "hidden",
            className,
        )

        if(iCondition) return children

        return (
            <div className={className}>
                {children}
            </div>
        )
    }

    // Если рендерить не обязательно
    else {
        return iCondition ? children : null
    }
}