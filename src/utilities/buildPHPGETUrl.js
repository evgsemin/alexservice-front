// Формирует строку запроса аналогично функции http_build_query из php.
// Источник: https://gist.github.com/luk-/2722097

import { isArr, isObj, isStr, isNumb } from './is'


export function buildPHPGETUrl(args, tempKey) {
    const answer = []

    if(isArr(args)) {
        const argsObj = {}
        args.map((item, index) => argsObj[index] = item)
        args = argsObj
    }

    if(isObj(args)) {
        Object.keys(args).forEach(val => {
            let key = val
    
            key = encodeURIComponent(key.replace(/[!'()*]/g, escape))
            if(tempKey) key = `${tempKey}[${key}]`
    
            // Object
            if(isObj(args[val])) {
                const query = buildPHPGETUrl(args[val], key)
                answer.push(query)
            }

            // Array
            else if(isArr(args[val])) {
                const valueObj = {}
                args[val].map((item, index) => valueObj[index] = item)
                const query = buildPHPGETUrl(valueObj, key)
                answer.push(query)
            }

            // String || number
            else if(isStr(args[val]) || isNumb(args[val])) {
                if(isNumb(args[val])) args[val] = `${args[val]}`
                const value = encodeURIComponent(args[val].replace(/[!'()*]/g, escape))
                answer.push(`${key}=${value}`)
            }
        })
    }

    return answer.join('&')
}