import nodeFetch from 'node-fetch'


export const document = document || {
    querySelector: (selector) => null,
    querySelectorAll: (selector) => [],
    getElementById: (id) => null,
    createElement: (name) => null,
    body: {
        style: {},
        classList: {
            add: (selectors) => null,
            remove: (selectors) => null,
        }
    }
}

export const fetch = fetch || nodeFetch