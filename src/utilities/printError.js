export function printError(positionId, event = "", ...payload) {
    console.log(`Произошла ошибка при ${event} (позиция: ${positionId})`)
    payload.map(m => console.log(m))
}