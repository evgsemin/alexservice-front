import { isObj, isFunc } from './is'
import { map } from './map'


export function getFormData(params, callback) {
    const formData = new FormData()
    
    if(isObj(params)) {
        map(params, (param, name) => {
            formData.append(
                name,
                isFunc(callback)
                    ? callback(param)
                    : param
            )
        })
    }

    return formData
}