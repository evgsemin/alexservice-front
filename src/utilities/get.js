import {
    MEDIA_BREAKPOINT_LARGEST,
    MEDIA_BREAKPOINT_LAPTOP,
    MEDIA_BREAKPOINT_TABLET,
    MEDIA_BREAKPOINT_MOBILE,
} from './breakpoints'

import { isBrowser, isArr, isObj, isStr, isNumb, isEmpty } from './is'
import { length } from './length'


export function getNumbers(str) {
    if(typeof str !== "string") return 0
    return +/\d+/.exec(str)
}

export function getWindowWidth() {
    try {
        if(isBrowser()) {
            return window.outerWidth
        } else {
            switch(process.fk.deviceType) {
                case "TV":
                    return MEDIA_BREAKPOINT_LARGEST
                case "DESKTOP":
                    return MEDIA_BREAKPOINT_LAPTOP
                case "TABLET":
                    return MEDIA_BREAKPOINT_TABLET
                case "PHONE":
                    return MEDIA_BREAKPOINT_MOBILE
                case "BOT":
                    return MEDIA_BREAKPOINT_LAPTOP
                case "CAR":
                    return MEDIA_BREAKPOINT_LAPTOP
            }
        }
    } catch(e) {
        console.log(e)
    }
    return MEDIA_BREAKPOINT_LAPTOP
}

export function getIndex(arrObjStr, index = 0, fromend = false) {
    if(!isNumb(index)) return undefined

    // array | string
    if(isArr(arrObjStr) || isStr(arrObjStr)) {

        // Последний
        if(index === -1) {
            return arrObjStr[arrObjStr.length-1]
        }

        // С начала
        if(!fromend) {
            return arrObjStr[index]
        }

        // С конца
        else {
            return arrObjStr[arrObjStr.length-index]
        }
    }

    // object
    else if(isObj(arrObjStr)) {
        let lastItem,
            ii = 0

        const objLength = fromend ? length(arrObjStr) : -1

        for(let item in arrObjStr) {

            // Последний
            if(index === -1) {
                lastItem = arrObjStr[item]
            }

            // С начала
            else if(!fromend) {
                if(ii === index) {
                    return arrObjStr[item]
                }
            }

            // С конца
            else {
                if(ii === objLength-index) {
                    return arrObjStr[item]
                }
            }

            ii += 1
        }

        // Последний
        if(index === -1) {
            return lastItem
        }

        return undefined
    }
}

export function getIndexes(arrObjStr, indexes = [], fromend = false) {
    if(!isArr(arrObjStr) && !isObj(arrObjStr) && !isStr(arrObjStr)) return undefined
    if(!isArr(indexes)) {
        if(isArr(arrObjStr)) return []
        if(isObj(arrObjStr)) return {}
        return undefined
    }

    // array | string
    if(isArr(arrObjStr) || isStr(arrObjStr)) {
        const answer = []
        const prepIndexes = []

        if(fromend) {
            const arrLength = arrObjStr.length
            indexes.map(index => {
                if(isNumb(index)) {
                    prepIndexes.push(arrLength-index)
                }
            })
        } else {
            prepIndexes = indexes
        }

        prepIndexes.map(index => {
            const item = arrObjStr[index]
            if(!isEmpty(item)) {
                answer.push(item)
            }
        })

        return answer
    }

    // object
    else if(isObj(arrObjStr)) {
        const answer = {}
        const prepIndexes = {}

        const objLength = fromend ? length(arrObjStr) : -1

        indexes.map(index => {
            if(isNumb(index)) {
                if(fromend) {
                    prepIndexes[objLength-index] = true
                } else {
                    prepIndexes[index] = true
                }
            }
        })

        let ii = 0

        for(let itemName in arrObjStr) {

            if(prepIndexes[ii]) {
                answer[itemName] = arrObjStr[itemName]
            }

            ii += 1
        }

        return answer
    }
}

export function getFirst(arrObjStr) {
    return getIndex(arrObjStr, 0)
}

export function getLast(arrObjStr) {
    return getIndex(arrObjStr, -1)
}

export function getEvery(arrObjStr, every) {
    const l = (length(arrObjStr)-1)
    if(l && isNumb(every)) {
        const indexes = []
        let last = 0

        while(last <= l) {
            indexes.push(last)
            last += every
        }

        return getIndexes(arrObjStr, indexes)
    }

    if(isArr(arrObjStr)) return []
    if(isObj(arrObjStr)) return {}
    if(isStr(arrObjStr)) return ""
    return undefined
}