import { isFunc, isArr, isObj, isStr } from './is'


export function map(obj, func, returnArray = false) {
    if(isFunc(func)) {

        // object

        if(isObj(obj)) {
            const answer = returnArray ? [] : {}
            let index = 0

            for(let n in obj) {
                const funcResult = func(obj[n], n, index)

                if(funcResult !== undefined) {

                    // Если возвращаем массив Array
                    if(returnArray) {
                        answer.push(funcResult)
                    }

                    // Если возвращаем объект
                    else {
                        if(isArr(funcResult)) {
                            answer[funcResult[0]] = funcResult[1]
                        }
                    }
                }

                index += 1
            }

            return answer
        }

        // array

        if(isArr(obj)) {
            const answer = []
            
            obj.map((pl, index) => {
                const funcResult = func(pl, index)
                if(funcResult !== undefined) {
                    answer.push(funcResult)
                }
            })

            return answer
        }

        // string

        if(isStr(obj)) {
            const answer = Array.prototype.map.call(obj, func)
            return answer
        }
    }
}