export function createPostId({ slug, id }) {
    let postId = ""

    if(slug) postId += slug
    // if(id) postId += (postId) ? `-${id}` : id

    return postId
}

export function createPostLink({ postType, postId }) {
    let link = `/${postId}`
    if(postType) link = `/${postType+link}`
    return link
}