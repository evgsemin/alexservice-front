import nodeFetch from 'node-fetch'

import { isServer } from './is'


export default isServer() ? nodeFetch : fetch