import { isObj, isArr } from './is'


export function convertObjToArr(obj) {
    let arr = []

    if(isObj(obj)) {
        arr = Object.values(obj)
    } else if(isArr(obj)) {
        arr = obj
    }

    return arr
}

export function convertArrToObj(arr, fieldName) {
    let obj = {}

    if(isArr(arr)) {
        arr.map((item, index) => {
            const key = (item[fieldName] && item[fieldName] !== 0) ? item[fieldName] : index
            obj[key] = item
        })
    } else if(isObj(arr)) {
        obj = arr
    }

    return obj
}