import { isStr } from './is'


export function clearHTMLTags(str) {
    let answer = ""

    if(isStr(str)) {
        try {
            if(document) {
                const div = document.createElement("div")
                div.innerHTML = str
                answer = div.textContent
                answer = answer.replace(/\s{2,}/g, " ")
                div.remove()
            } else {
                answer = str.replace(/<\/?[^>]+(>|$)/g, "")
                answer = answer.replace(/\s{2,}/g, " ")
            }
        } catch(e) {
            answer = str.replace(/<\/?[^>]+(>|$)/g, "")
            answer = answer.replace(/\s{2,}/g, " ")
        }
    }

    return answer
}