import { isFunc } from './is'


export function safeCall(func, ...args) {
    if(isFunc(func)) {
        return func(...args)
    }
    return null
}