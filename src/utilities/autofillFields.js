import { isStr, isNumb, isObj, isFunc } from './is'
import { map } from './map'


const storageKey = "fk-autofillfields"


export function savedToStore(updateState) {
    try {
        if(isFunc(updateState)) {
            const saved = localStorage.getItem(storageKey)
            if(saved) {
                const obj = JSON.parse(saved)
    
                map(obj, (fields, formId) => updateState({ formId, fields }))
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

export function saveToStorage({ formId, fields }) {
    try {
        if(isStr(formId) && isObj(fields)) {
            const saved = localStorage.getItem(storageKey)
            const obj   = JSON.parse(saved || "{}")
            
            if(!obj[formId]) obj[formId] = {}
            
            const filtered = map(fields, (value, key) => {
                if(isStr(value) || isNumb(value)) {
                    return [
                        key,
                        value
                    ]
                }
            })
            
            obj[formId] = {
                ...obj[formId],
                ...filtered,
            }

            localStorage.setItem(storageKey, JSON.stringify(obj))
        }
    }
    catch(e) {
        console.log(e)
    }
}