import { isFunc } from './is'


export function debounce(interval) {
    let callCounter = 0

    return function(func) {
        callCounter += 1
        setTimeout(() => {
            callCounter -= 1
            if(callCounter === 0) {
                if(isFunc(func)) {
                    func()
                }
            }
        }, interval)
    }
}