import { isArr, isObj, isStr, isNumb } from './is'
import { length } from './length'


export function getIndex(arrObjStr, index = 0, fromend = false) {
    if(!isNumb(index)) return undefined

    // array | string
    if(isArr(arrObjStr) || isStr(arrObjStr)) {

        // Последний
        if(index === -1) {
            return arrObjStr[arrObjStr.length-1]
        }

        // С начала
        if(!fromend) {
            return arrObjStr[index]
        }

        // С конца
        else {
            return arrObjStr[arrObjStr.length-index]
        }
    }

    // object
    else if(isObj(arrObjStr)) {
        let lastItem,
            ii = 0

        const objLength = fromend ? length(arrObjStr) : -1

        for(let item in arrObjStr) {

            // Последний
            if(index === -1) {
                lastItem = arrObjStr[item]
            }

            // С начала
            else if(!fromend) {
                if(ii === index) {
                    return arrObjStr[item]
                }
            }

            // С конца
            else {
                if(ii === objLength-index) {
                    return arrObjStr[item]
                }
            }

            ii += 1
        }

        // Последний
        if(index === -1) {
            return lastItem
        }

        return undefined
    }
}

export function getFirst(arrObjStr) {
    return getIndex(arrObjStr, 0)
}

export function getLast(arrObjStr) {
    return getIndex(arrObjStr, -1)
}