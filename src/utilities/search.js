import { isBool, isStr } from './is'


export function search() {
    const cache = {}

    return function isMatch(str, phrase, isExact = false) {
        if(!isStr(str) || !isStr(phrase)) return false

        const key = `${str}-${phrase}-${isExact}`
        
        if(isBool(cache[key])) return cache[key]

        str = str.toLowerCase()
        phrase = phrase.toLowerCase()

        // Если точное вхождение
        if(isExact) {
            return cache[key] = isFounded(str, phrase)
        }

        // Если просто должно включать слова
        else {
            const words = phrase.split(" ")
            let founded = true

            words.map(word => {
                if(founded) {
                    if(!isFounded(str, word)) {
                        founded = false
                    }
                }
            })

            return cache[key] = founded
        }
    }
}

function isFounded(str, phrase) {
    if(!isStr(str) || !isStr(phrase)) return false
    return str.indexOf(phrase) !== -1
}