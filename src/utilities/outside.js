import { createUniversalPortal } from 'react-portal-universal'


export const outside = (elem) => {
    return createUniversalPortal(elem, "#app-outside")
}