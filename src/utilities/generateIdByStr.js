import { isStr, isNumb } from './is'
import { map } from './map'


export function generateIdByStr(str) {
    if(isNumb(str)) return str
    if(!isStr(str)) return -1

    const ids = `абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.,!@#$%^&*()_+-=?><:";'{}[]|/~№1234567890`
    
    let id = ""

    map(str, symbol => {
        const symbolIndex = ids.indexOf(symbol)
        id = `${id}${symbolIndex === -1 || symbolIndex === 0 ? 254 : symbolIndex}`
    })

    return Number(id.slice(0, 20))
}