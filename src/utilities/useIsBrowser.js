import React, { useEffect, useState } from 'react'


export const useIsBrowser = () => {
    const [isBrow, setIsBrow] = useState(false)

    useEffect(() => {
        setIsBrow(true)
    }, [])

    return isBrow
}