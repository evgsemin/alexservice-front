import { isStr, isNumb } from './is'


export function round(numb, fract = 2){
    if(isStr(numb)) numb = Number(numb)
    if(!isNumb(numb) || !isNumb(fract)) return 0
    return +numb.toFixed(fract)
}