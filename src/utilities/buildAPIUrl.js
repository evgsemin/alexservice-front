import { isStr, isObj, isBrowser } from './is'
import { buildPHPGETUrl } from './buildPHPGETUrl'


export function buildAPIUrl({
    type,
    cats,
    search,
    city,
    brand,
    fields,
    filter,
    taxesRelation = "AND", // OR
    fieldsRelation = "AND", // OR
    includeChilds = false,
    method = "getPosts",
    args,
    params,
}) {

    if(isBrowser()) {
        window.BAU = buildAPIUrl
    }

    const domain = "http://api.alexservice-demo.ru"
    
    let url = `${domain}/${method}`,
        iArgs = []

    // Типы записей
    if(isStr(type)) {
        iArgs.push(getStrObjFromCommasStr(type, "type"))
    }

    // Поиск
    if(isStr(search)) {
        iArgs.push(`search=${search}`)
    }

    // Таксономии
    if(isObj(cats)) {
        const catQuery = {}
        let index = 0

        for(let catName in cats) {
            if(isStr(cats[catName])) {
                catQuery[index] = {
                    taxonomy: catName,
                    terms: cats[catName].replace(", ", ",").split(","),
                    field: "slug",
                    operator: "AND",
                }
                index += 1
            }
        }

        iArgs.push(buildPHPGETUrl({
            relation: taxesRelation,
            ...catQuery,
        }, "cats"))
    }

    // Фильтр (filter, fields)
    if(isObj(fields) || isObj(filter)) {
        let queryFields = {},
            queryFilter = {
                ...(isObj(filter) ? filter : {})
            },
            queryMeta = {}

        // fields
        if(isObj(fields)) {
            let index = 0

            for(let key in fields) {
                queryFields[index] = {
                    key,
                    value: fields[key],
                    compare: "="
                }
                index += 1
            }
        }

        // Создаём

        queryMeta = {
            relation: queryFilter.relation || fieldsRelation,
            ...queryFields,
            ...queryFilter,
        }

        iArgs.push(buildPHPGETUrl(queryMeta, "filter"))
    }

    // Добавляем аргументы WP_QUERY
    if(isObj(args)) {
        iArgs.push(buildPHPGETUrl(args, "args"))
    }

    // Добавляем произвольные аргументы
    if(isObj(params)) {
        for(let paramName in params) {
            iArgs.push(`${paramName}=${params[paramName]}`)
        }
    }

    // Город
    if(city) iArgs.push(`city=${city}`)

    // Бренд
    if(brand) iArgs.push(`brand=${brand}`)

    // Включать записи-потомки
    if(includeChilds) iArgs.push(`includeChilds=${includeChilds}`)

    // Результат
    if(iArgs.length) url += `?${iArgs.join("&")}`

    return url
}

function getStrObjFromCommasStr(str, argName) {
    str = str.replace(", ", ",")

    let answer =
        (str.indexOf(",") !== -1)
            ? buildPHPGETUrl({
                [argName]: str.split(",")
            })
            : `${argName}=${str}`

    return answer
}