import { isStr } from './is'


export function splitAddress(fullAddress) {
    const answer = {
        locality: "",
        street: "",
    }

    if(isStr(fullAddress)) {
        const [loc, str] = fullAddress.indexOf("ул.") !== -1
            ? fullAddress.split("ул.")
            : fullAddress.split("улица")

        answer.locality = cleanStr(loc)
        answer.street = cleanStr(`ул. ${str}`)
    }

    return answer
}

function cleanStr(str) {
    let answer = ""
    if(isStr(str)) {
        answer = str.trim()
        if(answer.slice(-1) === ",") {
            answer = answer.slice(0, -1)
        }
        if(answer.slice(0, -1) === ",") {
            answer = answer.slice(1)
        }
        answer = answer.replace(/  /g, " ")
    }
    return answer
}