import { isObj, isArr, isStr, isNumb } from './is'


export function length(thing) {

    // String || array
    if(isStr(thing) || isArr(thing)) {
        return thing.length
    }

    // Object array
    if(isObj(thing)) {
        return Object.keys(thing).length
    }

    // Number
    if(isNumb(thing)) {
        return `${thing}`.length
    }

    return 0
}