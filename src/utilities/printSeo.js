import { isBrowser } from './is'


export function printSeo({
    cityData = {},
    seoData = {},
    postTitle = "",
    html = "",
}) {

    const data = {
        title:       seoData?.title || `${postTitle ? postTitle+" | " : ""}${cityData?.title || ""}`,
        description: seoData?.description || cityData?.description || "",
        keywords:    seoData?.keywords || "",
        cover:       seoData?.cover || cityData?.cover || "",
    }

    const isServer = !isBrowser()

    // title

    if(data.title) {
        if(html) {
            html = html.replace(`<title></title>`,`<title>${data.title}</title>`)
            html = html.replace(`<meta property="og:title" content="">`,`<meta property="og:title" content="${data.title}">`)
            html = html.replace(`<meta property="twitter:title" content="">`,`<meta property="twitter:title" content="${data.title}">`)
            html = html.replace(`<meta property="og:site_name" content="">`,`<meta property="og:site_name" content="${data.title}">`)
        } else if(!isServer) {
            document.title = data.title
            document.querySelector(`head meta[property="og:title"]`)?.setAttribute("content", data.title || "Страница не найдена")
            document.querySelector(`head meta[property="twitter:title"]`)?.setAttribute("content", data.title)
            document.querySelector(`head meta[property="og:site_name"]`)?.setAttribute("content", data.title)
        }
    } else {
        if(html) {
            html = html.replace(`<title></title>`,`<title>${cityData.title}</title>`)
        } else if(!isServer) {
            document.title = cityData.title
            document.querySelector(`head meta[property="og:title"]`)?.setAttribute("content", "")
            document.querySelector(`head meta[property="twitter:title"]`)?.setAttribute("content", "")
            document.querySelector(`head meta[property="og:site_name"]`)?.setAttribute("content", "")
        }
    }

    // description

    if(data.description) {
        if(html) {
            html = html.replace(`<meta name="description" content="">`,`<meta name="description" content="${data.description}">`)
            html = html.replace(`<meta property="og:description" content="">`,`<meta property="og:description" content="${data.description}">`)
            html = html.replace(`<meta property="twitter:description" content="">`,`<meta property="twitter:description" content="${data.description}">`)
        } else if(!isServer) {
            document.querySelector(`head meta[name="description"]`)?.setAttribute("content", data.description)
            document.querySelector(`head meta[property="og:description"]`)?.setAttribute("content", data.description)
            document.querySelector(`head meta[property="twitter:description"]`)?.setAttribute("content", data.description)
        }
    } else if(!isServer) {
        document.querySelector(`head meta[name="description"]`)?.setAttribute("content", "")
        document.querySelector(`head meta[property="og:description"]`)?.setAttribute("content", "")
        document.querySelector(`head meta[property="twitter:description"]`)?.setAttribute("content", "")
    }

    // keywords

    if(data.keywords) {
        if(html) {
            html = html.replace(`<meta name="keywords" content="">`,`<meta name="keywords" content="${data.keywords}">`)
        } else if(!isServer) {
            document.querySelector(`head meta[name="keywords"]`)?.setAttribute("content", data.keywords)
        }
    } else if(!isServer) {
        document.querySelector(`head meta[name="keywords"]`)?.setAttribute("content", "")
    }

    // cover

    if(data.cover) {
        if(html) {
            html = html.replace(`<link rel="image_src" href="">`,`<link rel="image_src" href="${data.cover}">`)
            html = html.replace(`<meta property="og:image" content="">`,`<meta property="og:image" content="${data.cover}">`)
            html = html.replace(`<meta property="vk:image" content="">`,`<meta property="vk:image" content="${data.cover}">`)
            html = html.replace(`<meta property="twitter:image" content="">`,`<meta property="twitter:image" content="${data.cover}">`)
        } else if(!isServer) {
            document.querySelector(`head link[rel="image_src"]`)?.setAttribute("href", data.cover)
            document.querySelector(`head meta[property="og:image"]`)?.setAttribute("content", data.cover)
            document.querySelector(`head meta[property="vk:image"]`)?.setAttribute("content", data.cover)
            document.querySelector(`head meta[property="twitter:image"]`)?.setAttribute("content", data.cover)
        }
        if(data.title) {
            if(html) {
                html = html.replace(`<meta name="twitter:image:alt" content="">`,`<meta name="twitter:image:alt" content="${data.title}">`)
            } else if(!isServer) {
                document.querySelector(`head meta[property="twitter:image:alt"]`)?.setAttribute("content", data.title)
            }
        } else if(!isServer) {
            document.querySelector(`head meta[property="twitter:image:alt"]`)?.setAttribute("content", "")
        }
    } else if(!isServer) {
        document.querySelector(`head link[rel="image_src"]`)?.setAttribute("href", "")
        document.querySelector(`head meta[property="og:image"]`)?.setAttribute("content", "")
        document.querySelector(`head meta[property="vk:image"]`)?.setAttribute("content", "")
        document.querySelector(`head meta[property="twitter:image"]`)?.setAttribute("content", "")
    }

    if(html) return html
}