import { isArr, isObj, isFunc, isLength } from './is'

import Loading from '../components/FK/Loading'


export function renderMap(arrObj, func, loading, placeholder, loadingComponent) {
    if(!isArr(arrObj) && !isObj(arrObj)) return placeholder || null
    if(!isLength(arrObj)) return placeholder || null
    if(!isFunc(func)) return placeholder || null

    // Если идёт загрузка
    if(loading === true) return loadingComponent || <Loading/>

    // array
    if(isArr(arrObj)) {
        return arrObj.map(func)
    }

    // obj
    if(isObj(arrObj)) {
        const answer = []
        let index = 0

        for(let itemName in arrObj) {
            answer.push(func(arrObj[itemName], index, itemName))
            index += 1
        }
        return answer
    }
}