import { isObj } from './is'

// Временная мера. Позже нужно переделать АПИ и убрать эту прослойку

export function getACFImageData(imageObj, ...fields) {
    const answer = {}
    
    if(isObj(imageObj)) {
        fields.map(field => {
            answer[field] = imageObj[field]
        })
    }
    
    return answer
}