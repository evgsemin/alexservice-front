import { useEffect, useState } from 'react'

import { isBrowser, isFunc, isObj } from './is'
import { getWindowWidth } from './get'

import {
    MEDIA_BREAKPOINT_LARGEST,
    MEDIA_BREAKPOINT_LAPTOP,
    MEDIA_BREAKPOINT_DESKTOP,
    MEDIA_BREAKPOINT_TABLET,
    MEDIA_BREAKPOINT_MOBILE,
} from './breakpoints'

//
// is
//

export function useIs(pointName, deps = []) {
    const [answer, setAnswer] = useState(getAnswer())

    const watcher = () => setAnswer(getAnswer())

    useEffect(() => {
        if(isBrowser()) {
            window.addEventListener("resize", watcher)
            return () => window.removeEventListener("resize", watcher)
        }
    }, [])

    useEffect(() => {
        setAnswer(getAnswer())
    }, deps)

    function getAnswer() {
        if(pointName === "zero") return isZero()
        if(pointName === "mobile") return isMobile()
        if(pointName === "tablet") return isTablet()
        if(pointName === "desktop") return isDesktop()
        if(pointName === "laptop") return isLaptop()
        if(pointName === "largest") return isLargest()
        if(pointName === "mobileAndUp") return isMobileAndUp()
        if(pointName === "tabletAndUp") return isTabletAndUp()
        if(pointName === "desktopAndUp") return isDesktopAndUp()
        if(pointName === "laptopAndUp") return isLaptopAndUp()
        if(pointName === "mobileAndDown") return isMobileAndDown()
        if(pointName === "tabletAndDown") return isTabletAndDown()
        if(pointName === "desktopAndDown") return isDesktopAndDown()
        if(pointName === "laptopAndDown") return isLaptopAndDown()
        return false
    }

    return answer
}

export function isZero() {
    const ww = getWindowWidth()
    return (ww <= MEDIA_BREAKPOINT_MOBILE-1)
}

export function isMobile() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_MOBILE && ww <= MEDIA_BREAKPOINT_TABLET-1)
}

export function isMobileAndDown() {
    const ww = getWindowWidth()
    return (ww <= MEDIA_BREAKPOINT_TABLET-1)
}

export function isMobileAndUp() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_MOBILE)
}

export function isTablet() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_TABLET && ww <= MEDIA_BREAKPOINT_DESKTOP-1)
}

export function isTabletAndDown() {
    const ww = getWindowWidth()
    return (ww <= MEDIA_BREAKPOINT_DESKTOP-1)
}

export function isTabletAndUp() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_TABLET)
}

export function isDesktop() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_DESKTOP && ww <= MEDIA_BREAKPOINT_LAPTOP-1)
}

export function isDesktopAndDown() {
    const ww = getWindowWidth()
    return (ww <= MEDIA_BREAKPOINT_LAPTOP-1)
}

export function isDesktopAndUp() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_DESKTOP)
}

export function isLaptop() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_LAPTOP && ww <= MEDIA_BREAKPOINT_LARGEST-1)
}

export function isLaptopAndDown() {
    const ww = getWindowWidth()
    return (ww <= MEDIA_BREAKPOINT_LARGEST-1)
}

export function isLaptopAndUp() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_LAPTOP)
}

export function isLargest() {
    const ww = getWindowWidth()
    return (ww >= MEDIA_BREAKPOINT_LARGEST)
}

//
// render
//

export function useMedia(content = {}, placeholder = null, deps = []) {
    const [answer, setAnswer] = useState(getAnswer())

    const watcher = () => setAnswer(getAnswer())

    useEffect(() => {
        if(isBrowser()) {
            window.addEventListener("resize", watcher)
            return () => window.removeEventListener("resize", watcher)
        }
    }, [])

    useEffect(() => {
        setAnswer(getAnswer())
    }, deps)

    function getAnswer() {
        const funcs = {
            "zero": isZero,
            "mobile": isMobile,
            "tablet": isTablet,
            "desktop": isDesktop,
            "laptop": isLaptop,
            "largest": isLargest,
            "mobileAndUp": isMobileAndUp,
            "tabletAndUp": isTabletAndUp,
            "desktopAndUp": isDesktopAndUp,
            "laptopAndUp": isLaptopAndUp,
            "mobileAndDown": isMobileAndDown,
            "tabletAndDown": isTabletAndDown,
            "desktopAndDown": isDesktopAndDown,
            "laptopAndDown": isLaptopAndDown,
        }

        if(isObj(content)) {
            for(let pointName in content) {
                const pointFunc = funcs[pointName]
                const pointContent = content[pointName]

                if(isFunc(pointFunc)) {
                    if(pointFunc()) {
                        return pointContent
                    }
                }
            }
        }
        return placeholder
    }

    return answer
}

export function mediaZero(content, placeholder = null) {
    return isZero() ? content : placeholder
}

export function mediaMobile(content, placeholder = null) {
    return isMobile() ? content : placeholder
}

export function mediaTablet(content, placeholder = null) {
    return isTablet() ? content : placeholder
}

export function mediaDesktop(content, placeholder = null) {
    return isDesktop() ? content : placeholder
}

export function mediaLaptop(content, placeholder = null) {
    return isLaptop() ? content : placeholder
}

export function mediaLargest(content, placeholder = null) {
    return isLargest() ? content : placeholder
}

// and up

export function mediaMobileAndUp(content, placeholder = null) {
    return isMobileAndUp() ? content : placeholder
}

export function mediaTabletAndUp(content, placeholder = null) {
    return isTabletAndUp() ? content : placeholder
}

export function mediaDesktopAndUp(content, placeholder = null) {
    return isDesktopAndUp() ? content : placeholder
}

export function mediaLaptopAndUp(content, placeholder = null) {
    return isLaptopAndUp() ? content : placeholder
}

// and down

export function mediaMobileAndDown(content, placeholder = null) {
    return isMobileAndDown() ? content : placeholder
}

export function mediaTabletAndDown(content, placeholder = null) {
    return isTabletAndDown() ? content : placeholder
}

export function mediaDesktopAndDown(content, placeholder = null) {
    return isDesktopAndDown() ? content : placeholder
}

export function mediaLaptopAndDown(content, placeholder = null) {
    return isLaptopAndDown() ? content : placeholder
}