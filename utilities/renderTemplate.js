import { currentCity } from '../src/store/DetermineCurrentCity/select'
import { splitAddress } from '../src/utilities/splitAddress'


export function renderTemplate({
    host,
    path,
    DL404,
    state,
    content,
}) {

    const city = { ...currentCity(state) }
    city.splitedAddress = splitAddress(city.address)

    city.scriptsHead = city.scriptsHead.replace(/s-c-r-i-p-t/g, "script")
    city.scriptsHead = city.scriptsHead.replace(/s-t-y-l-e/g, "style")
    city.scriptsBody = city.scriptsBody.replace(/s-c-r-i-p-t/g, "script")
    city.scriptsBody = city.scriptsBody.replace(/s-t-y-l-e/g, "style")

    const contacts = `{
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "${city.cityTitle}",
        "url": "${host}",
        "logo": "${city.seo.cover}",
        "telephone": "${city.mainPhone}",
        "address": {
            "@type": "PostalAddress",
            "addressLocality": "${city.splitedAddress.locality}",
            "streetAddress": "${city.splitedAddress.street}"${city.postalCode ? "," : ""}
            ${city.postalCode ? `"postalCode": "${city.postalCode}"` : ""}
        }
    }`

    return `
        <!DOCTYPE html>
        <html lang="ru">
        <head>

            <meta charset="utf-8"/>
            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta name="theme-color" content="#000000"/>
            <link rel="manifest" href="${host}/dist/web/manifest.json"/>
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,800;1,300;1,400;1,800&display=swap" rel="stylesheet">
            
            <!-- МЕТА -->
            <meta property="og:type" content="website">
            <meta property="og:locale" content="ru_RU">
            <meta property="og:url" content="${host}">
            <link rel="canonical" href="${host}${path}"/>
        
            <title></title>
            <meta property="og:title" content="">
            <meta property="twitter:title" content="">
            <meta property="og:site_name" content="">
            
            <meta name="description" content="">
            <meta property="og:description" content="">
            <meta property="twitter:description" content="">
        
            <meta name="keywords" content="">

            <!-- FAVICON -->
            <link rel="shortcut icon" href="${host}/dist/web/favicon/favicon.ico">
            <link rel="shortcut icon" href="${host}/dist/web/favicon/favicon.png" type="image/png">
            <link rel="icon" href="${host}/dist/web/favicon/favicon180.png" type="image/png">
            <link rel="apple-touch-icon" href="${host}/dist/web/favicon/favicon180.png">
            <link rel="mask-icon" href="${host}/dist/web/favicon/favicon.svg" color="#149ce2">
        
            <!-- ГРАФИКА -->
            <link rel="image_src" href="">
            <meta property="og:image" content="">
            <meta property="vk:image" content="">
            <meta property="twitter:image" content="">
            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:image:alt" content="">

            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="${host}/dist/style.css">

            <script id="app-preloaded-state">window.__PRELOADED_STATE__ = ${JSON.stringify(state)};</script>
            <script id="app-DL404">window.__DL404__ = ${JSON.stringify(DL404 || "")};</script>

            <script type="application/ld+json">
                ${contacts}
            </script>

            ${city.scriptsHead}
        </head>
        <body>
            <div id="app">${content}</div>
            <div id="app-outside"></div>
            <script src="${host}/dist/client.js"></script>
            ${city.scriptsBody}
        </body>
        </html>
    `
}