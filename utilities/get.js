export function getSeoData(appPages, path) {
    return appPages[path] || appPages["404"]
}

export function getRequestData(req) {
    const domain   = req.get('Host')
    const protocol = domain.indexOf("localhost") === -1 ? `${req.protocol}://` : "//"
    const host     = `${protocol}${domain}`
    const location = `${host}${req.url}`
    const url      = req.url
    let   path     = url.split("?")[0],
          get      = url.split("?")[1] || ""

    path = (path.slice(-1) === "/" && path !== "/") ? path.slice(0, -1) : path

    return {
        location, // http://domain.ru/actions/?period=year&name=misha
        protocol, // https://
        domain,   // domain.ru
        url,      // /actions/?period=year&name=misha
        path,     // /actions
        get,      // period=year&name=misha
        host,     // https://domain.ru
    }
}