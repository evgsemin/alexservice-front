import { isObj } from '../src/utilities/is'


export function isFoundedPage(pages, path) {
    return isObj(pages[path])
}