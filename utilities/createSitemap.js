import moment from 'moment'
import { map } from '../src/utilities/map'

import { MAIN, SERVICE } from '../src/routes/routes'


export function createSitemap(host, pages) {
    let html = `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">`
    
    map(pages, (page, pageName) => {
        if(pageName !== "404") {
            let priority = "0.64"
            if(page.href === MAIN) priority = "1.00"
            if(page.href.startsWith(SERVICE)) priority = "0.80"

            html +=
                `<url>
                    <loc>${host}${page.href}</loc>
                    <lastmod>${moment().format("YYYY-MM-DDTHH:MM:SS+00:00")}</lastmod>
                    <priority>${priority}</priority>
                </url>`
        }        
    })

    html += `</urlset>`

    return html
}