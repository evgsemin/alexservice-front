import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { hydrate } from 'react-dom'
import { Provider } from 'react-redux'

import { removeUniversalPortals } from 'react-portal-universal'

import { applyMiddleware, createStore, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { rootReducer } from './src/store/reducer'
import { rootSaga } from './src/sagas/sagas.client'

import './src/sass/import.sass'

import App from './src/App'

const DL404 = window.__DL404__ ? window.__DL404__ : undefined
delete window.__DL404__
document.getElementById("app-DL404").remove()

const preloadedState = window.__PRELOADED_STATE__
delete window.__PRELOADED_STATE__
document.getElementById("app-preloaded-state").remove()

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
    rootReducer,
    preloadedState,
    compose(
        applyMiddleware(sagaMiddleware),
        typeof window.__REDUX_DEVTOOLS_EXTENSION__ === "undefined"
            ? a => a
            : window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
)

sagaMiddleware.run(rootSaga)

removeUniversalPortals()

hydrate(
    <BrowserRouter>
        <Provider store={store}>
            <App DL404={DL404}/>
        </Provider>
    </BrowserRouter>,
    document.getElementById("app")
)